<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeetingStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_has_students', function (Blueprint $table) {
            $table->id();
            $table->integer('meeting_id');
            $table->integer('user_id');
            //$table->foreign('meeting_id')->references('id')->on('meetings');
            //$table->foreign('user_id')->references('id')->on('users');
            $table->string('fullName');
            $table->string('fullName_rod');
            $table->string('fullName_dat');
            $table->string('fullName_tv');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_has_students');
    }
}
