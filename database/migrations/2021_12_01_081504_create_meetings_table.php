<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
            $table->id();
            $table->integer('group_id');
            //$table->foreign('group_id')->references('id')->on('groups');
            $table->string('group_name');
            $table->string('status');
            $table->integer('chair_id');
            $table->integer('faculty_id');
            $table->integer('inst_id');
            $table->integer('umu_id');
            //$table->foreign('chair_id')->references('id')->on('subdividions');
            //$table->foreign('faculty_id')->references('id')->on('subdividions')->nullable();
            //$table->foreign('inst_id')->references('id')->on('subdividions')->nullable();
            //$table->foreign('umu_id')->references('id')->on('subdividions')->nullable();
            $table->json('sub');
            $table->string('qualification');
            $table->string('comment')->nullable();
            $table->string('date_document')->nullable();
            $table->string('number_document')->nullable();
            $table->boolean('exam');
            $table->boolean('report');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('comissions', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->integer('meeting_id');
            $table->integer('user_id');
            $table->string('fullName');
            $table->string('shortName');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
        Schema::dropIfExists('comission');
    }
}
