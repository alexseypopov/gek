<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeetingHasExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_has_exam_protocols', function (Blueprint $table) {
            $table->id();
            $table->integer('meeting_id');
            $table->integer('user_id');
            //$table->foreign('user_id')->references('id')->on('users');
            //$table->foreign('meeting_id')->references('id')->on('meetings');
            $table->string('status');
            $table->string('number')->nullable();
            $table->string('date')->nullable();
            $table->string('time_from')->nullable();
            $table->string('time_to')->nullable();
            $table->string('dop_questions')->nullable();
            $table->string('review')->nullable();
            //$table->string('date_document')->nullable();
            //$table->string('consultation')->nullable();
            //$table->string('adviser')->nullable();
            //$table->string('feedback_adviser')->nullable();
            //$table->string('resume')->nullable();
            //$table->string('min')->nullable();
            //$table->json('questions')->nullable();
            //$table->json('materials')->nullable();
            $table->integer('eval')->nullable();
            $table->string('comment')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_has_exam_protocols');
    }
}
