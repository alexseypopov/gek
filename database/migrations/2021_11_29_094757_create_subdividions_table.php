<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubdividionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subdividions', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->integer('parent_id')->nullable();
            $table->string('name');
            $table->string('head_degree')->nullable();
            $table->integer('head_id')->nullable();
            //$table->foreign('head_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });

        $head_subdivision = \App\Models\Role::create([
            'name' => 'head_subdivision',
            'title'  => 'Руководитель структурного подразделения',
            'description'   => 'Учет подконтрольных продазделений, учебных групп',
        ]);
        $subdivision_list = \App\Models\Permission::create([
            'name'          => 'subdivision-list',
            'description'   => 'Просмотр подразделений'
        ]);
        $subdivision_create = \App\Models\Permission::create([
            'name'          => 'subdivision-create',
            'description'   => 'Добавление подразделений'
        ]);
        $subdivision_edit = \App\Models\Permission::create([
            'name'          => 'subdivision-edit',
            'description'   => 'Редартирование подразделений'
        ]);
        $subdivision_delete = \App\Models\Permission::create([
            'name'          => 'subdivision-delete',
            'description'   => 'Удаление подразделений'
        ]);

        $group_list = \App\Models\Permission::create([
            'name'          => 'group-list',
            'description'   => 'Просмотр групп'
        ]);
        $group_create = \App\Models\Permission::create([
            'name'          => 'group-create',
            'description'   => 'Добавление групп'
        ]);
        $group_edit = \App\Models\Permission::create([
            'name'          => 'group-edit',
            'description'   => 'Редартирование групп'
        ]);
        $group_delete = \App\Models\Permission::create([
            'name'          => 'group-delete',
            'description'   => 'Удаление групп'
        ]);

        $head_subdivision->givePermissionTo([
            $subdivision_list, $subdivision_edit,
            $group_list, $group_create, $group_edit
        ]);

        $admin = \App\Models\Role::findByName('admin');

        $admin->givePermissionTo([
            $subdivision_list, $subdivision_create, $subdivision_edit, $subdivision_delete,
            $group_list, $group_create, $group_edit, $group_delete
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subdividions');

        $subdivision_list = \App\Models\Permission::findByName('subdivision-list');
        $subdivision_create = \App\Models\Permission::findByName('subdivision-create');
        $subdivision_edit = \App\Models\Permission::findByName('subdivision-edit');
        $subdivision_delete = \App\Models\Permission::findByName('subdivision-delete');

        $group_list = \App\Models\Permission::findByName('group-list');
        $group_create = \App\Models\Permission::findByName('group-create');
        $group_edit = \App\Models\Permission::findByName('group-edit');
        $group_delete = \App\Models\Permission::findByName('group-delete');

        $admin = \App\Models\Role::findByName('admin');

        $admin->revokePermissionTo([
            $subdivision_list, $subdivision_create, $subdivision_edit, $subdivision_delete,
            $group_list, $group_create, $group_edit, $group_delete
        ]);

        $head_subdivision = \App\Models\Role::findByName('head_subdivision');

        $head_subdivision->revokePermissionTo([
            $subdivision_list, $subdivision_edit,
            $group_list, $group_create, $group_edit
        ]);

        $head_subdivision->delete();


        $subdivision_list->delete();
        $subdivision_create->delete();
        $subdivision_edit->delete();
        $subdivision_delete->delete();

        $group_list->delete();
        $group_create->delete();
        $group_edit->delete();
        $group_delete->delete();
    }
}
