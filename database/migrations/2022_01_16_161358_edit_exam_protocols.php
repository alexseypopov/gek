<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditExamProtocols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meeting_has_exam_protocols', function (Blueprint $table) {
            $table->integer('no')->after('ticket_id')->default(0);
            $table->integer('yes')->after('ticket_id')->default(0);
        });
        Schema::table('meeting_has_report_protocols', function (Blueprint $table) {
            $table->integer('no')->after('comment')->default(0);
            $table->integer('yes')->after('comment')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meeting_has_exam_protocols', function (Blueprint $table) {
            $table->dropColumn('no');
            $table->dropColumn('yes');
        });
        Schema::table('meeting_has_report_protocols', function (Blueprint $table) {
            $table->dropColumn('no');
            $table->dropColumn('yes');
        });
    }
}
