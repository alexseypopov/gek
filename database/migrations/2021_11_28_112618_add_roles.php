<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $admin = \App\Models\Role::create([
            'name' => 'admin',
            'title'  => 'Администратор',
            'description'   => 'Регистрация новых пользователей, выдача прав',
        ]);
        $role_list = \App\Models\Permission::create([
            'name'          => 'role-list',
            'description'   => 'Просмотр ролей'
        ]);
        $role_create = \App\Models\Permission::create([
            'name'          => 'role-create',
            'description'   => 'Создание роли'
        ]);
        $role_edit = \App\Models\Permission::create([
            'name'          => 'role-edit',
            'description'   => 'Редактирование роли'
        ]);
        $role_delete = \App\Models\Permission::create([
            'name'          => 'role-delete',
            'description'   => 'Удаление ролей'
        ]);
        //
        $user_list = \App\Models\Permission::create([
            'name'          => 'user-list',
            'description'   => 'Просмотр пользователей'
        ]);
        $user_create = \App\Models\Permission::create([
            'name'          => 'user-create',
            'description'   => 'Создание пользователей'
        ]);
        $user_edit = \App\Models\Permission::create([
            'name'          => 'user-edit',
            'description'   => 'Редактирование пользователей'
        ]);
        $user_delete = \App\Models\Permission::create([
            'name'          => 'user-delete',
            'description'   => 'Удаление пользователей'
        ]);
        $user_pass_notifications = \App\Models\Permission::create([
            'name'          => 'user-pass-notifications',
            'description'   => 'Уведомления о предоставлении доступа'
        ]);

        //
        //
        //
        $meeting_list = \App\Models\Permission::create([
            'name'          => 'meeting-list',
            'description'   => 'Просмотр заседаний ГЭК'
        ]);
        $meeting_create = \App\Models\Permission::create([
            'name'          => 'meeting-create',
            'description'   => 'Создание заседаний ГЭК'
        ]);
        $meeting_edit = \App\Models\Permission::create([
            'name'          => 'meeting-edit',
            'description'   => 'Редактирование заседаний ГЭК'
        ]);
        $meeting_delete = \App\Models\Permission::create([
            'name'          => 'meeting-delete',
            'description'   => 'Удаление заседаний ГЭК'
        ]);
        $meeting_admin = \App\Models\Permission::create([
            'name'          => 'meeting-admin',
            'description'   => 'Администрирование заседаний ГЭК  '
        ]);

        $admin->givePermissionTo([
            $user_pass_notifications,
            $role_list, $role_create, $role_edit, $role_delete,
            $user_list, $user_create, $user_edit, $user_delete,
            $meeting_list, $meeting_create, $meeting_edit, $meeting_delete, $meeting_admin
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $admin = \App\Models\Role::findByName('admin')->delete();

        $role_list = \App\Models\Permission::findByName('role_list')->delete();
        $role_create = \App\Models\Permission::findByName('role_create')->delete();
        $role_edit = \App\Models\Permission::findByName('role_edit')->delete();
        $role_delete = \App\Models\Permission::findByName('role_delete')->delete();

        $user_list = \App\Models\Permission::findByName('user_list')->delete();
        $user_create = \App\Models\Permission::findByName('user_create')->delete();
        $user_edit = \App\Models\Permission::findByName('user_edit')->delete();
        $user_delete = \App\Models\Permission::findByName('user_delete')->delete();

        $meeting_list = \App\Models\Permission::findByName('meeting_list')->delete();
        $meeting_create = \App\Models\Permission::findByName('meeting_create')->delete();
        $meeting_edit = \App\Models\Permission::findByName('meeting_edit')->delete();
        $meeting_delete = \App\Models\Permission::findByName('meeting_delete')->delete();
    }
}
