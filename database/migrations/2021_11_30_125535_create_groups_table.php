<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->id();
            $table->integer('chair_id');
            //$table->foreign('chair_id')->references('id')->on('subdividions');;
            $table->string('level');
            $table->string('name');
            $table->string('trend');
            $table->string('profile');
            $table->unsignedInteger('graduation_year');
            $table->string('form');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('group_has_students', function (Blueprint $table) {
            $table->id();
            $table->integer('group_id');
            $table->integer('user_id');
            //$table->foreign('group_id')->references('id')->on('groups');
            //$table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
        $student = \App\Models\Role::create([
            'name' => 'student',
            'title'  => 'Студент',
            'description'   => 'Имеет возможноть получить экзаменационный билет',
        ]);

        $chairman = \App\Models\Role::create([
            'name' => 'chairman',
            'title'  => 'Председатель комиссии ГЭК',
            'description'   => 'Учавствует в заседании ГЭК в роли председателя комиссии ГЭК',
        ]);

        $secretary = \App\Models\Role::create([
            'name' => 'secretary',
            'title'  => 'Секретарь комиссии ГЭК',
            'description'   => 'Учавствует в заседании ГЭК в роли секретаря комиссии ГЭК',
        ]);

        $secretary = \App\Models\Role::create([
            'name' => 'person',
            'title'  => 'Член комиссии ГЭК',
            'description'   => 'Учавствует в заседании ГЭК в роли члена комиссии ГЭК',
        ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
        Schema::dropIfExists('group_has_students');
    }
}
