<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditComission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comissions', function (Blueprint $table) {
            $table->boolean('distance')->after('user_id')->default(0);
        });
        Schema::table('meeting_has_students', function (Blueprint $table) {
            $table->boolean('distance')->after('user_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comissions', function (Blueprint $table) {
            $table->dropColumn('distance');
        });
        Schema::table('meeting_has_students', function (Blueprint $table) {
            $table->dropColumn('distance');
        });
    }
}
