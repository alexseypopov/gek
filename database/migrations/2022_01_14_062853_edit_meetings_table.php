<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meetings', function (Blueprint $table) {
            $table->string('exam_hash')->after('report')->nullable();
            $table->string('exam_status')->after('report')->nullable();
            $table->string('tickets_status')->after('report')->nullable();
            $table->string('date_agree_tickets')->after('report')->nullable();
            $table->integer('tickets_agree_sub')->after('report')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meetings', function (Blueprint $table) {
            $table->dropColumn('date_agree_tickets');
            $table->dropColumn('tickets_status');
            $table->dropColumn('exam_hash');
            $table->dropColumn('exam_status');
            $table->dropColumn('tickets_agree_sub');
        });
    }
}
