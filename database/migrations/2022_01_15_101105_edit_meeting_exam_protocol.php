<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditMeetingExamProtocol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meeting_has_exam_protocols', function (Blueprint $table) {
            $table->integer('ticket_id')->after('comment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meeting_has_exam_protocols', function (Blueprint $table) {
            $table->dropColumn('ticket_id');
        });
    }
}
