<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditMeetingsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meetings', function($table)
        {
            \Illuminate\Support\Facades\DB::statement('ALTER TABLE `meetings` MODIFY `faculty_id` INTEGER NULL;');
            \Illuminate\Support\Facades\DB::statement('ALTER TABLE `meetings` MODIFY `inst_id` INTEGER NULL;');
            \Illuminate\Support\Facades\DB::statement('ALTER TABLE `meetings` MODIFY `umu_id` INTEGER NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
