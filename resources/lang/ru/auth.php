<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Reset Password Notification' => 'Восстановление пароля',
    'failed' => 'Неверный логин или пароль.',
    'throttle' => 'Вы поизвели слишком много попыток входа в систему. Пожалуйста, повторите попытку через :seconds секунд.',

];
