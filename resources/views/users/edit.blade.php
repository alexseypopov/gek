@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('users.edit', $user) }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Редактирование данных пользователя</h2>
                    </div>
                </div>
            </div>

            <create-edit-user type="edit" data="{{ $user->toJson() }}" status="{{ $user->getAuthStatus() }}" data_roles="{{ $user->roles->toJson() }}"></create-edit-user>

        </div>
    </div>


@endsection
