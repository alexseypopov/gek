@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('users.show', $user) }}
@endsection

@section('content')
    <div class="mt-4">
        <div class="content content-box">
            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left pl-2">
                        <div>
                            <h1 class="mb-0 d-inline-block">{{ $user->getFullName() }}</h1>
                            <h4 class="d-inline-block text-secondary">{{ empty($user->email) ? '' : '('.$user->email.')' }}</h4>
                        </div>
                        <p class="pl-3 text-right">
                            ({!! $user->getAuthStatusContent('icon') !!} - {{  $user->getAuthStatusContent('text') }})
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                @if($user->subdivisions()->count())
                                    <h3>Подразделения:</h3>
                                    <div class="indent-25">
                                        @foreach($user->subdivisions as $sub)
                                            <figure>
                                                <blockquote class="blockquote mb-0">
                                                    <p class="mb-0"><a href="/subdivisions/{{ $sub->id }}" target="_blank">{{ $sub->name }}</a></p>
                                                </blockquote>
                                                <figcaption class="blockquote-footer">
                                                    {{ $sub->head_degree }}
                                                </figcaption>
                                            </figure>
                                        @endforeach
                                    </div>
                                @endif
                                @if($user->comissions()->count())
                                    <h3>Заседания ГЭК:</h3>
                                    <div class="indent-25">
                                        @foreach($user->comissions as $com)
                                            <figure>
                                                <blockquote class="blockquote mb-0">
                                                    <p class="mb-0"><a href="/meetings/{{ $com->meeting_id }}" target="_blank">Заседание ГЭК №{{ $com->meeting_id }}</a></p>
                                                </blockquote>
                                                <figcaption class="blockquote-footer">
                                                    {{ $com->getType() }} комиссии
                                                </figcaption>
                                            </figure>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <h3>Роли пользователя:</h3>
                                @if($user->roles()->count() == 0)
                                    <p class="text-secondary indent-25">У пользователя нет ролей в системе</p>
                                @endif
                                <div class="indent-25">
                                    @foreach($user->roles()->get() as $role)
                                        <figure>
                                            <blockquote class="blockquote mb-0">
                                                <p class="mb-0">{{ $role->title }}</p>
                                            </blockquote>
                                            <figcaption class="blockquote-footer">
                                                {{ $role->description }}
                                            </figcaption>
                                        </figure>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="d-flex mt-5" style="justify-content: space-evenly;">
                            @can('user-edit')
                                <a href="/users/{{ $user->id }}/edit">Редактировать пользователя</a>
                            @endcan
                                @can('user-edit')
                                    <confirm-link
                                            href="/users/{{ $user->id }}/auth">
                                        Авторизоваться под пользователем
                                    </confirm-link>
                                @endcan
                            @can('user-delete')
                                <confirm-link
                                    class="text-danger"
                                    type_query="delete"
                                    ok_title="Удалить"
                                    ok_variant="danger"
                                    title="Удаление пользователя"
                                    text="Вы уверены, что хотите удалить пользователя '{{ $user->getFullName() }}'"
                                    redirect="/users"
                                    href="/users/{{ $user->id }}">
                                    Удалить пользователя
                                </confirm-link>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
