@extends('layouts.app')

@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('users') }}
@endsection

@section('content')

    <div class="mb-3">
        <div class="content">
            <div class="dashboard">
                <div class="dashboard-cards">
                    <a class="dashboard-card grow-fix blue" href="{{ route('users.create') }}">
                        <p class="dashboard-card-title">
                            Создать пользователя
                        </p>
                        <p class="dashboard-card-icon">
                            <i class="bi bi-person-plus"></i>
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Список пользователей</h2>
                    </div>
                </div>
            </div>

            <p class="mb-0"><span style="font-size: 14pt;"><b-icon icon="x" variant="danger"></b-icon></span><span class="pl-1">Пользователь не имеет возможности войти в систему</span></p>
            <p class="mb-0"><span style="font-size: 14pt;"><b-icon icon="link" variant="danger"></b-icon></span><span class="pl-1">Пользователь имеет возможность войти в систему, но не получил ссылку для предоставления доступа/восстановления пароля</span></p>
            <p class="mb-0"><span style="font-size: 14pt;"><b-icon icon="link" variant="success"></b-icon></span><span class="pl-1">Пользователь получил ссылку для предоставления доступа/восстановления пароля</span></p>
            <p class="mb-0"><span style="font-size: 14pt;"><b-icon icon="check" variant="success"></b-icon></span><span class="pl-1">Пользователю предоставлен доступ к системе</span></p>
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="text-center">№</th>
                        <th class="text-center"></th>
                        <th>ФИО</th>
                        <th>Роли</th>
                        <th>Адрес электронной почты</th>
                        <th class="text-center"><i class="bi bi-gear-fill"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $key => $user)
                        <tr valign="middle">
                            <td align="center">{{ $users->currentPage() * $users->perPage() - $users->perPage() + $loop->iteration }}</td>
                            <td align="center" valign="top"><h3>{!! $user->getAuthStatusContent('icon') !!}</h3></td>
                            <td>
                                <a href="{{ route('users.show', $user) }}">
                                    {{ $user->getFullName() }}
                                </a>
                            </td>
                            <td>
                                @if($user->roles()->count())
                                    @foreach($user->getRoleNames() as $v)
                                        <label class="badge badge-success">{{ $v }}</label>
                                    @endforeach
                                @else
                                    <p class="text-secondary">Без привязки к роли</p>
                                @endif
                            </td>
                            <td>{!! empty($user->email) ? '<p class="text-secondary">Без доступа к системе</p>' : $user->email !!}</td>
                            <td align="center">

                                <b-dropdown right variant="outline-dark">
                                    <template #button-content>
                                        <i class="bi bi-gear-fill"></i>
                                    </template>

                                    @can('user-edit')
                                        <b-dropdown-item href="/users/{{ $user->id }}/edit">
                                            Редактировать данные
                                        </b-dropdown-item>
                                    @endcan

                                    @can('user-pass-notifications')
                                        <b-dropdown-item>
                                            <confirm-link class="dropdown-item p-0" href="/users/{{ $user->id }}/sendPasswordFillNotification">
                                                Выслать ссылку на предоставление доступа
                                            </confirm-link>
                                        </b-dropdown-item>
                                    @endcan

                                    @can('user-edit')
                                        <b-dropdown-item>
                                            <confirm-link class="dropdown-item p-0" href="/users/{{ $user->id }}/auth">
                                                Авторизоваться под пользователем
                                            </confirm-link>
                                        </b-dropdown-item>
                                    @endcan

                                    @can('user-delete')
                                        <b-dropdown-item>
                                            <confirm-link
                                                    class="dropdown-item p-0"
                                                    type_query="delete"
                                                    ok_title="Удалить"
                                                    ok_variant="danger"
                                                    title="Удаление пользователя"
                                                    text="Вы уверены, что хотите удалить пользователя '{{ $user->getFullName() }}'"
                                                    redirect="/users"
                                                    href="/users/{{ $user->id }}">
                                                Удалить
                                            </confirm-link>
                                        </b-dropdown-item>
                                    @endcan

                                </b-dropdown>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div>
                {{ $users->links() }}
            </div>

        </div>
    </div>
@endsection
