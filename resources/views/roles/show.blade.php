@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('roles.show', $role) }}
@endsection

@section('content')
    <div class="mt-4">
        <div class="content content-box">
            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left pl-2">
                        <div>
                            <h1 class="mb-0 d-inline-block">{{ $role->title }}</h1>
                            <h4 class="text-secondary indent-25">{{ $role->description }}</h4>
                            <p>
                                Возможности роли:<br>
                                @foreach($rolePermissions as $permission)
                                    <label class="badge badge-success">{{ $permission->description }}</label>
                                @endforeach
                            </p>
                        </div>
                        
                        <div class="d-flex" style="justify-content: space-evenly;">
                            <a href="/roles/{{ $role->id }}/edit">Редактировать роль</a>
                            <confirm-link
                                    class="text-danger"
                                    type_query="delete"
                                    ok_title="Удалить"
                                    ok_variant="danger"
                                    title="Удаление роли"
                                    text="Вы уверены, что хотите удалить роль '{{ $role->title }}'"
                                    redirect="/roles"
                                    href="/roles/{{ $role->id }}">
                                Удалить роль
                            </confirm-link>
                        </div><hr>

                        <role-user-add role="{{ $role->toJson() }}"></role-user-add><hr>

                        <h4>Список пользователей у которых есть роль "{{ $role->title }}"</h4>
                        @if($role->users()->count() == 0)
                            <p class="text-secondary indent-25">У роли нет пользователей в системе</p>
                        @endif
                        <table class="table table-bordered table-hover">
                            <tbody>
                            @foreach($role->users()->get() as $user)
                                <tr>
                                    <td>{{ $user->getFullName() }}</td>
                                    <td>{{ empty($user->email) ? 'Пользователь без доступа к системе' : $user->email }}</td>
                                    <td>
                                        <confirm-link
                                                class="text-danger"
                                                type_query="delete"
                                                ok_title="Подтвердить"
                                                ok_variant="danger"
                                                text="Вы уверены, что хотите убрать у пользователя '{{ $user->getFullName() }}' роль '{{ $role->title }}'?"
                                                redirect="/roles/{{ $role->id }}"
                                                href="/users/{{ $user->id }}/detachRole/{{ $role->id }}">
                                            Отвязать пользователя
                                        </confirm-link>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
