@extends('layouts.app')

@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('roles') }}
@endsection

@section('content')

    <div class="mb-3">
        <div class="content">
            <div class="dashboard">
                <div class="dashboard-cards">
                    <a class="dashboard-card grow-fix blue" href="{{ route('roles.create') }}">
                        <p class="dashboard-card-title">
                            Создать роль
                        </p>
                        <p class="dashboard-card-icon">
                            <i class="bi bi-person-plus"></i>
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Список ролей</h2>
                    </div>
                </div>
            </div>

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="text-center">№</th>
                        <th>Роль</th>
                        <th>Описание</th>
                        <th>Пользователей</th>
                        <th class="text-center"><i class="bi bi-gear-fill"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($roles as $key => $role)
                        <tr valign="middle">
                            <td align="center">{{ $roles->currentPage() * $roles->perPage() - $roles->perPage() + $loop->iteration }}</td>
                            <td><a href="/roles/{{ $role->id }}">{{ $role->title }}</a></td>
                            <td>{{ $role->getDescription() }}</td>
                            <td>
                                @if(!$role->users()->count())
                                    <p class="text-secondary">Пользователей с ролью "{{ $role->title }}" не найдено</p>
                                @else
                                    <p class="text-center">{{ $role->users()->count() }}</p>
                                @endif
                            </td>
                            <td align="center">
                                <b-dropdown class="mx-1" right variant="outline-dark">
                                    <template #button-content>
                                        <i class="bi bi-gear-fill"></i>
                                    </template>

                                    @can('role-edit')
                                        <b-dropdown-item href="/roles/{{ $role->id }}/edit">
                                            Редактировать
                                        </b-dropdown-item>
                                    @endcan

                                    @can('role-delete')
                                        <b-dropdown-item>
                                            <confirm-link
                                                    class="dropdown-item p-0"
                                                    type_query="delete"
                                                    ok_title="Удалить"
                                                    ok_variant="danger"
                                                    title="Удаление роли"
                                                    text="Вы уверены, что хотите удалить роль '{{ $role->title }}'"
                                                    redirect="/roles"
                                                    href="/roles/{{ $role->id }}">
                                                Удалить
                                            </confirm-link>
                                        </b-dropdown-item>
                                    @endcan

                                </b-dropdown>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div>
                {{ $roles->links() }}
            </div>

        </div>
    </div>
@endsection
