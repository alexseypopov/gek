@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('roles.edit', $role) }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Редактирование роли</h2>
                    </div>
                </div>
            </div>

            <create-edit-role type="edit" data="{{ $role->toJson() }}" data_permissions="{{ $rolePermissions->toJson() }}"></create-edit-role>

        </div>
    </div>


@endsection
