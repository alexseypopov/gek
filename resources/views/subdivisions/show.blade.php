@extends('layouts.app')

@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('subdivisions.show', $subdivision) }}
@endsection

@section('content')
    <div class="mt-4">
        <div class="content content-box">
            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div>
                        <h2 class="mb-3">{{ $subdivision->name }}</h2>

                        @if($subdivision->parent()->exists())
                            <h4 class="text-right">Подразделение является частью подразделения:</h4>
                            <div class="text-right">
                                <a href="{{ route('subdivisions.show', $subdivision->parent) }}" class="meeting-item mb-3" style="">
                                        <h5 class="mb-1">{{ $subdivision->parent->name }}</h5>
                                    <p class="mb-0 d-flex meeting-item-text" style="justify-content: space-between;">
                                        <span class="pl-1 pr-1">{{ $subdivision->parent->head_degree }}</span>
                                        <span class="pl-1 pr-1">{{ $subdivision->parent->head->getFullName() }}</span>
                                    </p>
                                </a>
                            </div>
                        @endif

                        <h4 class="mb-0">{{ $subdivision->head_degree }}</h4>
                        <h4 class="mb-3">
                            <a href="/users/{{ $subdivision->head_id }}" target="_blank">{{ $subdivision->head->getFullName() }}</a>
                        </h4>

                        @if($subdivision->childs()->exists())
                           <div class="mb-3">
                               <h4>Структура подразделения:</h4>
                               @foreach($subdivision->childs as $child)
                                   <div class="ml-5">
                                       <a href="{{ route('subdivisions.show', $child) }}" class="meeting-item" style="width: 100%;">
                                           <h5 class="mb-1">{{ $child->name }}</h5>
                                           <p class="mb-0 meeting-item-text" style="">
                                               <span class="pl-1 pr-5">{{ $child->head_degree }}</span>
                                               <span class="pl-1 pr-1">{{ $child->head->getFullName() }}</span>
                                           </p>
                                       </a>
                                       @foreach($child->childs as $child2)
                                           <div class="ml-5">
                                               <a href="{{ route('subdivisions.show', $child2) }}" class="meeting-item" style="width: 100%;">
                                                   <h5 class="mb-1">{{ $child2->name }}</h5>
                                                   <p class="mb-0 meeting-item-text">
                                                       <span class="pl-1 pr-5">{{ $child2->head_degree }}</span>
                                                       <span class="pl-1 pr-1">{{ $child2->head->getFullName() }}</span>
                                                   </p>
                                               </a>
                                               @foreach($child2->childs as $child3)
                                                   <div class="ml-5">
                                                       <a href="{{ route('subdivisions.show', $child3) }}" class="meeting-item" style="width: 100%;">
                                                           <h5 class="mb-1">{{ $child3->name }}</h5>
                                                           <p class="mb-0 meeting-item-text">
                                                               <span class="pl-1 pr-5">{{ $child3->head_degree }}</span>
                                                               <span class="pl-1 pr-1">{{ $child3->head->getFullName() }}</span>
                                                           </p>
                                                       </a>
                                                       @foreach($child3->childs as $child4)
                                                           <div class="ml-5">
                                                               <a href="{{ route('subdivisions.show', $child4) }}" class="meeting-item" style="width: 100%;">
                                                                   <h5 class="mb-1">{{ $child4->name }}</h5>
                                                                   <p class="mb-0 meeting-item-text">
                                                                       <span class="pl-1 pr-5">{{ $child4->head_degree }}</span>
                                                                       <span class="pl-1 pr-1">{{ $child4->head->getFullName() }}</span>
                                                                   </p>
                                                               </a>
                                                           </div>
                                                       @endforeach
                                                   </div>
                                               @endforeach
                                           </div>
                                       @endforeach
                                   </div>
                               @endforeach
                           </div>
                        @endif
                        @if($subdivision->chair_groups()->count())
                            <div class="mb-3">
                                <h4>Список групп:</h4>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>Группа</th>
                                            <th>Студентов в группе</th>
                                            <th>Каферда</th>
                                            <th>Уровень образования</th>
                                            <th>Форма обучения</th>
                                            <th>Год выпуска</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($subdivision->chair_groups as $key => $group)
                                            <tr valign="middle">
                                                <td>
                                                    <a href="/groups/{{ $group->id }}" target="_blank">{{ $group->name }}</a>
                                                </td>
                                                <td>
                                                    {{ $group->getCoutStudents() }}
                                                </td>
                                                <td>
                                                    {{ $group->getChairName() }}
                                                </td>
                                                <td>
                                                    {{ $group->getLevel() }}
                                                </td>
                                                <td>
                                                    {{ $group->getForm() }}
                                                </td>
                                                <td>
                                                    {{ $group->graduation_year }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                            </div>
                        @endif

                        <div class="d-flex mt-5" style="justify-content: space-evenly;">
                            @can('subdivision-edit')
                                <a href="/subdivisions/{{ $subdivision->id }}/edit">Редактировать данные подразделения</a>
                            @endcan
                            @can('subdivision-delete')
                                <confirm-link
                                        class="text-danger"
                                        type_query="delete"
                                        ok_title="Удалить"
                                        ok_variant="danger"
                                        title="Удаление подразделения"
                                        text="Вы уверены, что хотите удалить подразделение '{{ $subdivision->name }}'"
                                        redirect="/subdivisions"
                                        href="/subdivisions/{{ $subdivision->id }}">
                                    Удалить подразделение
                                </confirm-link>
                            @endcan
                        </div>
                       {{-- <div class="row">
                            <div class="col-md-5">
                                <h3>Группа <a href="/groups/{{ $meeting->group_id }}" target="_blank">{{ $meeting->group_name }}</a></h3>
                                <p class="mb-0">Председатель комиссии:</p>
                                <p class="mb-1 indent-25">
                                    <a href="/user/{{ $meeting->getChairmanId() }}">{{ $meeting->getChairmanName('full') }}</a>
                                </p>
                                <p class="mb-0">Секретарь комиссии: </p>
                                <p class="mb-1 indent-25">
                                    <a href="/user/{{ $meeting->getSecretaryId() }}">{{ $meeting->getSecretaryName('full') }}</a>
                                </p>
                                <p class="mb-0">Члены комиссии: </p>
                                @foreach($meeting->persons as $person)
                                    <p class="mb-1 indent-25">
                                        <a href="/user/{{ $person->id }}">{{ $person->fullName }}</a>
                                    </p>
                                @endforeach
                            </div>
                            <div class="col-md-7">
                                @foreach(json_decode($meeting->sub) as $sub)
                                    @if(!empty($sub->id))
                                        <div class="mb-3">
                                            <p class="mb-1"><a href="/subdivisions/{{ $sub->id }}" target="_blank">{{ $sub->name }}</a></p>
                                            <p class="mb-0 d-flex" style="justify-content: space-between;">
                                                <span>{{ $sub->head_degree }}</span>
                                                <a target="_blank" href="/users/{{ $sub->head_id }}">{{ $sub->head_fio }}</a>
                                            </p>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>--}}
                    </div>
                   {{-- <div class="pull-left pl-2">
                        <div>
                            <h1 class="mb-0 d-inline-block">{{ $user->getFullName() }}</h1>
                            <h4 class="d-inline-block text-secondary">{{ empty($user->email) ? '' : '('.$user->email.')' }}</h4>
                        </div>
                        <p class="pl-3 text-right">
                            ({!! $user->getAuthStatusContent('icon') !!} - {{  $user->getAuthStatusContent('text') }})
                        </p>
                        <p>
                            Роли пользователя:<br>
                            @if($user->roles()->count() == 0)
                                <p class="text-secondary indent-25">У пользователя нет ролей в системе</p>
                            @endif
                            @foreach($user->roles()->get() as $role)
                                <label class="badge badge-success">{{ $role->title }}</label>
                            @endforeach
                        </p>
                        <div class="d-flex" style="justify-content: space-evenly;">
                            @can('user-edit')
                                <a href="/users/{{ $user->id }}/edit">Редактировать пользователя</a>
                            @endcan
                            @can('user-delete')
                                <confirm-link
                                    class="text-danger"
                                    type_query="delete"
                                    ok_title="Удалить"
                                    ok_variant="danger"
                                    title="Удаление пользователя"
                                    text="Вы уверены, что хотите удалить пользователя '{{ $user->getFullName() }}'"
                                    redirect="/users"
                                    href="/users/{{ $user->id }}">
                                    Удалить пользователя
                                </confirm-link>
                            @endcan
                        </div><hr>
                    </div>--}}
                </div>
            </div>
        </div>
    </div>
@endsection
