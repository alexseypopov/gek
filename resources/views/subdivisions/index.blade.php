@extends('layouts.app')

@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('subdivisions') }}
@endsection

@section('content')

    <div class="mb-3">
        <div class="content">
            <div class="dashboard">
                <div class="dashboard-cards">
                    <a class="dashboard-card grow-fix blue" href="{{ route('subdivisions.create') }}">
                        <p class="dashboard-card-title">
                            Создать подразделение
                        </p>
                        <p class="dashboard-card-icon">
                            <i class="bi bi-person-plus"></i>
                        </p>
                    </a>
                    <a class="dashboard-card grow-fix green" href="{{ route('groups.create') }}">
                        <p class="dashboard-card-title">
                            Добавить группу
                        </p>
                        <p class="dashboard-card-icon">
                            <i class="bi bi-journal-plus"></i>
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Список подразделений</h2>
                    </div>
                </div>
            </div>

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="text-center">№</th>
                        <th>Подразделение</th>
                        <th>Должность руководителя</th>
                        <th>Руководитель</th>
                        <th class="text-center"><i class="bi bi-gear-fill"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($subdivisions as $key => $subdivision)
                        <tr valign="middle">
                            <td align="center">{{ $subdivisions->currentPage() * $subdivisions->perPage() - $subdivisions->perPage() + $loop->iteration }}</td>
                            <td>
                                <a href="/subdivisions/{{ $subdivision->id }}">{{ $subdivision->name }}</a>
                            </td>
                            <td>
                                {{ $subdivision->head_degree }}
                            </td>
                            <td>
                                <a href="/users/{{ $subdivision->head_id }}" target="_blank"> {{ $subdivision->getHeadFullName() }}</a>
                            </td>
                            <td align="center">

                                <b-dropdown right variant="outline-dark">
                                    <template #button-content>
                                        <i class="bi bi-gear-fill"></i>
                                    </template>

                                    @can('subdivision-edit')
                                        <b-dropdown-item href="/subdivisions/{{ $subdivision->id }}/edit">
                                            Редактировать данные
                                        </b-dropdown-item>
                                    @endcan

                                    @can('subdivision-delete')
                                        <b-dropdown-item>
                                            <confirm-link
                                                    class="dropdown-item p-0"
                                                    type_query="delete"
                                                    ok_title="Удалить"
                                                    ok_variant="danger"
                                                    title="Удаление подразделения"
                                                    text="Вы уверены, что хотите удалить подразделение '{{ $subdivision->name }}'"
                                                    redirect="/subdivisions"
                                                    href="/subdivisions/{{ $subdivision->id }}">
                                                Удалить
                                            </confirm-link>
                                        </b-dropdown-item>
                                    @endcan

                                </b-dropdown>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div>
                {{ $subdivisions->links() }}
            </div>

        </div>
    </div>
@endsection
