@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('subdivisions.edit', $subdivision) }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Редактирование данных структурного подразделения</h2>
                    </div>
                </div>
            </div>

            <create-edit-sub
                    type="edit"
                    level="{{ json_encode($subdivision->getLevel()) }}"
                    data="{{ $subdivision->toJson() }}"
                    data_user="{{ json_encode([ 'value' => $subdivision->getHeadFullName(), 'id' => $subdivision->head_id, 'data' => $subdivision->head ]) }}"
                    data_parent="{{ $subdivision->parent()->exists() ? json_encode($subdivision->parent) : '{}'}}">
            </create-edit-sub>

        </div>
    </div>


@endsection
