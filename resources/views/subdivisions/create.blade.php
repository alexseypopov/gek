@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('subdivisions.create') }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Создание нового подразделения</h2>
                    </div>
                </div>
            </div>
            <create-edit-sub></create-edit-sub>
        </div>
    </div>


@endsection
