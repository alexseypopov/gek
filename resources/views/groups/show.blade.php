@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('groups.show', $group) }}
@endsection

@section('content')
    <div class="mt-4">
        <div class="content content-box">
            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left pl-2">
                        <div>
                            <h1 class="mb-3">{{ $group->name }}</h1>

                            <div class="row">
                                <div class="col-md-6">
                                    @if($group->meetings()->count())
                                        <h3>Заседания ГЭК:</h3>
                                        <div class="indent-25">
                                            @foreach($group->meetings as $meeting)
                                                <figure>
                                                    <blockquote class="blockquote mb-0">
                                                        <p class="mb-0"><a href="/meetings/{{ $meeting->id }}" target="_blank">Заседание ГЭК №{{ $meeting->id }}</a></p>
                                                    </blockquote>
                                                    {{--<figcaption class="blockquote-footer">--}}
                                                        {{--{{ $com->getType() }} комиссии--}}
                                                    {{--</figcaption>--}}
                                                </figure>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    @if($group->chair()->exists())
                                        <h4 class="text-right">Кафедра:</h4>
                                        <div class="text-right">
                                            <a href="{{ route('subdivisions.show', $group->chair) }}" target="_blank" class="meeting-item mb-3" style="max-width: 640px;">
                                                <h5 class="mb-1">{{ $group->chair->name }}</h5>
                                                <p class="mb-0 d-flex meeting-item-text" style="justify-content: space-between;">
                                                    <span class="pl-1 pr-1">{{ $group->chair->head_degree }}</span>
                                                    <span class="pl-1 pr-1">{{ $group->chair->head->getFullName() }}</span>
                                                </p>
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            @if($group->students()->count())
                                <div class="mb-3">
                                    <h4>Список студентов:</h4>
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>ФИО</th>
                                            <th>Доступ к системе</th>
                                            <th>Адрес электронной почты</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($group->students as $key => $user)
                                            <tr valign="middle">
                                                <td>
                                                    <a href="{{ route('users.show', $user) }}">
                                                        {{ $user->getFullName() }}
                                                    </a>
                                                </td>
                                                <td>
                                                    {{ $user->getAuthStatusContent('text') }}
                                                </td>
                                                <td>{!! empty($user->email) ? '<p class="text-secondary">Без доступа к системе</p>' : $user->email !!}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>


                        <div class="d-flex mt-5" style="justify-content: space-evenly;">
                            @can('group-edit')
                                <a href="/groups/{{ $group->id }}/edit">Редактировать группу</a>
                            @endcan
                            @can('group-delete')
                                <confirm-link
                                    class="text-danger"
                                    type_query="delete"
                                    ok_title="Удалить"
                                    ok_variant="danger"
                                    title="Удаление пользователя"
                                    text="Вы уверены, что хотите удалить группу '{{ $group->name }}'"
                                    redirect="/groups"
                                    href="/groups/{{ $group->id }}">
                                    Удалить пользователя
                                </confirm-link>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
