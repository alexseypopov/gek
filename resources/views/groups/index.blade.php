@extends('layouts.app')

@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('groups') }}
@endsection

@section('content')

    <div class="mb-3">
        <div class="content">
            <div class="dashboard">
                <div class="dashboard-cards">
                    <a class="dashboard-card grow-fix green" href="{{ route('groups.create') }}">
                        <p class="dashboard-card-title">
                            Добавить группу
                        </p>
                        <p class="dashboard-card-icon">
                            <i class="bi bi-journal-plus"></i>
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Список учебных групп</h2>
                    </div>
                </div>
            </div>

            @if($groups->count())
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">№</th>
                        <th>Группа</th>
                        <th>Студентов в группе</th>
                        <th>Каферда</th>
                        <th>Уровень образования</th>
                        <th>Форма обучения</th>
                        <th>Год выпуска</th>
                        <th class="text-center"><i class="bi bi-gear-fill"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($groups as $key => $group)
                        <tr valign="middle">
                            <td align="center">{{ $groups->currentPage() * $groups->perPage() - $groups->perPage() + $loop->iteration }}</td>
                            <td>
                                <a href="/groups/{{ $group->id }}">{{ $group->name }}</a>
                            </td>
                            <td>
                                {{ $group->getCoutStudents() }}
                            </td>
                            <td>
                                {{ $group->getChairName() }}
                            </td>
                            <td>
                                {{ $group->getLevel() }}
                            </td>
                            <td>
                                {{ $group->getForm() }}
                            </td>
                            <td>
                                {{ $group->graduation_year }}
                            </td>

                            <td align="center">

                                <b-dropdown right variant="outline-dark">
                                    <template #button-content>
                                        <i class="bi bi-gear-fill"></i>
                                    </template>

                                    @can('group-edit')
                                        <b-dropdown-item href="/groups/{{ $group->id }}/edit">
                                            Редактировать данные
                                        </b-dropdown-item>
                                    @endcan

                                    @can('group-delete')
                                        <b-dropdown-item>
                                            <confirm-link
                                                    class="dropdown-item p-0"
                                                    type_query="delete"
                                                    ok_title="Удалить"
                                                    ok_variant="danger"
                                                    title="Удаление группы"
                                                    text="Вы уверены, что хотите удалить группу '{{ $group->name }}'"
                                                    redirect="/groups"
                                                    href="/groups/{{ $group->id }}">
                                                Удалить
                                            </confirm-link>
                                        </b-dropdown-item>
                                    @endcan

                                </b-dropdown>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p class="text-center text-secondary mt-5">Список групп пуст</p>
            @endif

            <div>
                {{ $groups->links() }}
            </div>

        </div>
    </div>
@endsection
