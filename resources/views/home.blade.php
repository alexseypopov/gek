@extends('layouts.app')

{{--@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('home') }}
@endsection--}}

@section('content')
    <div class="mt-4">
       <div class="content content-box">
           @include('dashboard.dashboard')
       </div>
    </div>
@endsection
