@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('meetings.report', $meeting) }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Запролнение протокола ВКР/Научного доклада</h2>
                    </div>
                </div>
            </div>
            <create-edit-meeting-report
                    protocol="{{ $protocol }}"
                    meeting="{{ $meeting }}"
                    student="{{ $student }}"
                    chairman="{{ $meeting->chairman }}"
                    secretary="{{ $meeting->secretary }}"
                    persons="{{ $meeting->persons }}"
                    group="{{ $meeting->group }}"
            ></create-edit-meeting-report>
        </div>
    </div>


@endsection
