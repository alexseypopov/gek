@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('meetings.show', $meeting) }}
@endsection

@section('content')
    <div class="mt-4">
        <div class="content content-box">
            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left pl-2">
                        <div>
                            <h4 class="mb-1 text-right text-secondary">{{ $meeting->getStatus() }}</h4>
                            <h3 class="mb-3"> Заседание государственной экзаменационной комиссии №{{ $meeting->id }}</h3>
                            <div class="row">
                                <div class="col-md-5">
                                    <h3>Группа <a href="/groups/{{ $meeting->group_id }}" target="_blank">{{ $meeting->group_name }}</a></h3>
                                    <p class="mb-0">Председатель комиссии:</p>
                                    <h5 class="mb-1 indent-25">
                                        <a href="/users/{{ $meeting->getChairmanId() }}">{{ $meeting->getChairmanName('full') }}</a>
                                    </h5>
                                    <p class="mb-0">Секретарь комиссии: </p>
                                    <h5 class="mb-1 indent-25">
                                        <a href="/users/{{ $meeting->getSecretaryId() }}">{{ $meeting->getSecretaryName('full') }}</a>
                                    </h5>
                                    <p class="mb-0">Члены комиссии: </p>
                                    @foreach($meeting->persons as $person)
                                        <h5 class="mb-1 indent-25">
                                            <a href="/users/{{ $person->user_id }}">{{ $person->fullName }}</a>
                                        </h5>
                                    @endforeach
                                    <p class="mb-0">Контрольное мероприятие:</p>
                                        @if($meeting->report)
                                            <p class="indent-25 mb-0">- <strong>ВКР/Научный доклад</strong></p>
                                        @endif
                                        @if($meeting->exam)
                                            <p class="indent-25 mb-0">- <strong>Экзамен</strong></p>
                                            <p class="mt-3">Билетов создано: {{ $meeting->tickets()->count() }}</p>
                                        @endif
                                </div>
                                <div class="col-md-7 pr-4">
                                    @foreach(json_decode($meeting->sub) as $sub)
                                            @if(!empty($sub->id))
                                            <div class="mb-2">
                                                <h5 class="mb-1"><a href="/subdivisions/{{ $sub->id }}" target="_blank">{{ $sub->name }}</a></h5>
                                                <p class="mb-0 d-flex" style="justify-content: space-between;">
                                                    <span>{{ $sub->head_degree }}</span>
                                                    <a target="_blank" href="/users/{{ $sub->head_id }}">{{ $sub->head_fio }}</a>
                                                </p>
                                            </div>
                                            @endif
                                    @endforeach
                                </div>
                            </div>
                            @if($meeting->exam_status == \App\Models\Meeting::EXAM_NOW)
                                <div class="row bg-white p-5">
                                    <div class="col-md-12">
                                        <div class="mb-3">
                                            <h5>Ссылка для получения билетов</h5>
                                            <p>Скопируйте ссылку и перешлите её студентам, у которых есть доступ к системе. После авторизации им будет выдан случайный билет из списка. При обращении студента с просьбой выбрать другой билет необходимо нажать на ссылку "Повторный выбор билета" напротив ФИО студента и подтвердить действие.</p>
                                            <input type="text" class="form-control" id="exampleFormControlInput1" value="{{ url('exam/'.$meeting->exam_hash) }}" disabled>
                                            <p class="mt-3 mb-0"><strong>{{ $meeting->students()->count() }}</strong> учавствуют в заседании.</p>
                                            <p class="mb-0"><strong>{{ $meeting->exam_protocols()->where('ticket_id', '!=', null)->count() }}</strong> получили билет.</p>
                                            <p class="mb-0"><strong>{{ $meeting->exam_protocols()->where('ticket_id', null)->count() }}</strong> не получили билет.</p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <hr>
                        <div>
                            @if($meeting->students()->count())
                                <div class="mb-3">
                                    <div class="">
                                        <div class="dashboard">
                                            <div class="dashboard-cards">
                                                @if($meeting->status == \App\Models\Meeting::STATUS_CREATED_SLUG)
                                                    <confirm-link
                                                            class="dashboard-card grow-fix green"
                                                            ok_title="Подтвердить"
                                                            ok_variant="primary"
                                                            title="Подтверждение состава комиссии и списка студентов"
                                                            text="Вы уверены, что хотите подтвердить корректность состава комиссии и списка студентов? После подтверждения, редактирование списка студентов и состава комиссии будет возможно только при обращении к администратору."
                                                            redirect="/meetings/{{ $meeting->id }}"
                                                            href="/meetings/{{ $meeting->id }}/confirm">
                                                        <p class="dashboard-card-title">
                                                            Подтвердить состав комиссии
                                                        </p>
                                                        <p class="dashboard-card-icon">
                                                            <i class="bi bi-check-lg"></i>
                                                        </p>
                                                    </confirm-link>
                                                @endif
                                                @if($meeting->status == \App\Models\Meeting::STATUS_CREATED_SLUG)
                                                    <a class="dashboard-card grow-fix green" href="{{ route('meetings.students', $meeting) }}">
                                                        <p class="dashboard-card-title">
                                                            Изменить допуски студентов
                                                        </p>
                                                        <p class="dashboard-card-icon">
                                                            <i class="bi bi-person"></i>
                                                        </p>
                                                    </a>
                                                @endif
                                                @if($meeting->exam)
                                                    <a class="dashboard-card grow-fix green" href="{{ route('meetings.tickets', $meeting) }}">
                                                        <p class="dashboard-card-title">
                                                            Экзаменационные билеты
                                                        </p>
                                                        <p class="dashboard-card-icon">
                                                            <i class="bi bi-ticket"></i>
                                                        </p>
                                                    </a>
                                                @endif
                                                @if(($meeting->tickets_status == \App\Models\Meeting::TICKET_CREATED_SLUG || empty($meeting->tickets_status)) && $meeting->tickets()->count() )
                                                    <confirm-link
                                                        class="dashboard-card grow-fix green"
                                                        ok_title="Подтвердить"
                                                        ok_variant="primary"
                                                        title="Отправить билеты на согласование"
                                                        text="Вы уверены, что хотите отправить билеты на согласование? После подтверждения, редактирование списка билетов будет невозможно."
                                                        redirect="/meetings/{{ $meeting->id }}"
                                                        href="/meetings/{{ $meeting->id }}/ticketsConfirm">
                                                        <p class="dashboard-card-title">
                                                            Согласовать билеты
                                                        </p>
                                                        <p class="dashboard-card-icon">
                                                            <i class="bi bi-check-lg"></i>
                                                        </p>
                                                    </confirm-link>
                                                @endif
                                                @if($meeting->tickets_status == \App\Models\Meeting::TICKET_CONFIRMED_SLUG)
                                                        @if($meeting->exam_status == \App\Models\Meeting::EXAM_WAIT || empty($meeting->exam_status))
                                                            <confirm-link
                                                                    class="dashboard-card grow-fix blue"
                                                                    ok_title="Подтвердить"
                                                                    ok_variant="primary"
                                                                    title="Начать экзамен"
                                                                    text="Вы уверены, что хотите начать экзмен? После подтверждения появится ссылку, перейдя по которой студенты с доступом к системе смогут получить билет."
                                                                    redirect="/meetings/{{ $meeting->id }}"
                                                                    href="/meetings/{{ $meeting->id }}/startExam">
                                                                <p class="dashboard-card-title">
                                                                    Начать экзамен
                                                                </p>
                                                                <p class="dashboard-card-icon">
                                                                    <i class="bi bi-exclamation-lg"></i>
                                                                </p>
                                                            </confirm-link>
                                                        @elseif($meeting->exam_status == \App\Models\Meeting::EXAM_NOW)
                                                            <confirm-link
                                                                    class="dashboard-card grow-fix blue"
                                                                    ok_title="Подтвердить"
                                                                    ok_variant="primary"
                                                                    title="Начать экзамен"
                                                                    text="Вы уверены, что хотите начать экзмен? После подтверждения студенты не смогут получить билет."
                                                                    redirect="/meetings/{{ $meeting->id }}"
                                                                    href="/meetings/{{ $meeting->id }}/closeExam">
                                                                <p class="dashboard-card-title">
                                                                    Закончить экзамен
                                                                </p>
                                                                <p class="dashboard-card-icon">
                                                                    <i class="bi bi-dash-circle"></i>
                                                                </p>
                                                            </confirm-link>
                                                        @endif
                                                @endif

                                                 {{--   @if($meeting->status == \App\Models\Meeting::STATUS_CREATED_SLUG)
                                                        <a class="dashboard-card grow-fix green" href="{{ route('meetings.students', $meeting) }}">
                                                            <p class="dashboard-card-title">
                                                                Изменить допуски студентов
                                                            </p>
                                                            <p class="dashboard-card-icon">
                                                                <i class="bi bi-person"></i>
                                                            </p>
                                                        </a>
                                                    @endif--}}

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <h2 class="mb-3">Список допущеных студентов</h2>
                                <table class="table table-bordered table-hover">
                                    @foreach($meeting->students as $student)
                                        <tr id="student_{{ $student->user_id }}">
                                            <td width="75px"><h4 class="text-center">{{ $loop->iteration }}</h4></td>
                                            <td>
                                                <h4>{{ $student->fullName }}</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p class="text-secondary">{{ empty($student->user->email) ? 'Без доступа к системе' : $student->user->email }}</p>
                                                        @if($meeting->exam && $meeting->tickets_status == \App\Models\Meeting::STATUS_CONFIRMED_SLUG && $student->exam_protocol->ticket_id == null && $student->distance)
                                                            <p class="mb-0 text-secondary">Билет не выдан, начните экзамен и передайте ссылку студенту, по которой он сможет получить билет</p>
                                                        @elseif($meeting->exam && $meeting->tickets_status == \App\Models\Meeting::STATUS_CONFIRMED_SLUG && $student->exam_protocol->ticket_id == null && !$student->distance)
                                                            <form action="/meetings/{{ $meeting->id }}/exam/{{ $student->exam_protocol->id }}/attachTicket">
                                                            <div class="row">
                                                                <div class="col-md-8">
                                                                        <select class="form-control" name="ticket_id">
                                                                            @foreach($meeting->tickets as $ticket)
                                                                                <option value="{{ $ticket->id }}">{{ $ticket->number }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <button class="btn btn-outline-primary mt-1" type="submit">Выбрать</button>
                                                                </div>
                                                            </div>
                                                            </form>
                                                        @elseif($meeting->exam && $meeting->tickets_status == \App\Models\Meeting::STATUS_CONFIRMED_SLUG && $student->exam_protocol->ticket_id != null)
                                                          <div class="row mb-1">
                                                              <div class="col-md-6">
                                                                  <strong>
                                                                      <a href="/meetings/{{ $meeting->id }}/tickets/{{ $student->exam_protocol->ticket_id }}" target="_blank">Выбран билет №{{ $student->exam_protocol->ticket->number }}</a>
                                                                  </strong>
                                                              </div>
                                                              <div class="col-md-6">
                                                                  <confirm-link
                                                                          class="text-danger"
                                                                          type_query="get"
                                                                          ok_title="Подтвердить"
                                                                          ok_variant="danger"
                                                                          title="Вторая попытка выбора билета"
                                                                          text="Вы уверены, что хотите дать '{{ $student->fullName_dat }}' возможность повторной выдачи билета?"
                                                                          href="/meetings/{{ $meeting->id }}/exam/{{ $student->exam_protocol->id }}/detachTicket">
                                                                      Повторный выбор билета
                                                                  </confirm-link>
                                                              </div>
                                                          </div>
                                                        @else
                                                            <p class="mb-0 text-secondary">Билет не выдан, выдача билета возможна только после согласования списка билетов</p>
                                                        @endif
                                                        @if($meeting->status != \App\Models\Meeting::STATUS_CREATED_SLUG)
                                                            <p class="mt-1">Итоги голосования: <strong>за {{ $student->yesCount() }}, против {{ $student->noCount() }} членов комиссии</strong></p>
                                                            @if($meeting->voteComission()->where('distance', 0)->exists())
                                                               @if($meeting->exam && $meeting->voteComission()->where('distance', 0)->count())
                                                                    <p class="mb-0">Распределите голоса {{ $meeting->voteComission()->where('distance', 0)->count() }} голос(а) по протоколу экзамена.</p>
                                                                    <form class="mb-2" action="/meetings/{{ $meeting->id }}/exam/{{ $student->exam_protocol->id }}/votes">
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label class="form-label">За</label>
                                                                                <input name="yes" type="number" class="form-control" value="{{ $student->exam_protocol->yes }}">
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <label class="form-label">Против</label>
                                                                                <input name="no" type="number" class="form-control" value="{{ $student->exam_protocol->no }}">
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="mb-3"></div>
                                                                                <button type="submit" class="btn btn-outline-primary mt-3">Сохранить</button>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                               @endif
                                                                   @if($meeting->report && $meeting->voteComission()->where('distance', 0)->count())
                                                                       <p class="mb-0">Распределите голоса {{ $meeting->voteComission()->where('distance', 0)->count() }} голос(а) по протоколу ВКР.</p>
                                                                       <form class="mb-2" action="/meetings/{{ $meeting->id }}/report/{{ $student->report_protocol->id }}/votes">
                                                                           <div class="row">
                                                                               <div class="col-md-4">
                                                                                   <label class="form-label">За</label>
                                                                                   <input name="yes" type="number" class="form-control" value="{{ $student->report_protocol->yes }}">
                                                                               </div>
                                                                               <div class="col-md-4">
                                                                                   <label class="form-label">Против</label>
                                                                                   <input name="no" type="number" class="form-control" value="{{ $student->report_protocol->no }}">
                                                                               </div>
                                                                               <div class="col-md-4">
                                                                                   <div class="mb-3"></div>
                                                                                   <button type="submit" class="btn btn-outline-primary mt-3">Сохранить</button>
                                                                               </div>
                                                                           </div>
                                                                       </form>
                                                                   @endif
                                                            @endif
                                                            <table class="table table-bordered bg-white">
                                                                <tr>
                                                                    <th></th>
                                                                    @if($meeting->report)
                                                                        <th>ВКР</th>
                                                                    @endif
                                                                    @if($meeting->exam)
                                                                        <th>Экзамен</th>
                                                                    @endif
                                                                </tr>
                                                                @foreach($meeting->voteComission as $comission)
                                                                    <tr>
                                                                        <td>{{ $comission->shortName }}</td>
                                                                        @if($meeting->report)
                                                                            @if($student->report_protocol->status == \App\Models\MeetingHasReportProtocol::STATUS_FILL_SLUG)
                                                                                <td align="center">{!!  $comission->getVoteIcon('report', $student->user_id) !!}</td>
                                                                            @else
                                                                                <td align="center"><span class="text-secondary"><i class="bi bi-dash"></i></span></td>
                                                                            @endif
                                                                        @endif
                                                                        @if($meeting->exam)
                                                                            @if($student->exam_protocol->status == \App\Models\MeetingHasExamProtocol::STATUS_FILL_SLUG)
                                                                                <td align="center">{!! $comission->getVoteIcon('exam', $student->user_id) !!}</td>
                                                                            @else
                                                                                <td align="center"><span class="text-secondary"><i class="bi bi-dash"></i></span></td>
                                                                            @endif
                                                                        @endif
                                                                    </tr>
                                                                @endforeach
                                                            </table>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-6">
                                                        @include('meetings.buttons.index')
                                                        {{--<p><a href="/meetings/{{ $meeting->id }}/report/{{ $student->user->id }}">Заполнить протокол ВКР</a></p>--}}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>

                            @else
                                <div class="mb-3">
                                    <div class="">
                                        <div class="dashboard">
                                            <div class="dashboard-cards">
                                                <a class="dashboard-card grow-fix green" href="{{ route('meetings.students', $meeting) }}">
                                                    <p class="dashboard-card-title">
                                                        Допустить студентов
                                                    </p>
                                                    <p class="dashboard-card-icon">
                                                        <i class="bi bi-person-plus"></i>
                                                    </p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
