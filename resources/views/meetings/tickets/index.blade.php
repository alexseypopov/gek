@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('meetings.tickets', $meeting) }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Экзаменационные билеты группы <a href="/groups/{{ $meeting->group_id }}" target="_blank">{{ $meeting->group_name }}</a></h2>
                        <h3 class="text-secondary">{{ $meeting->getTicketStatus() }}</h3>
                        <p>Дата согласования экзаменационных билетов: {{ $meeting->normal_date_agree_tickets }}</p>
                    </div>
                </div>
            </div>

            <div class="mb-3">
                <div class="dashboard">
                    <div class="dashboard-cards">
                        @if($meeting->tickets_status == \App\Models\Meeting::TICKET_CREATED_SLUG || empty($meeting->tickets_status))
                            <a class="dashboard-card grow-fix green" v-b-modal.modal-ticket-create>
                                <p class="dashboard-card-title">
                                    Добавить билет
                                </p>
                                <p class="dashboard-card-icon">
                                    <i class="bi bi-ticket"></i>
                                </p>
                            </a>
                            <b-modal id="modal-ticket-create" title="Создание билета" hide-footer>
                                <form action="/meetings/{{ $meeting->id }}/tickets/create">
                                    <label for="number">№ билета</label>
                                    <div class="input-group mb-3">
                                        <input name="number" type="text" class="form-control" placeholder="" aria-label="number" aria-describedby="basic-addon1" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Создать билет</button>
                                    </div>
                                </form>
                            </b-modal>
                        @endif
                        @if(($meeting->tickets_status == \App\Models\Meeting::TICKET_CREATED_SLUG || empty($meeting->tickets_status)) && $meeting->tickets()->count())
                            <confirm-link
                                    class="dashboard-card grow-fix green"
                                    ok_title="Подтвердить"
                                    ok_variant="primary"
                                    title="Отправить билеты на согласование"
                                    text="Вы уверены, что хотите отправить билеты на согласование? После подтверждения, редактирование списка билетов будет невозможно."
                                    redirect="/meetings/{{ $meeting->id }}"
                                    href="/meetings/{{ $meeting->id }}/ticketsConfirm">
                                <p class="dashboard-card-title">
                                    Согласовать билеты
                                </p>
                                <p class="dashboard-card-icon">
                                    <i class="bi bi-check-lg"></i>
                                </p>
                            </confirm-link>
                        @endif
                        @if($meeting->status != \App\Models\Meeting::STATUS_SUCCESS_SLUG)
                                <a class="dashboard-card grow-fix green" v-b-modal.modal-change-date>
                                    <p class="dashboard-card-title">
                                        Дата согласования билетов
                                    </p>
                                    <p class="dashboard-card-icon">
                                        <i class="bi bi-calendar-event"></i>
                                    </p>
                                </a>
                                <b-modal id="modal-change-date" title="Дата согласования билетов" hide-footer>
                                    <form action="/meetings/{{ $meeting->id }}/changeDate">
                                        <label for="number">Дата согласования билетов (выводится при скачивании билета)</label>
                                        <div class="input-group mb-3">
                                            <input name="date_agree_tickets" type="date" class="form-control" placeholder="" aria-label="number" aria-describedby="basic-addon1" required>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">Изменить дату</button>
                                        </div>
                                    </form>
                                </b-modal>
                        @endif
                    </div>
                </div>
            </div>

            <hr>

            @foreach($meeting->tickets as $ticket)
                <div class="row mt-3 border-bottom pb-3 mb-3">
                    <div class="col-md-7">
                        @if($meeting->tickets_status == \App\Models\Meeting::TICKET_CREATED_SLUG || empty($meeting->tickets_status))
                            <h3><a href="/meetings/{{ $meeting->id }}/tickets/{{ $ticket->id }}">Билет №{{ $ticket->number }}</a></h3>
                        @else
                            <h3>Билет №{{ $ticket->number }}</h3>
                        @endif

                        @foreach($ticket->tasks as $task)
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="mb-0 mt-3">Задание №{{ $task->number }}</p>
                                    <p class="indent-25">{{ $task->text }}</p>
                                    @if(!empty($task->img))
                                        <img src="{{ $task->img }}" alt="" style="max-width: 100%;">
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-5">
                        <div class="dashboard">
                            <div class="dashboard-cards" style="justify-content: flex-end;">
                                <a class="dashboard-card grow-fix blue" href="/meetings/{{ $meeting->id }}/tickets/{{ $ticket->id }}/download">
                                    <p class="dashboard-card-title">
                                        Скачать билет
                                    </p>
                                    <p class="dashboard-card-icon">
                                        <i class="bi bi-file-earmark-arrow-down"></i>
                                    </p>
                                </a>
                            </div>
                        </div>
                        @if($meeting->tickets_status == \App\Models\Meeting::TICKET_CREATED_SLUG || empty($meeting->tickets_status))
                            <div class="text-center">
                                <confirm-link
                                        class="text-danger"
                                        type_query="get"
                                        ok_title="Удалить"
                                        ok_variant="danger"
                                        title="Удаление билета"
                                        text="Вы уверены, что хотите удалить билет № '{{ $ticket->number }}'"
                                        href="/meetings/{{ $meeting->id }}/tickets/{{ $ticket->id }}/delete">
                                    Удалить билет
                                </confirm-link>
                            </div>
                        @endif
                    </div>
                </div>
            @endforeach

        </div>
    </div>


@endsection
