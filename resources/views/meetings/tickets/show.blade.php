@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('meetings.tickets.show', $meeting, $ticket) }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Билет №{{ $ticket->number }}</h2>
                        <h3 class="text-secondary">{{ $meeting->getTicketStatus() }}</h3>
                        <p>Дата согласования экзаменационных билетов: {{ $meeting->normal_date_agree_tickets }}</p>
                    </div>
                </div>
            </div>

            <div class="mb-3">
                <div class="dashboard">
                    <div class="dashboard-cards">
                        <a class="dashboard-card grow-fix green" v-b-modal.modal-task-create>
                            <p class="dashboard-card-title">
                                Добавить задание
                            </p>
                            <p class="dashboard-card-icon">
                                <i class="bi bi-ticket"></i>
                            </p>
                        </a>
                        <b-modal id="modal-task-create" title="Создание билета" hide-footer>
                            <form method="post" enctype="multipart/form-data" action="/meetings/{{ $meeting->id }}/tickets/{{ $ticket->id }}/task">
                                @csrf
                                <label for="number">Задание</label>
                                <div class="input-group mb-3">
                                    <input name="text" type="text" class="form-control" placeholder="" aria-label="text" aria-describedby="basic-addon1" required>
                                </div>

                                <div class="mb-3">
                                    <label for="img">Изображение (при наличии)</label>
                                    <input type="file" id="profile_pic" name="img"
                                           accept="image/*">
                                </div>

                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Добавить задание</button>
                                </div>
                            </form>
                        </b-modal>
                    </div>
                </div>
            </div>

            <hr>

            @foreach($ticket->tasks as $task)
                <div class="row mb-3 mt-3 pb-3 border-bottom">
                    <div class="col-md-12">
                        <p class="mb-0">Задание №{{ $task->number }}
                            <confirm-link
                                    class="text-danger"
                                    type_query="get"
                                    ok_title="Удалить"
                                    ok_variant="danger"
                                    title="Удаление задания"
                                    text="Вы уверены, что хотите удалить задание № '{{ $ticket->number }}'"
                                    href="/meetings/{{ $meeting->id }}/tickets/{{ $ticket->id }}/task/{{ $task->id }}/delete">
                                (Удалить задание)
                            </confirm-link>
                        </p>
                        <p class="indent-25">{{ $task->text }}</p>
                        @if(!empty($task->img))
                            <img src="{{ $task->img }}" alt="" style="max-width: 100%;">
                        @endif    
                    </div>
                </div>
            @endforeach


        </div>
    </div>


@endsection
