@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('meetings.tickets.showStudent', $meeting, $ticket) }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Билет №{{ $ticket->number }}</h2>
                        <p>Дата согласования экзаменационных билетов: {{ $meeting->normal_date_agree_tickets }}</p>
                    </div>
                </div>
            </div>

            <hr>

            @foreach($ticket->tasks as $task)
                <div class="row mb-3 mt-3 pb-3 border-bottom">
                    <div class="col-md-12">
                        <p class="mb-0">Задание №{{ $task->number }}
                        </p>
                        <p class="indent-25">{{ $task->text }}</p>
                        @if(!empty($task->img))
                            <img src="{{ $task->img }}" alt="" style="max-width: 100%;">
                        @endif    
                    </div>
                </div>
            @endforeach


        </div>
    </div>


@endsection
