@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('groups.edit', $group) }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Редактирование данных структурного подразделения</h2>
                    </div>
                </div>
            </div>

            <create-edit-group
                    type="edit"
                    chair="{{ json_encode($group->chair) }}"
                    level="{{ json_encode([ 'slug' => $group->level, 'name' => $group->getLevel() ]) }}"
                    form="{{ json_encode([ 'slug' => $group->form, 'name' => $group->getForm() ]) }}"
                    data="{{ $group->toJson() }}"
                    data_students="{{ $group->getStudentsJson() }}"
            ></create-edit-group>

        </div>
    </div>


@endsection
