@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('meetings.create') }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Создание заседания ГЭК</h2>
                    </div>
                </div>
            </div>
            <create-edit-meeting></create-edit-meeting>
        </div>
    </div>


@endsection
