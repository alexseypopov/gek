@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('meetings.students', $meeting) }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Заседания ГЭК №{{ $meeting->id }} для группы <a href="/groups/{{ $meeting->group_id }}" target="_blank">{{ $meeting->group_name }}</a></h2>
                    </div>
                </div>
            </div>
            <create-edit-meeting-students meeting="{{ $meeting->toJson() }}"></create-edit-meeting-students>
        </div>
    </div>


@endsection
