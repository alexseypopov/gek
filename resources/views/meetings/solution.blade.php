@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('meetings.solution', $meeting) }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Запролнение решения ГЭК</h2>
                    </div>
                </div>
            </div>
            <create-edit-meeting-solution
                    protocol="{{ $protocol }}"
                    meeting="{{ $meeting }}"
                    student="{{ $student }}"
                    chairman="{{ $meeting->chairman }}"
                    secretary="{{ $meeting->secretary }}"
                    persons="{{ $meeting->persons }}"
                    group="{{ $meeting->group }}"
                    report_protocol_text="{{ $student->getReportProtocolText() }}"
                    exam_protocol_text="{{ $student->getExamProtocolText() }}"
                    excelent="{{ $student->isExcelent() }}"
                    yes="{{ $student->yesCount() }}"
                    no="{{ $student->noCount() }}"
            ></create-edit-meeting-solution>
        </div>
    </div>


@endsection
