@extends('layouts.app')

@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('meetings') }}
@endsection

@section('content')

    <div class="mb-3">
        <div class="content">
            <div class="dashboard">
                <div class="dashboard-cards">
                    <a class="dashboard-card grow-fix green" href="{{ route('meetings.create') }}">
                        <p class="dashboard-card-title">
                            Добавить заседание ГЭК
                        </p>
                        <p class="dashboard-card-icon">
                            <i class="bi bi-calendar3-event"></i>
                        </p>
                    </a>
                </div>
            </div>
        </div>
    </div>


    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Список учебных групп и заседаний ГЭК</h2>
                    </div>
                </div>
            </div>

            @if($groups->count())
                @foreach($groups as $key => $group)
                    <div class="row group-row {{ $loop->even ? 'lightgrey' : '' }}">
                        <div class="col-md-1 text-center"><h3>{{ $groups->currentPage() * $groups->perPage() - $groups->perPage() + $loop->iteration }}</h3></div>
                        <div class="col-md-5"><h3>{{ $group->name }}</h3></div>
                        <div class="col-md-6 meeting-box pr-2">
                            @if($group->meetings->count())
                                @foreach($group->meetings as $meeting)
                                    <a href="{{ route('meetings.show', $meeting) }}" class="meeting-item bg-white" style="width: 100%;">
                                        <div class="row mb-1">
                                            <div class="col-md-6">
                                                <h5 class="mb-0">Заседание ГЭК №{{ $meeting->id }}</h5>
                                                <p class="mb-0 text-secondary indent-25"><i>{{ $meeting->comment }}</i></p>
                                            </div>
                                            <div class="col-md-6 text-secondary text-right pr-3">
                                                {{ $meeting->getStatus() }}
                                            </div>
                                        </div>
                                        <div class="meeting-item-text">
                                            <p class="mb-0">Контрольное мероприятие:
                                            @if($meeting->report)
                                                <strong>ВКР</strong>
                                            @endif
                                                @if($meeting->report && $meeting->exam)
                                                    ,
                                                @endif
                                            @if($meeting->exam)
                                                <strong>Экзамен</strong>
                                            @endif
                                            </p>
                                            <p class="mb-0">Председатель комиссии: <span class="text-primary">{{ $meeting->getChairmanName('full') }}</span></p>
                                            <p class="mb-0">Секретарь комиссии: <span class="text-primary">{{ $meeting->getSecretaryName('full') }}</span></p>
                                            <p class="mb-0">Студентов допущено: {{ $meeting->students()->count() }}
                                            @if($meeting->exam)
                                                , билетов создано: {{ $meeting->tickets()->count() }}
                                            @endif
                                            </p>
                                        </div>
                                    </a>
                                @endforeach
                            @else
                                <p class="text-secondary mb-0">Заседаний ГЭК не найдено</p>
                            @endif
                            {{--@can('meeting-create')
                                <p class="">
                                    <a href="{{ route('meetings.create') }}">Создать заседание ГЭК</a>
                                </p>
                            @endcan--}}
                        </div>
                    </div>
                @endforeach


              {{--      <div class="row margin-bottom-30" style="border-bottom: dashed 1px lightgrey;">
                        <div class="col-md-1 text-center"></div>
                        <div class="col-md-11">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <h3 class="margin-bottom-10"></h3>
                                    </div>

                                    <div class="col-md-8 ">
                                        @if($group->meetings->count())
                                            @foreach($group->meetings as $meeting)
                                                <div class="row paddings-10 margin-bottom-10" style="outline: 1px solid lightgrey;">
                                                    <div class="col-md-12">
                                                        <div class="col-md-7">
                                                            @if(!empty($meeting->comment))
                                                                <p><span style="font-weight: bold; background-color: #f2f2f2; padding: 5px 15px; border-radius: 15px;">{{ $meeting->comment }}</span></p>
                                                            @endif
                                                            <p>Статус: </p>
                                                            <p>Квалификация: </p>
                                                            <p>Форма контрольного мероприятия: </p>
                                                            <p class="margin-bottom-10"></p>
                                                            <p>Допущено студентов: </p>
                                                            <p>Форма проведения заседания: </p>
                                                           --}}{{-- @if($gekMeeting->isExam)
                                                                <div class="border-e5 padding-left-10 padding-top-5 margin-bottom-10">

                                                                    <p>Составлено протоколов экзамена: {{ $gekMeeting->getCountProtocols(null, 'exam', true) }}</p>
                                                                    <p>Составлено решений: {{ $gekMeeting->getCountSolutions() }}</p>
                                                                    <p>Утверждено решений: {{ $gekMeeting->getCountApproveSolutions() }} </p>

                                                                </div>
                                                            @endif
                                                            <p>Председатель: {{ $gekMeeting->getChairmanLink() }}</p>
                                                            <p>Секретарь: {{ $gekMeeting->getSecretaryLink() }}</p>--}}{{--
                                                        </div>
                                                        <div class="col-md-5 text-right">
--}}{{--
                                                            <a class="btn btn-success" href="/education/gek/{{ $gekMeeting->id }}">Данные о заседании</a>

                                                            @if( $gekMeeting->status != \Education\Gek\GekMeeting::STATUS_APPROVE && \Education\Gek\GekAccess::isSecretary($gekMeeting->id))
                                                                <a class="btn btn-warning margin-top-5 confirm" data-href="/education/gek/{{ $gekMeeting->id }}/approve" data-name="Завершить заседание ГЭК" data-group="{{ $group->name }}">Завершить заседание ГЭК</a>
                                                            @endif

                                                            @if($gekMeeting->status != \Education\Gek\GekMeeting::STATUS_APPROVE && \Education\Gek\GekAccess::isGekAdmin())
                                                                <a class="btn-block text-danger margin-top-20 confirm" data-href="/education/gek/{{ $gekMeeting->id }}/delete" data-name="Удалить заседание ГЭК" data-group="{{ $group->name }}" style="cursor: pointer;">Удалить заседание ГЭК</a>
                                                            @endif

                                                            @if($gekMeeting->status == \Education\Gek\GekMeeting::STATUS_APPROVE && \Education\Gek\GekAccess::isGekAdmin())
                                                                <a class="btn text-danger padding-left-10 margin-top-20 confirm" data-href="/education/gek/{{ $gekMeeting->id }}/back" data-name="Разрешить редактирование заседания ГЭК" data-group="{{ $group->name }}" style="cursor: pointer;">Разрешить редактирование</a>
                                                            @endif--}}{{--
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6"></div>
                                                    <div class="col-md-6 text-right padding-right-20">
                                                        <a class="btn btn-primary" href="/education/gek/add?group={{ $group->id }}">Объявить заседание ГЭК</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>--}}


            @else
                <p class="text-center text-secondary mt-5">Список групп пуст</p>
            @endif

            <div>
                {{ $groups->links() }}
            </div>

        </div>
    </div>
@endsection
