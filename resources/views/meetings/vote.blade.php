@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('vote') }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            @if(empty($result))
                <h2 class="text-secondary text-center mt-5">На данный момент все голоса учтены, спасибо.</h2>
            @endif
            @foreach($result as $votes)
                @foreach($votes as $type => $vote)
                    <div class="row mt-2 mb-2">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2>Cтудент группы {{ $vote['meeting']->group_name }} {{ $vote['student']->fullName }}</h2>
                                <h3>{{ $type == 'exam' ? $vote['student']->getExamProtocolText() : $vote['student']->getReportProtocolText() }}</h3>
                            </div>
                        </div>
                    </div>

                    <div class="mb-3">
                        <div class="dashboard">
                            <div class="dashboard-cards">
                                <confirm-link
                                        class="dashboard-card grow-fix green"
                                        ok_title="Подтвердить"
                                        ok_variant="primary"
                                        title="Согласовать билеты"
                                        text="Вы уверены, что хотите согласовать билеты?"
                                        href="/vote/{{ $vote['meeting']->id }}/{{ $type }}/{{ $vote['student']->user_id }}/success">
                                    <p class="dashboard-card-title">
                                        За
                                    </p>
                                    <p class="dashboard-card-icon">
                                        <i class="bi bi-check-lg"></i>
                                    </p>
                                </confirm-link>
                                <confirm-link
                                        class="dashboard-card grow-fix red"
                                        ok_title="Подтвердить"
                                        ok_variant="danger"
                                        title="Отправить на доработку"
                                        text="Вы уверены, что хотите отправить билеты на доработку?"
                                        href="/vote/{{ $vote['meeting']->id }}/{{ $type }}/{{ $vote['student']->user_id }}/fail">
                                    <p class="dashboard-card-title">
                                        Против
                                    </p>
                                    <p class="dashboard-card-icon">
                                        <i class="bi bi-x-lg"></i>
                                    </p>
                                </confirm-link>
                            </div>
                        </div>
                        <hr>
                    </div>
                @endforeach
            @endforeach
        </div>
    </div>


@endsection
