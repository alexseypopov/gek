<div class="dashboard-cards" style="justify-content: center; align-items: flex-start;">
    @if($meeting->status != \App\Models\Meeting::STATUS_SUCCESS_SLUG)
    <a class="dashboard-card grow-fix-mini green" href="/meetings/{{ $meeting->id }}/exam/{{ $student->user->id }}" style="height: 112px;">
       <div class="row">
           <div class="col-md-6">
               <p class="dashboard-card-title">
                   Заполнить<br> протокол экзамена
               </p>

           </div>
           <div class="col-md-6">
               <p class="dashboard-card-icon d-flex">
                   <i class="bi bi-pencil-square"></i>
               </p>
           </div>
       </div>
    </a>
    @endif
    @if($student->statusExam() == \App\Models\MeetingHasReportProtocol::STATUS_CREATED_SLUG)
        <b-button class="dashboard-card grow-fix-mini blue" disabled href="/" style="height: 112px;">
            <div class="row">
                <div class="col-md-6">
                    <p class="dashboard-card-title">Скачивание<br> недоступно</p>
                </div>
                <div class="col-md-6">
                    <p class="dashboard-card-icon">
                        <i class="bi bi-file-earmark-arrow-down"></i>
                    </p>
                </div>
            </div>
        </b-button>
    @else
        <a class="dashboard-card grow-fix-mini blue"
           href="/meetings/{{ $meeting->id }}/exam/{{ $student->user->id }}/download" style="height: 112px;">
            <div class="row">
                <div class="col-md-6">
                    <p class="dashboard-card-title">
                        Скачать<br> протокол экзамена
                    </p>
                </div>
                <div class="col-md-6">
                    <p class="dashboard-card-icon">
                        <i class="bi bi-file-earmark-arrow-down"></i>
                    </p>
                </div>
            </div>
        </a>
    @endif
</div>
