<div class="dashboard mt-0">
    @if($meeting->status == \App\Models\Meeting::STATUS_CREATED_SLUG)
        <div class="dashboard-cards" style="justify-content: center; align-items: flex-start;">
            <b-button class="dashboard-card gray" disabled href="/">
                <p class="dashboard-card-title">Перед заполнением протоколов необходимо утвердить состав комиссии</p>
            </b-button>
        </div>
    @else
       @if($meeting->report)
           @include('meetings.buttons.report')
       @endif
       @if($meeting->exam)
           @include('meetings.buttons.exam')
       @endif
           @include('meetings.buttons.solution')
    @endif
</div>