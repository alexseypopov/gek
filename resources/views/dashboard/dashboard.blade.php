@include('dashboard.parts.gek')

@can('user-create', 'user-list', 'role-create', 'role-list')
    @include('dashboard.parts.admin')
@endcan

@can('subdivision-create', 'subdivision-list', 'group-create', 'group-list')
    @include('dashboard.parts.education')
@endcan

