<div class="dashboard">
    <h2 class="dashboard-title">Учебный процесс</h2>
    <div class="dashboard-cards">

        @can('subdivision-list')
            <a class="dashboard-card grow-1 green" href="/subdivisions">
                <p class="dashboard-card-title">
                    Структурные подразделения
                </p>
                <p class="dashboard-card-icon">
                    <i class="bi bi-diagram-3"></i>
                </p>
            </a>
        @endcan

        @can('subdivision-list')
            <a class="dashboard-card grow-3 green" href="/groups">
                <p class="dashboard-card-title">
                    Учебные группы
                </p>
                <p class="dashboard-card-icon">
                    <i class="bi bi-journal"></i>
                </p>
            </a>
        @endcan

            <a class="dashboard-card grow-1 green" href="/agreement">
                <p class="dashboard-card-title">
                    Согласование экзаменационных билетов
                </p>
                <p class="dashboard-card-icon">
                    <i class="bi bi-ticket"></i>
                </p>
            </a>

    </div>
</div>