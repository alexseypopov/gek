<div class="dashboard">
    <h2 class="dashboard-title">Заседания ГЭК</h2>
    <div class="dashboard-cards">

        <a class="dashboard-card grow-1 green" href="{{ route('meetings.create') }}">
            <p class="dashboard-card-title">
                Создать заседание
            </p>
            <p class="dashboard-card-icon">
                <i class="bi bi-calendar3-event"></i>
            </p>
        </a>

        <a class="dashboard-card grow-2 green" href="/meetings">
            <p class="dashboard-card-title">
                Заседания ГЭК
            </p>
            <p class="dashboard-card-icon">
                <i class="bi bi-calendar3"></i>
            </p>
        </a>

        <a class="dashboard-card grow-1 green" href="/vote">
            <p class="dashboard-card-title">
                Участие в голосовании
            </p>
            <p class="dashboard-card-icon">
                <i class="bi bi-check-square"></i>
            </p>
        </a>

    </div>
</div>
