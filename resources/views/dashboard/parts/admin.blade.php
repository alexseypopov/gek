<div class="dashboard">
    <h2 class="dashboard-title">Администрирование</h2>
    <div class="dashboard-cards">

        @can('user-create')
            <a class="dashboard-card grow-3 blue" href="/users/create">
                <p class="dashboard-card-title">
                    Создать пользователя
                </p>
                <p class="dashboard-card-icon">
                    <i class="bi bi-person-plus"></i>
                </p>
            </a>
        @endcan

        @can('user-list')
            <a class="dashboard-card grow-2 blue" href="/users">
                <p class="dashboard-card-title">
                    Просмотр пользователей
                </p>
                <p class="dashboard-card-icon">
                    <i class="bi bi-people"></i>
                </p>
            </a>
        @endcan

        @can('role-list')
            <a class="dashboard-card grow-1 blue" href="/roles">
                <p class="dashboard-card-title">
                    Роли пользователей
                </p>
                <p class="dashboard-card-icon">
                    <i class="bi bi-gear"></i>
                </p>
            </a>
        @endcan

    </div>
</div>