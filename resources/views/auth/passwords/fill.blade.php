@extends('layouts.app')

@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2 class="mb-3">Предоставление доступа к системе</h2>
                        <p class="pl-3">Для получения доступа к системе Вам необходимо задать пароль для входа.</p>
                    </div>
                </div>
            </div>

            <form method="POST" action="{{ route('password.fill.post', [ 'token' => $token, 'email' => $email ]) }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">Адрес электронной почты</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                               value="{{ old('email') ? old('email') : $email }}" required autocomplete="email"
                               placeholder="Адрес электронной почты" disabled>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row" style="display: none;">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">Пароль</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Подтверждение пароля</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4"></div>

                    <div class="col-md-6">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" required value="" id="flexCheckDefault">
                            <label class="form-check-label" for="flexCheckDefault">
                                Нажимая на кнопку "Задать пароль" Вы даете свое согласие на обработку персональной информации в соответствии с <a href="https://www.consultant.ru/document/cons_doc_LAW_61801/" target="_blank">Федеральным заканом №152-ФЗ от 27.07.2006 "О персональных данных"</a>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group row mb-0 mt-5 ">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-outline-primary">
                            Задать пароль
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection
