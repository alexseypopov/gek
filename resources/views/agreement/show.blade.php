@extends('layouts.app')
@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('agreement.show', $meeting) }}
@endsection
@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Экзаменационные билеты группы <a href="/groups/{{ $meeting->group_id }}" target="_blank">{{ $meeting->group_name }}</a></h2>
                        <h3 class="text-secondary">{{ $meeting->getTicketStatus() }}</h3>
                        <p>Дата согласования экзаменационных билетов: {{ $meeting->normal_date_agree_tickets }}</p>
                    </div>
                </div>
            </div>

            <div class="mb-3">
                <div class="dashboard">
                    <div class="dashboard-cards">
                            <confirm-link
                                    class="dashboard-card grow-fix green"
                                    ok_title="Подтвердить"
                                    ok_variant="primary"
                                    title="Согласовать билеты"
                                    text="Вы уверены, что хотите согласовать билеты?"
                                    href="/agreement/{{ $meeting->id }}/success">
                                <p class="dashboard-card-title">
                                    Согласовать билеты
                                </p>
                                <p class="dashboard-card-icon">
                                    <i class="bi bi-check-lg"></i>
                                </p>
                            </confirm-link>
                            <confirm-link
                                    class="dashboard-card grow-fix red"
                                    ok_title="Подтвердить"
                                    ok_variant="danger"
                                    title="Отправить на доработку"
                                    text="Вы уверены, что хотите отправить билеты на доработку?"
                                    href="/agreement/{{ $meeting->id }}/fail">
                                <p class="dashboard-card-title">
                                    Отправить на доработку
                                </p>
                                <p class="dashboard-card-icon">
                                    <i class="bi bi-x-lg"></i>
                                </p>
                            </confirm-link>
                    </div>
                </div>
            </div>

            <div class="mb-3">
                <div class="dashboard">
                    <div class="dashboard-cards">
                        @if($meeting->tickets_status == \App\Models\Meeting::TICKET_CREATED_SLUG || empty($meeting->tickets_status))
                            <a class="dashboard-card grow-fix green" v-b-modal.modal-ticket-create>
                                <p class="dashboard-card-title">
                                    Добавить билет
                                </p>
                                <p class="dashboard-card-icon">
                                    <i class="bi bi-ticket"></i>
                                </p>
                            </a>
                            <b-modal id="modal-ticket-create" title="Создание билета" hide-footer>
                                <form action="/meetings/{{ $meeting->id }}/tickets/create">
                                    <label for="number">№ билета</label>
                                    <div class="input-group mb-3">
                                        <input name="number" type="text" class="form-control" placeholder="" aria-label="number" aria-describedby="basic-addon1" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Создать билет</button>
                                    </div>
                                </form>
                            </b-modal>
                        @endif
                        @if($meeting->tickets_status == \App\Models\Meeting::TICKET_CREATED_SLUG || empty($meeting->tickets_status))
                            <confirm-link
                                    class="dashboard-card grow-fix green"
                                    ok_title="Подтвердить"
                                    ok_variant="primary"
                                    title="Отправить билеты на согласование"
                                    text="Вы уверены, что хотите отправить билеты на согласование? После подтверждения, редактирование списка билетов будет невозможно."
                                    redirect="/meetings/{{ $meeting->id }}"
                                    href="/meetings/{{ $meeting->id }}/ticketsConfirm">
                                <p class="dashboard-card-title">
                                    Согласовать билеты
                                </p>
                                <p class="dashboard-card-icon">
                                    <i class="bi bi-check-lg"></i>
                                </p>
                            </confirm-link>
                        @endif
                    </div>
                </div>
            </div>

            <hr>

            @foreach($meeting->tickets as $ticket)
                <div class="row mt-3 border-bottom pb-3 mb-3">
                    <div class="col-md-7">
                        <h3>Билет №{{ $ticket->number }}</h3>

                        @foreach($ticket->tasks as $task)
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="mb-0">Задание №{{ $task->number }}</p>
                                    <p class="indent-25">{{ $task->text }}</p>
                                    @if(!empty($task->img))
                                        <img src="{{ $task->img }}" alt="" style="max-width: 100%;">
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-5">
                        <div class="dashboard">
                            <div class="dashboard-cards" style="justify-content: flex-end;">
                                <a class="dashboard-card grow-fix blue" href="/meetings/{{ $meeting->id }}/tickets/{{ $ticket->id }}/download">
                                    <p class="dashboard-card-title">
                                        Скачать билет
                                    </p>
                                    <p class="dashboard-card-icon">
                                        <i class="bi bi-file-earmark-arrow-down"></i>
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>


@endsection
