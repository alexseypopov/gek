@extends('layouts.app')

@section('breadcrumbs')
    {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('agreement') }}
@endsection

@section('content')

    <div class="mt-4">
        <div class="content content-box">

            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>Согласование экзаменационных билетов</h2>
                    </div>
                </div>
            </div>

            @if($meetings->count())
                @foreach($meetings as $key => $meeting)
                    <div class="row group-row">
                        <div class="col-md-1 text-center"><h3>{{ $loop->iteration }}</h3></div>
                        <div class="col-md-11">
                            <h3 class="mb-1">{{ $meeting->group_name }}</h3>
                            <div class="mb-3">
                                <div class="dashboard mt-0">
                                    <div class="dashboard-cards">
                                        <a class="dashboard-card grow-fix green" href="{{ route('agreement.show', $meeting) }}">
                                            <p class="dashboard-card-title">
                                                Список билетов
                                            </p>
                                            <p class="dashboard-card-icon">
                                                <i class="bi bi-clipboard"></i>
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

            @else
                <p class="text-center text-secondary mt-5">Список пуст</p>
            @endif


        </div>
    </div>
@endsection
