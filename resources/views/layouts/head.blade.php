@once
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Yandex -->
        <meta name="yandex-verification" content="{{ config('app.yandex-verification', '') }}" />

        <title>{{ config('app.name', 'Заседания ГЭК') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{ asset('js/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ asset('js/jquery.cecutient.js') }}"></script>
        <script src="{{ asset('js/name.js') }}"></script>

        <!-- Fonts -->
    {{--<link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">--}}

    <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/jquery.cecutient.css') }}" rel="stylesheet">

        <script type="text/javascript">
            jQuery(function($) {
                $('#cecutient').cecutient({
                    increment: 1,
                    target: "#app main",
                    imageClass: ".cecutientImagesOn",
                    minimumFontSize : 0,
                    maximumFontSize : 10,
                    skipForFont: ".skip",
                    container: "#panelWrapper",
                    containerPath: "/templates",
                    language: 'en'
                });
            });
        </script>
    </head>
@endonce
