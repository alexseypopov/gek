<nav class="navbar navbar-expand-md navbar-light bg-white bg-white-please shadow-sm">
    <div class="container">
        {{-- Название + кнопка --}}
        <a class="navbar-brand" href="{{ url('/home') }}">
            {{--<i class="bi bi-mortarboard fs-32 pr-2"></i>--}}
            <img src="/img/mortarboard_4.png" height="64px" alt="">
            {{ config('app.name', 'Заседания ГЭК') }}
        </a>
        <button class="navbar-toggler fs-16" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Навигационная панель') }}">
            <i class="bi bi-list"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <li><a class="nav-link" id="cecutient" href="#"></a></li>
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">Войти</a>
                </li>
                @else
                    {{--<li><a class="nav-link" href="{{ route('users.index') }}">Manage Users</a></li>--}}
                    {{--<li><a class="nav-link" href="{{ route('roles.index') }}">Manage Role</a></li>--}}
                    {{--<li><a class="nav-link" href="{{ route('meetings.index') }}">Manage Meeting</a></li>--}}
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->getFullName() }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item" href="{{ route('profile') }}">
                                Профиль
                            </a>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                Выйти
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
            </ul>
        </div>
    </div>
</nav>
<div id="panelWrapper"></div>

@yield('breadcrumbs')

