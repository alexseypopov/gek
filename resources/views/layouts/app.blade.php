<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.head')

<body>
    <div id="app">
        @include('layouts.header')

        <main>
            @yield('content')
        </main>
        <footer class="footer">
            <p>ВКР студента группы 18-ИСТв-1 Попова Алексея Юрьевича <br> НГТУ им. Р.Е. Алексеева <br> 2022 г.</p>
        </footer>
        {{--
        TODO:: НА СЛУЧАЙ ЕСЛИ НАДО БУДЕТ ПОДТВЕРДИТЬ ПРАВО НА ИСПОЛЬЗОВАНИЕ КАРТИНОК
        <div style="display: none;">
            Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from
            <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
        </div>--}}
    </div>
</body>
</html>
