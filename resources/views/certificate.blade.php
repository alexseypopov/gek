@extends('layouts.app')

@section('content')

    <div class="mt-4">
        <div class="content content-box">
            <div class="row mt-2 mb-2">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2 class="mb-5 pl-2">Сертификат простой электронной подписи</h2>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @guest
                            <h5 class="indent-25">Настоящий сертификат подтверждает, что {{ $sign->user->getShortName() }} подписал простой электронной подписью докумен "{{ $sign->getDocumentName() }}".</h5>
                            <div class="text-center"><img src="{{ url('img/certificate/'.$sign->hash.'.png') }}" alt=""></div>
                            <h5 class="indent-25 mt-3">Для получения более подробной информации необходимо пройти <a href="/login?redirect={{ url('certificate/'.$sign->hash) }}">авторизацию</a>.</h5>
                            @else
                                <h5 class="indent-25">Настоящий сертификат подтверждает, что {{ $sign->user->getShortName() }} подписал простой электронной подписью докумен "{{ $sign->getDocumentName() }}".</h5>
                                <div class="text-center"><img src="{{ url('img/certificate/'.$sign->hash.'.png') }}" alt=""></div>
                                @if($sign->model == 'Test')
                                    <h5 class="indent-25 mt-3">Подпись создана для проведения тестирования.</h5>
                                @elseif($sign->model == \App\Models\Meeting::class)
                                    <div class="dashboard">
                                        <div class="dashboard-cards">
                                            <a class="dashboard-card grow-fix green" href="/meetings/{{ $sign->element_id }}">
                                                <p class="dashboard-card-title">
                                                    Перейти к заседанию ГЭК
                                                </p>
                                                <p class="dashboard-card-icon">
                                                    <i class="bi bi-link"></i>
                                                </p>
                                            </a>
                                        </div>
                                    </div>
                                @elseif($sign->model == \App\Models\MeetingHasReportProtocol::class)
                                    <div class="dashboard">
                                        <div class="dashboard-cards">
                                            <a class="dashboard-card grow-fix green" href="/meetings/{{ \App\Models\Meeting::getMeetingIdByReportId($sign->element_id) }}/">
                                                <p class="dashboard-card-title">
                                                    Перейти к заседанию ГЭК
                                                </p>
                                                <p class="dashboard-card-icon">
                                                    <i class="bi bi-link"></i>
                                                </p>
                                            </a>
                                            <a class="dashboard-card grow-fix green" href="/meetings/{{ \App\Models\Meeting::getMeetingIdByReportId($sign->element_id) }}/report/{{ $sign->element_id }}/download">
                                                <p class="dashboard-card-title">
                                                    Скачать документ
                                                </p>
                                                <p class="dashboard-card-icon">
                                                    <i class="bi bi-link"></i>
                                                </p>
                                            </a>
                                        </div>
                                    </div>
                                @elseif($sign->model == \App\Models\MeetingHasExamProtocol::class)
                                    <div class="dashboard">
                                        <div class="dashboard-cards">
                                            <a class="dashboard-card grow-fix green" href="/meetings/{{ \App\Models\Meeting::getMeetingIdByExamId($sign->element_id) }}/">
                                                <p class="dashboard-card-title">
                                                    Перейти к заседанию ГЭК
                                                </p>
                                                <p class="dashboard-card-icon">
                                                    <i class="bi bi-link"></i>
                                                </p>
                                            </a>
                                            <a class="dashboard-card grow-fix green" href="/meetings/{{ \App\Models\Meeting::getMeetingIdByExamId($sign->element_id) }}/exam/{{ $sign->element_id }}/download">
                                                <p class="dashboard-card-title">
                                                    Скачать документ
                                                </p>
                                                <p class="dashboard-card-icon">
                                                    <i class="bi bi-link"></i>
                                                </p>
                                            </a>
                                        </div>
                                    </div>
                                @elseif($sign->model == \App\Models\Ticket::class)
                                    <div class="dashboard">
                                        <div class="dashboard-cards">
                                            <a class="dashboard-card grow-fix green" href="/meetings/{{ \App\Models\Meeting::getMeetingIdByTicketId($sign->element_id) }}/">
                                                <p class="dashboard-card-title">
                                                    Перейти к заседанию ГЭК
                                                </p>
                                                <p class="dashboard-card-icon">
                                                    <i class="bi bi-link"></i>
                                                </p>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
