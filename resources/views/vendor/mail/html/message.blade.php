@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{ config('app.name') }}
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
    @lang('ВКР студента группы 18-ИСТв-1 Попова Алексея Юрьевича, НГТУ им Р.Е. Алексеева,') {{ date('Y') }}
@endcomponent
@endslot
@endcomponent
