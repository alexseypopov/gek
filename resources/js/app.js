/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import "bootstrap-vue/dist/bootstrap-vue.css";
import "vue-multiselect/dist/vue-multiselect.min.css";

import Vue from 'vue';
import BootstrapVue from "bootstrap-vue";
import axios from "axios";
import VueAxios from "vue-axios";
import Multiselect from "vue-multiselect";

Vue.use(BootstrapVue);
Vue.use(VueAxios, axios);

Vue.directive("can", function(el, binding){
    let hasAccess = false;
    Vue.axios.get('/api/permissions/' + binding.value.permissionName)
        .then((response) => {
            hasAccess = true;
        })
    return hasAccess;
});

Vue.component('multiselect', Multiselect);

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('create-edit-user', require('./components/CreateEditUser.vue').default);
Vue.component('create-edit-role', require('./components/CreateEditRole.vue').default);
Vue.component('loader', require('./components/Loader.vue').default);
Vue.component('confirm-link', require('./components/ConfirmLink.vue').default);
Vue.component('user-select', require('./components/UserSelect.vue').default);
Vue.component('role-user-add', require('./components/RoleUserAdd.vue').default);
Vue.component('create-edit-sub', require('./components/CreateEditSub.vue').default);
Vue.component('create-edit-group', require('./components/CreateEditGroup.vue').default);
Vue.component('create-edit-meeting', require('./components/CreateEditMeeting.vue').default);
Vue.component('create-edit-meeting-students', require('./components/CreateEditMeetingStudents.vue').default);
Vue.component('create-edit-meeting-report', require('./components/CreateEditMeetingReport.vue').default);
Vue.component('create-edit-meeting-exam', require('./components/CreateEditMeetingExam.vue').default);
Vue.component('create-edit-meeting-solution', require('./components/CreateEditMeetingSolution.vue').default);



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
