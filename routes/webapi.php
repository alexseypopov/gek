<?php

use Illuminate\Support\Facades\Route;


// Проверка наличия у пользователя пермишна
Route::get('/api/permissions/{permissionName}', 'ApiController@checkPermission')->where('permissionName', '.*');

// Список ролей
Route::get('/api/roles/list', 'ApiController@getRolesList')->middleware('permission:role-list');
// Список пермишнов
Route::get('/api/permission/list', 'ApiController@getPermissionsList')->middleware('permission:role-list');
// Поиск пользователей
Route::get('/api/users/find', 'ApiController@getUsersFind')->middleware('permission:user-list');

// Поиск пользователей
Route::get('/api/sub/types', 'ApiController@getSubTypes')->middleware('permission:subdivision-list');
// Поиск пользователей
Route::get('/api/sub/chairs', 'ApiController@getChairs')->middleware('permission:group-list');
// Поиск возможных родительских подразделений
Route::get('/api/sub/parents/{level}', 'ApiController@getParents')->middleware('permission:subdivision-list');
//
Route::get('/api/group/levels', 'ApiController@getEduLevels')->middleware('permission:group-list');
//
Route::get('/api/group/forms', 'ApiController@getEduForms')->middleware('permission:group-list');
//
Route::get('/api/group/list', 'ApiController@getGroupList')->middleware('permission:meeting-list');
//
Route::get('/api/meeting/forms', 'ApiController@getMeetingForms')->middleware('permission:meeting-list');
//
Route::get('/api/meeting/controls', 'ApiController@getMeetingControls')->middleware('permission:meeting-list');
//
Route::get('/api/meeting/{meeting}/students', 'ApiController@getMeetingStudents')->middleware('permission:meeting-create');