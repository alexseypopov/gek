<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MeetingController;
use App\Http\Controllers\Auth\ResetPasswordController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return redirect('/home');
});

// Роуты Laravel UI
Auth::routes();
// Редирект роута регистрации
Route::get('/register', function () {
    return redirect('/login');
});
// Предоставление доступа
Route::get('/password/fill/{token}', 'Auth\ResetPasswordController@fill')->name('password.fill');
Route::post('/password/fill/{token}', 'Auth\ResetPasswordController@fillPost')->name('password.fill.post');




Route::get('/home', 'HomeController@index')->middleware('auth')->name('home');
Route::get('/certificate/{hash}', 'HomeController@certificate')->name('certificate');
Route::get('/centificate/{hash}', function ($hash) {
    return redirect('/certificate/'.$hash);
});

Route::group(['middleware' => ['auth']], function() {
    // API
    include('webapi.php');
    
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController', [ 'names' => 'users' ])->middleware('permission:user-edit');;
    Route::get('/users/{user}/auth', 'UserController@auth');

    Route::resource('meetings', 'MeetingController');
    Route::resource('subdivisions', 'SubdividionController');
    Route::resource('groups', 'GroupController');

    // Отвязывание роли
    Route::delete('users/{user}/detachRole/{role}', 'UserController@detachRole')->where('user', '[0-9]+')->where('role', '[0-9]+');
    // Привязка роли
    Route::post('users/{user}/attachRole/{role}', 'UserController@attachRole')->where('user', '[0-9]+')->where('role', '[0-9]+');

    // Настройки
    Route::get('/profile', 'UserController@profile')->name('profile');

    Route::get('/meetings/{meeting}/students', 'MeetingController@students')->name('meetings.students');
    Route::post('/meetings/{meeting}/students', 'MeetingController@postStudents')->middleware('permission:meeting-create');

    Route::get('/meetings/{meeting}/report/{user}', 'MeetingController@report')->name('meetings.report');
    Route::post('/meetings/{meeting}/report/{user}', 'MeetingController@postReport');
    Route::get('/meetings/{meeting}/report/{user}/download', 'MeetingController@downloadReport');

    Route::get('/meetings/{meeting}/exam/{user}', 'MeetingController@exam')->name('meetings.exam');
    Route::post('/meetings/{meeting}/exam/{user}', 'MeetingController@postExam');
    Route::get('/meetings/{meeting}/exam/{user}/download', 'MeetingController@downloadExam');

    Route::get('/meetings/{meeting}/exam/{examProtocol}/votes', 'MeetingController@examVotes');
    Route::get('/meetings/{meeting}/report/{reportProtocol}/votes', 'MeetingController@reportVotes');

    Route::get('/meetings/{meeting}/solution/{user}', 'MeetingController@solution')->name('meetings.solution');
    Route::post('/meetings/{meeting}/solution/{user}', 'MeetingController@postSolution');
    Route::get('/meetings/{meeting}/solution/{user}/download', 'MeetingController@downloadSolution');

    Route::get('/meetings/{meeting}/confirm', 'MeetingController@confirm')->name('meetings.confirm');

    Route::get('/meetings/{meeting}/tickets', 'MeetingController@tickets')->name('meetings.tickets');
    Route::get('/meetings/{meeting}/tickets/create', 'MeetingController@ticketsCreate')->name('meetings.tickets.create');
    Route::get('/meetings/{meeting}/tickets/{ticket}', 'MeetingController@ticketShow')->name('meetings.tickets.show');
    Route::post('/meetings/{meeting}/tickets/{ticket}/task', 'MeetingController@ticketsCreateTask');
    Route::get('/meetings/{meeting}/tickets/{ticket}/download', 'MeetingController@ticketDownload');
    Route::get('/meetings/{meeting}/changeDate', 'MeetingController@changeDate');

    Route::get('/meetings/{meeting}/tickets/{ticket}/task/{task}/delete', 'MeetingController@ticketsDeleteTask');
    Route::get('/meetings/{meeting}/tickets/{ticket}/delete', 'MeetingController@ticketDelete');

    Route::get('meetings/{meeting}/exam/{examProtocol}/detachTicket', 'MeetingController@detachTicket');
    Route::get('meetings/{meeting}/exam/{examProtocol}/attachTicket', 'MeetingController@attachTicket');

    Route::get('meetings/{meeting}/startExam', 'MeetingController@startExam');
    Route::get('meetings/{meeting}/closeExam', 'MeetingController@closeExam');

    Route::get('/meetings/{meeting}/ticketsConfirm', 'MeetingController@ticketsConfirm')->name('meetings.ticketsConfirm');

    Route::get('/agreement', 'MeetingController@agreement')->name('agreement');
    Route::get('/agreement/{meeting}/show', 'MeetingController@agreementShow')->name('agreement.show');
    Route::get('/agreement/{meeting}/success', 'MeetingController@agreementSuccess');
    Route::get('/agreement/{meeting}/fail', 'MeetingController@agreementFail');

    Route::get('/exam/{hash}', 'MeetingController@getTicket');

    Route::get('/vote', 'MeetingController@vote')->name('vote');
    Route::get('/vote/{meeting}/{type}/{user}/{vote}', 'MeetingController@voteSend');





});

//});