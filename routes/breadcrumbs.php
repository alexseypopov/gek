<?php

use Diglactic\Breadcrumbs\Breadcrumbs;

use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

// Главная
Breadcrumbs::for('home', function (BreadcrumbTrail $trail) {
    $trail->push('Главная', route('home'));
});
Breadcrumbs::for('profile', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Профиль', route('profile'));
});

// Пользователи
Breadcrumbs::for('users', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Пользователи', route('users.index'));
});
Breadcrumbs::for('users.create', function (BreadcrumbTrail $trail) {
    $trail->parent('users');
    $trail->push('Добавление нового пользователя', route('users.create'));
});
Breadcrumbs::for('users.show', function (BreadcrumbTrail $trail, $user) {
    $trail->parent('users');
    $trail->push($user->getFullName(), route('users.show', $user));
});
Breadcrumbs::for('users.edit', function (BreadcrumbTrail $trail, $user) {
    $trail->parent('users');
    $trail->push($user->getFullName(), route('users.edit', $user));
});


// Роли
Breadcrumbs::for('roles', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Роли', route('roles.index'));
});
Breadcrumbs::for('roles.create', function (BreadcrumbTrail $trail) {
    $trail->parent('roles');
    $trail->push('Создание новой роли', route('roles.create'));
});
Breadcrumbs::for('roles.edit', function (BreadcrumbTrail $trail, $role) {
    $trail->parent('roles');
    $trail->push($role->title, route('roles.edit', $role));
});
Breadcrumbs::for('roles.show', function (BreadcrumbTrail $trail, $role) {
    $trail->parent('roles');
    $trail->push($role->title, route('roles.show', $role));
});


// Подразделения
Breadcrumbs::for('subdivisions', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Структурные подразделения', route('subdivisions.index'));
});
Breadcrumbs::for('subdivisions.create', function (BreadcrumbTrail $trail) {
    $trail->parent('subdivisions');
    $trail->push('Создание нового подразделения', route('subdivisions.create'));
});
Breadcrumbs::for('subdivisions.edit', function (BreadcrumbTrail $trail, $subdivision) {
    $trail->parent('subdivisions');
    $trail->push($subdivision->name, route('subdivisions.edit', $subdivision));
});
Breadcrumbs::for('subdivisions.show', function (BreadcrumbTrail $trail, $subdivision) {
    $trail->parent('subdivisions');
    $trail->push($subdivision->name, route('subdivisions.show', $subdivision));
});


// Группы
Breadcrumbs::for('groups', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Группы', route('groups.index'));
});
Breadcrumbs::for('groups.create', function (BreadcrumbTrail $trail) {
    $trail->parent('groups');
    $trail->push('Создание новой группы', route('groups.create'));
});
Breadcrumbs::for('groups.edit', function (BreadcrumbTrail $trail, $group) {
    $trail->parent('groups');
    $trail->push($group->name, route('groups.edit', $group));
});
Breadcrumbs::for('groups.show', function (BreadcrumbTrail $trail, $group) {
    $trail->parent('groups');
    $trail->push($group->name, route('groups.show', $group));
});

// Заседания ГЭК
Breadcrumbs::for('meetings', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Заседания ГЭК', route('meetings.index'));
});
Breadcrumbs::for('meetings.create', function (BreadcrumbTrail $trail) {
    $trail->parent('meetings');
    $trail->push('Создание заседания ГЭК', route('meetings.create'));
});
Breadcrumbs::for('meetings.edit', function (BreadcrumbTrail $trail, $meeting) {
    $trail->parent('meetings');
    $trail->push('Заседание ГЭК №'.$meeting->id, route('meetings.edit', $meeting));
});
Breadcrumbs::for('meetings.show', function (BreadcrumbTrail $trail, $meeting) {
    $trail->parent('meetings');
    $trail->push('Заседание ГЭК №'.$meeting->id, route('meetings.show', $meeting));
});
Breadcrumbs::for('meetings.students', function (BreadcrumbTrail $trail, $meeting) {
    $trail->parent('meetings.show', $meeting);
    $trail->push('Допуск студентов', route('meetings.students', $meeting));
});
Breadcrumbs::for('meetings.report', function (BreadcrumbTrail $trail, $meeting) {
    $trail->parent('meetings.show', $meeting);
    $trail->push('Заполнение протокола ВКР/Научного доклада', "/home");
});
Breadcrumbs::for('meetings.exam', function (BreadcrumbTrail $trail, $meeting) {
    $trail->parent('meetings.show', $meeting);
    $trail->push('Заполнение протокола экзамена', "/home");
});
Breadcrumbs::for('meetings.solution', function (BreadcrumbTrail $trail, $meeting) {
    $trail->parent('meetings.show', $meeting);
    $trail->push('Заполнение решения ГЭК', "/home");
});
Breadcrumbs::for('meetings.tickets', function (BreadcrumbTrail $trail, $meeting) {
    $trail->parent('meetings.show', $meeting);
    $trail->push('Экзаменационные билеты', route('meetings.tickets', $meeting));
});
Breadcrumbs::for('meetings.tickets.show', function (BreadcrumbTrail $trail, $meeting, $ticket) {
    $trail->parent('meetings.tickets', $meeting);
    $trail->push('Билет №'.$ticket->number, "/meetings/".$meeting->id."/tickets/".$ticket->id);
});
Breadcrumbs::for('meetings.tickets.showStudent', function (BreadcrumbTrail $trail, $meeting, $ticket) {
    $trail->parent('home');
    $trail->push('Билет №'.$ticket->number);
});

Breadcrumbs::for('agreement', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Согласование экзаменационных билетов', "/agreement");
});
Breadcrumbs::for('agreement.show', function (BreadcrumbTrail $trail, $meeting) {
    $trail->parent('agreement');
    $trail->push('Заседание ГЭК №'.$meeting->id);
});
Breadcrumbs::for('vote', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('Голосование комиссии ГЭК');
});