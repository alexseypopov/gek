<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Предоставление доступа
     * @param Request $request
     * @param null $token
     * @return mixed
     */
    public function fill(Request $request, $token = null)
    {
        if(!PasswordReset::correctToken($token)) {
            abort(400, 'Некорректный токен, попробуйте восстановить пароль');
        }
        return view('auth.passwords.fill')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    /**
     * Обновление пароля
     * @param Request $request
     * @param null $token
     * @return mixed
     */
    public function fillPost (Request $request, $token = null)
    {
        $request->validate($this->rules(), $this->validationErrorMessages());

        if(!PasswordReset::haveTokenAndEmail($token, $request->get('email'))) {
            abort(400, 'Некорректный токен или email');
        }

        $user = User::where('email', $request->get('email'))->first();

        $this->resetPassword($user, $request->get('password'));

        PasswordReset::clearTokens($user);

        return $this->sendResetResponse($request, 'Пароль успешно задан!');
    }
}
