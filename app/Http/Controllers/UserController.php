<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Arr;

class UserController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index','show']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    /**
     * Список пользователей
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::orderBy('id','DESC')->paginate(User::PAGINATE);
        return view('users.index',compact('users'));
    }

    /**
     * Страница создания пользователя
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }

    /**
     * Запрос создания пользователя
     * @param Request $request
     * @return string
     */
    public function store(Request $request) {

        $validate = User::validateBeforeCreate($request->all());

        $messages = [
            'submit' => '', 'lastName' => '', 'firstName' => '',
            'patronymic' => '', 'roles' => '', 'auth' => '',
            'email' => '', 'send' => '',
        ];

        // Отправка сообщения об ошибке
        if($validate->fails()) {
            
            foreach ($validate->getMessageBag()->toArray() as $key => $msgs) {
                $messages[$key] = implode(',', $msgs);
            }

            return json_encode([
                'status' => 'fail',
                'validations' => $messages,
            ]);
        }

        $user = User::create($request->all());

        // Применяем роли
        foreach ($request->get('roles') as $role) {
            $user->assignRole($role);
        }

        if($request->get('send', false)) {
            $user->sendPasswordFillNotification();
        }

        return json_encode([
            'status' => 'success',
            'user_id' => $user->id,
            'user' => $user,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::pluck('name','title')->all();
        $userRole = $user->roles->pluck('name','name')->all();

        return view('users.edit',compact('user','roles','userRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validate = User::validateBeforeUpdate($request->all());

        $messages = [
            'submit' => '', 'lastName' => '', 'firstName' => '',
            'patronymic' => '', 'roles' => '', 'auth' => '',
            'email' => '', 'send' => '',
        ];

        // Отправка сообщения об ошибке
        if($validate->fails()) {

            foreach ($validate->getMessageBag()->toArray() as $key => $msgs) {
                $messages[$key] = implode(',', $msgs);
            }

            return json_encode([
                'status' => 'fail',
                'validations' => $messages,
            ]);
        }
      
        $user->update([
            'firstName' => $request->get('firstName'),
            'lastName' => $request->get('lastName'),
            'patronymic' => $request->get('patronymic'),
            'email' => $request->get('email')
        ]);

        DB::table('model_has_roles')->where('model_id',$user->id)->delete();

        // Применяем роли
        foreach ($request->get('roles') as $role) {
            $user->assignRole($role);
        }

        return json_encode([
            'status' => 'success',
        ]);
    }

    public function auth(User $user)
    {
        Auth::guard()->login($user);
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return true;
    }

    /**
     * Добавляем роль
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function attachRole(User $user, Role $role)
    {
        $user->assignRole($role);
        return true;
    }

    /**
     * Убираем роль
     * @param User $user
     * @param Role $role
     * @return bool
     */
    public function detachRole(User $user, Role $role)
    {
        $user->removeRole($role);
        return true;
    }

    public function profile()
    {
        $user = Auth::user();
        return view('users.profile',compact('user'));
    }
}
