<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\GroupHasStudent;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:group-list|group-create|group-edit|group-delete', ['only' => ['index','show']]);
        $this->middleware('permission:group-create', ['only' => ['create','store']]);
        $this->middleware('permission:group-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:group-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::orderBy('id','DESC')->paginate(Group::PAGINATE);
        return view('groups.index',compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Group::validate($request->all());

        $messages = [
            'chair_id' => '', 'level' => '', 'name' => '',
            'trend' => '', 'profile' => '', 'graduation_year' => '',
            'form' => '', 'students' => '', 'submit' => '',];

        // Отправка сообщения об ошибке
        if($validate->fails()) {
            foreach ($validate->getMessageBag()->toArray() as $key => $msgs) {
                $messages[$key] = implode(',', $msgs);
            }
            return json_encode([
                'status' => 'fail',
                'validations' => $messages,
            ]);
        }

        $group = Group::create([
            'chair_id' => $request->get('chair_id'),
            'level' => $request->get('level'),
            'name' => $request->get('name'),
            'trend' => $request->get('trend'),
            'profile' => $request->get('profile'),
            'graduation_year' => $request->get('graduation_year'),
            'form' => $request->get('form'),
        ]);

        // Применяем роли
        foreach ($request->get('students') as $student_id) {
            GroupHasStudent::addStudent($group->id, $student_id);
        }

        return json_encode([
            'status' => 'success',
            'group_id' => $group->id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        return view('groups.show', compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view('groups.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Group $group)
    {
        $validate = Group::validate($request->all());

        $messages = [
            'chair_id' => '', 'level' => '', 'name' => '',
            'trend' => '', 'profile' => '', 'graduation_year' => '',
            'form' => '', 'students' => '', 'submit' => '',];

        // Отправка сообщения об ошибке
        if($validate->fails()) {
            foreach ($validate->getMessageBag()->toArray() as $key => $msgs) {
                $messages[$key] = implode(',', $msgs);
            }
            return json_encode([
                'status' => 'fail',
                'validations' => $messages,
            ]);
        }

        $group->update([
            'chair_id' => $request->get('chair_id'),
            'level' => $request->get('level'),
            'name' => $request->get('name'),
            'trend' => $request->get('trend'),
            'profile' => $request->get('profile'),
            'graduation_year' => $request->get('graduation_year'),
            'form' => $request->get('form'),
        ]);

        GroupHasStudent::syncStudent($group->id, $request->get('students'));

        return json_encode([
            'status' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $group->deleteMe();
        return true;
    }
}
