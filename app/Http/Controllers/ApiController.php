<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Meeting;
use App\Models\MeetingHasStudent;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Subdividion;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{
    /**
     * Проверка пермишна у пользователя
     * @param Request $request
     * @return mixed
     */
    public function checkPermission(Request $request) {
        return auth()->user()->can($request->permissionName);
    }

    /**
     * Список ролей в системе
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRolesList() {
        return Role::all();
    }

    /**
     * Список пермишнов
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getPermissionsList() {
        return Permission::all();
    }

    public function getUsersFind(Request $request) {
        $response          = array();
        $response['query'] = \Request::get('query', '');

        $users = User::search($request->get('query'))->get();

        //преобразуем в необходимый для рендеринга вид
        foreach ($users as $value)
        {
            if($value)
            {
                $response['suggestions'][] =  array('value' => $value->getFullName().' - '.(empty($value->email) ? 'Без доступа к системе' : $value->email), 'id' => $value->id, 'data' => $value);
            }
        }

        //если ничего не найдено, выводим болванку
        if(!isset($response['suggestions']))
        {
            $response['suggestions'][] = array('value' => 'Пользователь ' . $response['query'] . ' не найден!', '$isDisabled' => true);
        }

        return json_encode($response);
    }

    /**
     * Типы подразделений
     * @return mixed
     */
    public function getSubTypes()
    {
        return Subdividion::$types;
    }

    /**
     * Список возможных родительских подразделений
     * @param $level
     * @return string
     */
    public function getParents($level) {
        return json_encode(Subdividion::getParents($level));
    }

    /**
     * Уровни образования
     * @return mixed
     */
    public function getEduLevels()
    {
        return Group::$levels;
    }

    public function getEduForms()
    {
        return Group::$forms;
    }

    public function getChairs()
    {
        return Subdividion::where('type', Subdividion::CHAIR)->get();
    }

    public function getGroupList()
    {
        $result = [];
        $groups = Group::get();
        foreach ($groups as $group) {
            $result[] = $group->getDataToSelect();
        }

        return $result;
    }

    public function getMeetingForms()
    {
        return Meeting::$forms;
    }

    public function getMeetingControls()
    {
        return Meeting::$controls;
    }

    public function getMeetingStudents(Meeting $meeting)
    {
        $result = [];
        foreach ($meeting->group->students as $student) {

            $fullName_rod = '';
            $fullName_dat = '';
            $fullName_tv = '';
            $allow = $meeting->isAllowStudent($student->id);

            if($allow) {
                $meetingStudent = MeetingHasStudent::where('meeting_id', $meeting->id)->where('user_id', $student->id)->first();
                $fullName_rod = $meetingStudent->fullName_rod;
                $fullName_dat = $meetingStudent->fullName_dat;
                $fullName_tv = $meetingStudent->fullName_tv;
            }

            $result[] = [
                'user_id'       => $student->id,
                'fullName'      => $student->getFullName(),
                'fullName_rod'  => $fullName_rod,
                'fullName_dat'  => $fullName_dat,
                'fullName_tv'   => $fullName_tv,
                'allow_lock'    => $allow,
                'allow'         => $allow,
                'auth_lock'     => !empty($student->email),
                'auth'          => !empty($student->email),
                'mail'          => $student->email,

            ];
        }

        return json_encode($result);
    }
}
