<?php

namespace App\Http\Controllers;

use App\Models\Subdividion;
use App\Models\User;
use Illuminate\Http\Request;

class SubdividionController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:subdivision-list|subdivision-create|subdivision-edit|subdivision-delete', ['only' => ['index','show']]);
        $this->middleware('permission:subdivision-create', ['only' => ['create','store']]);
        $this->middleware('permission:subdivision-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:subdivision-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subdivisions = Subdividion::orderBy('id','ASC')->paginate(Subdividion::PAGINATE);
        return view('subdivisions.index',compact('subdivisions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subdivisions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Subdividion::validate($request->all());

        $messages = [
            'submit' => '', 'type' => '', 'name' => '',
            'head_degree' => '', 'head_id' => '', 'parent_id' => ''];

        // Отправка сообщения об ошибке
        if($validate->fails()) {

            foreach ($validate->getMessageBag()->toArray() as $key => $msgs) {
                $messages[$key] = implode(',', $msgs);
            }

            return json_encode([
                'status' => 'fail',
                'validations' => $messages,
            ]);
        }

        $subdivision = Subdividion::create($request->all());

        $user = User::find($request->get('head_id'));

        $user->assignRole('head_subdivision');

//        // Применяем роли
//        foreach ($request->get('roles') as $role) {
//            $user->assignRole($role);
//        }

        return json_encode([
            'status' => 'success',
            'subdivision_id' => $subdivision->id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subdividion  $subdividion
     * @return \Illuminate\Http\Response
     */
    public function show(Subdividion $subdivision)
    {
        return view('subdivisions.show', compact('subdivision'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subdividion  $subdividion
     * @return \Illuminate\Http\Response
     */
    public function edit(Subdividion $subdivision)
    {
        return view('subdivisions.edit', compact('subdivision'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subdividion  $subdividion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subdividion $subdivision)
    {
        $validate = Subdividion::validate($request->all());

        $messages = [
            'submit' => '', 'type' => '', 'name' => '',
            'head_degree' => '', 'head_id' => '', 'parent_id' => ''];

        // Отправка сообщения об ошибке
        if($validate->fails()) {

            foreach ($validate->getMessageBag()->toArray() as $key => $msgs) {
                $messages[$key] = implode(',', $msgs);
            }

            return json_encode([
                'status' => 'fail',
                'validations' => $messages,
            ]);
        }

        $subdivision->update([
            'type' => $request->get('type'),
            'name' => $request->get('name'),
            'head_degree' => $request->get('head_degree'),
            'head_id' => $request->get('head_id'),
            'parent_id' => $request->get('parent_id')
        ]);


        return json_encode([
            'status' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subdividion  $subdividion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subdividion $subdivision)
    {
        $subdivision->delete();
        return true;
    }
}
