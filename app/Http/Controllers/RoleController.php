<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('permission:role-create', ['only' => ['create','store']]);
        $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::orderBy('id','ASC')->paginate(Role::PAGINATE);
        return view('roles.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::get();
        return view('roles.create',compact('permission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Role::validateBeforeCreate($request->all());
        $messages = ['submit' => '', 'title' => '', 'name' => '', 'description' => '', 'permissions' => ''];

        // Отправка сообщения об ошибке
        if($validate->fails()) {

            foreach ($validate->getMessageBag()->toArray() as $key => $msgs) {
                $messages[$key] = implode(',', $msgs);
            }

            return json_encode([
                'status' => 'fail',
                'validations' => $messages,
            ]);
        }

        $role = Role::create(['name' => $request->input('name'), 'description' => $request->input('description'), 'title' => $request->input('title')]);

        $role->syncPermissions($request->input('permissions'));

        return json_encode([
            'status' => 'success',
            'role_id' => $role->id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$role->id)
            ->get();

        return view('roles.show',compact('role','rolePermissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $permission = Permission::get();
        $rolePermissions = $role->permissions()->get();

        return view('roles.edit',compact('role','permission','rolePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $validate = Role::validateBeforeUpdate($request->all());
        $messages = ['submit' => '', 'title' => '', 'name' => '', 'description' => '', 'permissions' => ''];

        // Отправка сообщения об ошибке
        if($validate->fails()) {

            foreach ($validate->getMessageBag()->toArray() as $key => $msgs) {
                $messages[$key] = implode(',', $msgs);
            }

            return json_encode([
                'status' => 'fail',
                'validations' => $messages,
            ]);
        }

        $role->name = $request->input('name');
        $role->title = $request->input('title');
        $role->description = $request->input('description');
        $role->save();

        $role->syncPermissions($request->input('permissions'));

        return json_encode([
            'status' => 'success',
            'role_id' => $role->id,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();
        return true;
    }
}
