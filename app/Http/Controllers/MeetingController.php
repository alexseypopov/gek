<?php

namespace App\Http\Controllers;

use App\Models\MeetingHasSolution;
use App\Models\Comission;
use App\Models\Group;
use App\Models\Meeting;
use App\Models\MeetingHasExamProtocol;
use App\Models\MeetingHasReportProtocol;
use App\Models\MeetingHasStudent;
use App\Models\Sign;
use App\Models\Subdividion;
use App\Models\Ticket;
use App\Models\TicketTask;
use App\Models\User;
use App\Models\Vote;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic;

class MeetingController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:meeting-list|meeting-create|meeting-edit|meeting-delete', ['only' => ['index','show']]);
        $this->middleware('permission:meeting-create', ['only' => ['create','store']]);
        $this->middleware('permission:meeting-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:meeting-delete', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::orderBy('id','DESC')->paginate(Group::PAGINATE);
        //$meetings = Meeting::orderBy('id','DESC')->paginate(Meeting::PAGINATE);
        return view('meetings.index',compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('meetings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Meeting::validate($request->all());

        $messages = [
            'submit' => '', 'group_id'  => '', 'sub'  => '', 'qualification'  => '', 'comment'  => '', 'chairman'  => '', 'secretary'  => ''
        ];

        // Отправка сообщения об ошибке
        if($validate->fails()) {

            foreach ($validate->getMessageBag()->toArray() as $key => $msgs) {
                $messages[$key] = implode(',', $msgs);
            }

            return json_encode([
                'status' => 'fail',
                'validations' => $messages,
            ]);
        }

        $sub = [
            Subdividion::CHAIR => null,
            Subdividion::FACULTY => null,
            Subdividion::INST => null,
            Subdividion::UMU => null,
        ];
        foreach ($request->get('sub') as $item) {
           if(isset($item['type'])) {
               $sub[$item['type']] = $item['id'];
           }
        }

        $meeting = Meeting::create([
            'chair_id' => $sub[Subdividion::CHAIR],
            'faculty_id' => $sub[Subdividion::FACULTY],
            'inst_id' => $sub[Subdividion::INST],
            'umu_id' => $sub[Subdividion::UMU],
            'group_id' => $request->get('group_id'),
            'group_name' => $request->get('group_name'),
            'status' => Meeting::STATUS_CREATED_SLUG,
            'sub' => json_encode($request->get('sub')),
            'qualification' => $request->get('qualification'),
            'comment'  => $request->get('comment'),
            'date_document'  => $request->get('date_document'),
            'number_document'  => $request->get('number_document'),
            'exam'  => $request->get('exam'),
            'report'  => $request->get('report')
        ]);

        $chairman = Comission::createPerson($meeting->id, Comission::TYPE_CHAIRMAN_SLUG, $request->get('chairman')['id'], $request->get('chairman_distance', 'false') == 'true' ? 1 : 0);
        $secretary = Comission::createPerson($meeting->id, Comission::TYPE_SECRETARY_SLUG, $request->get('secretary')['id'], $request->get('secretary_distance', 'false') == 'true' ? 1 : 0);

        foreach ($request->get('comission') as $person) {
            Comission::createPerson($meeting->id, Comission::TYPE_PERSON_SLUG, $person['id'], isset($person['distance']) ? ($person['distance'] == 'true' ? 1 : 0) : 0);
        }

        //$user = User::find($request->get('head_id'));

        //$user->assignRole('head_subdivision');

//        // Применяем роли
//        foreach ($request->get('roles') as $role) {
//            $user->assignRole($role);
//        }

        return json_encode([
            'status' => 'success',
            'subdivision_id' => $meeting->id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Meeting  $meeting
     * @return \Illuminate\Http\Response
     */
    public function show(Meeting $meeting)
    {
        return view('meetings.show',compact('meeting'));
    }

    public function students(Meeting $meeting)
    {
        return view('meetings.students',compact('meeting'));
    }

    public function postStudents(Meeting $meeting, Request $request)
    {
        $validations = [ 'submit' => '' ];
        $errors = false;

        foreach ($request->all() as $student) {

            $validations[$student['user_id']] = [
                'allow' => '',
                'auth' => '',
                'send' => '',
                'email' => '',
                'fullName_rod' => '',
                'fullName_dat' => '',
                'fullName_tv' => '',
            ];

            if(!$student['allow_lock'] && !$student['allow']) {
                continue;
            }
            elseif($student['allow_lock'] && !$student['allow']) {
                $result = MeetingHasStudent::deleteStudent($meeting->id, $student['user_id']);
                if($result['status'] != 'success') {
                    $validations[$student['user_id']] = $result['validations'];
                    $errors = true;
                }
            } elseif (!$student['allow_lock'] && $student['allow']) {
                $result = MeetingHasStudent::createStudent($meeting->id, $student);
                if($result['status'] != 'success') {
                    $validations[$student['user_id']] = $result['validations'];
                    $errors = true;
                }
                if(!$student['auth_lock'] && $student['auth'] && $result['status'] == 'success') {
                    $result = MeetingHasStudent::changeUserAccess($student['user_id'], $student['send'], $student['email']);
                    if($result['status'] != 'success') {
                        $validations[$student['user_id']] = $result['validations'];
                        $errors = true;
                    }
                }
            } else {
                $result = MeetingHasStudent::updateStudent($meeting->id, $student);
                if($result['status'] != 'success') {
                    $validations[$student['user_id']] = $result['validations'];
                }
                if(!$student['auth_lock'] && $student['auth'] && $result['status'] == 'success') {
                    $result = MeetingHasStudent::changeUserAccess($student['user_id'], $student['send'], $student['email']);
                    if($result['status'] != 'success') {
                        $validations[$student['user_id']] = $result['validations'];
                        $errors = true;
                    }
                }
            }
        }

        if($errors) {
            return json_encode([
                'status' => 'fail',
                'validations' => $validations,
            ]);
        }

        return json_encode([
            'status' => 'success',
        ]);
    }
    
    public function confirm(Meeting $meeting)
    {
        if($meeting->status == Meeting::STATUS_CREATED_SLUG) {
            $meeting->status = Meeting::STATUS_CONFIRMED_SLUG;
            $meeting->save();

            foreach ($meeting->students as $student) {
                if($meeting->report) {
                    $student->createReportProtocol();
                }
                if($meeting->exam) {
                    $student->createExamProtocol();
                }
                $student->createSolution();
            }
        }

        return redirect("/meetings/".$meeting->id);
    }

    public function report(Meeting $meeting, User $user)
    {
        $protocol = MeetingHasReportProtocol::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();

        $student = MeetingHasStudent::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();

        return view('meetings.report',compact('meeting', 'user', 'protocol', 'student'));
    }

    public function postReport(Meeting $meeting, User $user, Request $request)
    {
        $data = $request->all();
        $data['excelent'] = $data['excelent'] ? 1 : 0;

        $protocol = MeetingHasReportProtocol::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();

        if($protocol->status == MeetingHasReportProtocol::STATUS_CREATED_SLUG) {
            $data['status'] = MeetingHasReportProtocol::STATUS_FILL_SLUG;
        }
        if(empty(Sign::getHash(MeetingHasReportProtocol::class, $protocol->id, auth()->user()->id))) {
            Sign::sign(MeetingHasReportProtocol::class, $protocol->id, auth()->user()->id);
        }

        $protocol->update($data);

        return json_encode([
            'status' => 'success',
        ]);
    }

    public function downloadReport(Meeting $meeting, User $user, Request $request)
    {
       $report = MeetingHasReportProtocol::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();

        return $report->download();
    }

    public function downloadExam(Meeting $meeting, User $user, Request $request)
    {
        $report = MeetingHasExamProtocol::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();

        return $report->download();
    }
    public function downloadSolution(Meeting $meeting, User $user)
    {
        $report = MeetingHasSolution::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();

        return $report->download();
    }
    public function ticketDownload(Meeting $meeting, Ticket $ticket)
    {
        return $ticket->download();
    }
    public function changeDate(Meeting $meeting, Request $request)
    {
        $meeting->date_agree_tickets = $request->get('date_agree_tickets');
        $meeting->save();
        return redirect('/meetings/'.$meeting->id.'/tickets/');
    }

    public function exam(Meeting $meeting, User $user)
    {
        $protocol = MeetingHasExamProtocol::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();

        $student = MeetingHasStudent::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();

        return view('meetings.exam',compact('meeting', 'user', 'protocol', 'student'));
    }

    public function postExam(Meeting $meeting, User $user, Request $request)
    {
        $data = $request->all();

        $protocol = MeetingHasExamProtocol::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();

        if($protocol->status == MeetingHasExamProtocol::STATUS_CREATED_SLUG) {
            $data['status'] = MeetingHasExamProtocol::STATUS_FILL_SLUG;
        }
        if(empty(Sign::getHash(MeetingHasExamProtocol::class, $protocol->id, auth()->user()->id))) {
            Sign::sign(MeetingHasExamProtocol::class, $protocol->id, auth()->user()->id);
        }

        $protocol->update($data);

        return json_encode([
            'status' => 'success',
        ]);
    }

    public function solution(Meeting $meeting, User $user)
    {
        $protocol = MeetingHasSolution::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();

        $student = MeetingHasStudent::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();

        return view('meetings.solution',compact('meeting', 'user', 'protocol', 'student'));
    }

    public function postSolution(Meeting $meeting, User $user, Request $request)
    {
        $data = $request->all();

        $protocol = MeetingHasSolution::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();

        if($protocol->status == MeetingHasSolution::STATUS_CREATED_SLUG) {
            $data['status'] = MeetingHasSolution::STATUS_FILL_SLUG;
            Sign::sign(MeetingHasSolution::class, $protocol->id, auth()->user()->id);
        }

        $protocol->update($data);

        return json_encode([
            'status' => 'success',
        ]);
    }

    public function tickets(Meeting $meeting)
    {
        return view('meetings.tickets.index',compact('meeting'));
    }

    public function ticketsCreate(Meeting $meeting, Request $request)
    {
        $ticket = $meeting->createTicket($request->get('number'));
        return redirect('/meetings/'.$meeting->id.'/tickets/'.$ticket->id);
    }
    public function ticketsDeleteTask(Meeting $meeting, Ticket $ticket, TicketTask $task)
    {
        $place = $task->number;
        $list = $ticket->tasks()->where('number', '>', $place)->orderBy('number', 'asc')->get();
        foreach ($list as $element) {
            $element->decrement('number');
        }

        $task->delete();
        return redirect('/meetings/'.$meeting->id.'/tickets/'.$ticket->id);
    }
    public function ticketDelete(Meeting $meeting, Ticket $ticket)
    {
        $ticket->tasks()->delete();
        $ticket->delete();
        return redirect('/meetings/'.$meeting->id.'/tickets');
    }

    public function ticketShow(Meeting $meeting, Ticket $ticket)
    {
        return view('meetings.tickets.show',compact('meeting', 'ticket'));
    }

    public function ticketsCreateTask(Meeting $meeting, Ticket $ticket, Request $request) {
        $ticket->createTask($request);
        return redirect('/meetings/'.$meeting->id.'/tickets/'.$ticket->id);
    }

    public function ticketsConfirm(Meeting $meeting)
    {
        if($meeting->tickets_status == Meeting::TICKET_CREATED_SLUG || empty($meeting->tickets_status)) {
            $meeting->tickets_status = Meeting::TICKET_WAIT_CONFIRM_SLUG;
            $meeting->save();
        }

        return redirect('/meetings/'.$meeting->id);
    }

    public function detachTicket(Meeting $meeting, MeetingHasExamProtocol $examProtocol)
    {
        $examProtocol->ticket_id = null;
        $examProtocol->save();
        return redirect('/meetings/'.$meeting->id.'#student_'.$examProtocol->user_id);
    }
    public function attachTicket(Meeting $meeting, MeetingHasExamProtocol $examProtocol, Request $request)
    {
        $ticket = Ticket::findOrFail($request->get('ticket_id'));
        $ticket->increment('counter');

        $examProtocol->ticket_id = $ticket->id;
        $examProtocol->save();

        return redirect('/meetings/'.$meeting->id.'#student_'.$examProtocol->user_id);
    }

    public function startExam(Meeting $meeting)
    {
        $meeting->startExam();
        return redirect('/meetings/'.$meeting->id);
    }
    public function closeExam(Meeting $meeting)
    {
        $meeting->closeExam();
        return redirect('/meetings/'.$meeting->id);
    }

    public function agreement(Request $request)
    {
        $type = $request->get('type');
        $types = [Meeting::TICKET_WAIT_CONFIRM_SLUG, Meeting::TICKET_CONFIRMED_CHAIR_SLUG];
        if($type == 'chair') {
            $types = [Meeting::TICKET_WAIT_CONFIRM_SLUG];
        } elseif($type == 'sub') {
            $types = [Meeting::TICKET_CONFIRMED_CHAIR_SLUG];
        }

        $meetings = Meeting::whereIn('tickets_status', $types)->get();
        return view('agreement.index',compact('meetings'));
    }

    public function agreementShow(Meeting $meeting)
    {
        return view('agreement.show',compact('meeting'));
    }

    public function agreementSuccess(Meeting $meeting)
    {
        if($meeting->tickets_status == Meeting::TICKET_WAIT_CONFIRM_SLUG) {
            $meeting->tickets_status = Meeting::TICKET_CONFIRMED_CHAIR_SLUG;
            Sign::sign(Meeting::class, $meeting->id, auth()->user()->id);
        } elseif ($meeting->tickets_status == Meeting::TICKET_CONFIRMED_CHAIR_SLUG) {
            $meeting->tickets_status = Meeting::TICKET_CONFIRMED_SLUG;
            Sign::sign(Meeting::class, $meeting->id, auth()->user()->id);
        }
        $meeting->save();
        return redirect('/agreement');
    }

    public function agreementFail(Meeting $meeting)
    {
        $meeting->tickets_status = Meeting::TICKET_CREATED_SLUG;
        $meeting->save();
        return redirect('/agreement');
    }

    public function examVotes(Meeting $meeting, MeetingHasExamProtocol $examProtocol, Request $request)
    {
        $examProtocol->yes = $request->get('yes', 0);
        $examProtocol->no = $request->get('no', 0);
        $examProtocol->save();
        return redirect('/meetings/'.$examProtocol->meeting_id);
    }

    public function reportVotes(Meeting $meeting, MeetingHasReportProtocol $reportProtocol, Request $request)
    {
        $reportProtocol->yes = $request->get('yes', 0);
        $reportProtocol->no = $request->get('no', 0);
        $reportProtocol->save();
        return redirect('/meetings/'.$reportProtocol->meeting_id);
    }

    public function vote()
    {
        $result = [];

        $meetings = Comission::where('user_id', auth()->user()->id)->get();
        foreach ($meetings as $meeting) {
            foreach ($meeting->meeting->students as $student) {
                //if($student->distance) {
                    if($meeting->meeting->exam) {
                        if( !Vote::where('model', MeetingHasExamProtocol::class)->where('meeting_id', $meeting->meeting->id)->where('student_id', $student->user_id)->where('user_id', auth()->user()->id)->exists() ) {
                            if(MeetingHasExamProtocol::where('meeting_id', $meeting->meeting->id)->where('user_id', $student->user_id)->first()->status != MeetingHasExamProtocol::STATUS_CREATED_SLUG) {
                                $result[$student->id]['exam'] = [
                                    'meeting' => $meeting->meeting,
                                    'student' => $student,
                                ];
                            }
                        }
                    }
                    if($meeting->meeting->report) {
                        if( !Vote::where('model', MeetingHasReportProtocol::class)->where('meeting_id', $meeting->meeting->id)->where('student_id', $student->user_id)->where('user_id', auth()->user()->id)->exists() ) {
                           if(MeetingHasReportProtocol::where('meeting_id', $meeting->meeting->id)->where('user_id', $student->user_id)->first()->status != MeetingHasReportProtocol::STATUS_CREATED_SLUG) {
                               $result[$student->id]['report'] = [
                                   'meeting' => $meeting->meeting,
                                   'student' => $student,
                               ];
                           }
                        }
                    }
               // }
            }
        }

        return view('meetings.vote',compact('result'));
    }

    public function voteSend(Meeting $meeting, $type, User $user, $vote)
    {
        if($type == 'exam') {
            $model = MeetingHasExamProtocol::class;
            $protocol = MeetingHasExamProtocol::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();
        } else {
            $model = MeetingHasReportProtocol::class;
            $protocol = MeetingHasReportProtocol::where('meeting_id', $meeting->id)->where('user_id', $user->id)->first();
        }

        Vote::create([
            'model' => $model,
            'element_id'  => $protocol->id,
            'meeting_id'  => $meeting->id,
            'student_id'  => $user->id,
            'user_id'  => auth()->user()->id,
            'vote'  => $vote == 'success' ? 1 : 0
        ]);

        Sign::sign($model, $protocol->id, auth()->user()->id);

        return redirect('/vote');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Gek\Meeting  $meeting
     * @return \Illuminate\Http\Response
     */
    public function edit(Meeting $meeting)
    {
        return view('meetings.edit',compact('meeting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Gek\Meeting  $meeting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Meeting $meeting)
    {
        request()->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);

        $meeting->update($request->all());

        return redirect()->route('meetings.index')
            ->with('success','Meeting updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Gek\Meeting  $meeting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Meeting $meeting)
    {
        $meeting->delete();

        return redirect()->route('meetings.index')
            ->with('success','Meeting deleted successfully');
    }

    public function getTicket($hash)
    {
        $meeting = Meeting::where('exam_hash', $hash)->firstOrFail();
        $protocol = MeetingHasExamProtocol::where('meeting_id', $meeting->id)->where('user_id', auth()->user()->id)->firstOrFail();

        if(empty($protocol->ticket_id)) {
            $ticket = $protocol->getTicket();
        } else {
            $ticket = $protocol->ticket;
        }

        return view('meetings.tickets.showStudent',compact('meeting', 'ticket'));
    }
}
