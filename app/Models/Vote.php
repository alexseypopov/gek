<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = [
        'id', 'model', 'element_id', 'meeting_id', 'student_id', 'user_id', 'vote'
    ];
}
