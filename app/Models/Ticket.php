<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\TemplateProcessor;

class Ticket extends Model
{
    protected $fillable = [
        'id',
        'meeting_id',
        'number',
        'counter',
        'sign_chair',
        'sign_sub',
    ];

    public function tasks()
    {
        return $this->hasMany(TicketTask::class,'ticket_id', 'id');
    }
    public function meeting()
    {
        return $this->hasOne(Meeting::class,'id', 'meeting_id');
    }

    public function createTask(Request $request)
    {
        $number = $this->tasks()->max('number') + 1;

        $task = TicketTask::create([
            'meeting_id'  => $this->meeting_id,
            'ticket_id'   => $this->id,
            'number'    => $number,
            'text'      => $request->get('text'),
        ]);

        if($request->hasFile('img')) {

            $img = $request->file('img');

            $filePath = '/img/tasks/' . $task->id . '.' . $img->getClientOriginalExtension();

            $request->file('img')->storeAs('img/tasks/', $task->id . '.' . $img->getClientOriginalExtension());

            $task->img = $filePath;
            $task->save();
        }

            return $task;
        }

        public function download()
        {
 //           $protocol = $this->getProtocol();

            // Вставка данных с проверкой на наличее
            function setValue($template, $position, $value) {
                if(empty($value) || is_array($value)) {
                    $value = '';
                }
                // Вставка значения
                $template->setValue($position, htmlspecialchars($value));
            }
            //
            $chair = ''; $zav = ''; $zav_id = ''; $zav_fio = '';
            $rector = ''; $rector_id = ''; $rector_fio = '';
            foreach (json_decode($this->meeting->sub) as $sub) {
                if(!empty($sub->type) && $sub->type == Subdividion::CHAIR) {
                    $chair   = $sub->name;
                    $zav = $sub->head_degree;
                    $zav_fio = $sub->head_fio;
                    $zav_id = $sub->head_id;

                } elseif (!empty($sub->type) && $sub->type == Subdividion::UMU) {
                    $rector = $sub->head_degree;
                    $rector_fio = $sub->head_fio;
                    $rector_id = $sub->head_id;
                }
            }

            $exam   = $this->meeting->group->trend;
            $chair2   = $this->meeting->group->profile;
            $number   = $this->number;

//            $prorectorSign = Sign::getSign(Sign::SECTION_GEK_TICKET,$this->id, Sign::TYPE_PRORECTOR_UMD ) ;
//
//            $rector   = Sign::$types_sign[Sign::TYPE_PRORECTOR_UMD];
//            $rector_sign   = empty($prorectorSign) ? '' : $prorectorSign->hash;
//            $rector_fio   = empty($prorectorSign) ? 'А. А. Толстенева' : $prorectorSign->getAuthorShort();
//
//            $zav   = 'Заведующий кафедрой';
//            $zav_sign   = '';
//            $zav_fio   = $this->geksGroup->chair->chief->user->getShortName();

            $day    = Meeting::getNormalDate($this->meeting->date_agree_tickets, 'd');
            $month  = Meeting::getNormalDate($this->meeting->date_agree_tickets, 'm');
            $year   = Meeting::getNormalDate($this->meeting->date_agree_tickets, 'y');

            // Создание файла
            $templatePath = 'templates/ticket.docx';
            $filename     = public_path($templatePath);
            $template = new TemplateProcessor($filename);



            setValue($template, 'chair',        $chair);
            setValue($template, 'exam',        $exam);
            setValue($template, 'chair2',        $chair2);
            setValue($template, 'number',        $number);
//            setValue($template, 'rector',        $rector);
//            setValue($template, 'rector_sign',        $rector_sign);
            setValue($template, 'rector_fio',        $rector_fio);
//            setValue($template, 'zav',        $zav);
//            setValue($template, 'zav_sign',        $zav_sign);
            setValue($template, 'zav_fio',        $zav_fio);


            setValue($template, 'rector',          $rector);
            $rector_hash = Sign::getHash(Meeting::class, $this->id, $rector_id);
            $rector_hash_img = Sign::haveImg($rector_hash);
            if($rector_hash_img) {
                $template->setImageValue('rector_sign', array('path' => public_path('img/certificate/'.$rector_hash.'.png'), 'width' => 300, 'ratio' => false));
            } else {
                setValue($template, 'rector_sign', '');
            }


            setValue($template, 'zav',          $zav);
            $zav_hash = Sign::getHash(Meeting::class, $this->id, $zav_id);
            $zav_hash_img = Sign::haveImg($zav_hash);
            if($zav_hash_img) {
                $template->setImageValue('zav_sign', array('path' => public_path('img/certificate/'.$zav_hash.'.png'), 'width' => 300, 'ratio' => false));
            } else {
                setValue($template, 'zav_sign', '');
            }

            setValue($template, 'day',        $day);
            setValue($template, 'month',        $month);
            setValue($template, 'year',        $year);

            $template->cloneBlock('block',$this->tasks()->count(), true, true);

            foreach ($this->tasks as $taskIndex => $task) {
                $taskNum = $taskIndex + 1;

                setValue($template, 'num#'.$taskNum, $task->number);
                setValue($template, 'text#'.$taskNum, $task->text);

                if(!empty($task->img)) {
                    $template->setImageValue('img#'.$taskNum, array('path' => public_path($task->img), 'width' => 300, 'height' => 200, 'ratio' => false));
                }  else { setValue($template, 'img#'.$taskNum, ''); }
            }


            //
            header('Content-Disposition: inline; filename="Экзаменационный билет №'.$number.'.docx"');
            header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            $template->saveAs('php://output');
            //
            exit;
        }



}
