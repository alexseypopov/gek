<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupHasStudent extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'group_id', 'user_id'
    ];


    public static function addStudent($group_id, $student_id)
    {
        if(!self::where('group_id', $group_id)->where('user_id', $student_id)->exists()) {

            $user = User::find($student_id);
            $user->assignRole('student');

            self::create([
                'group_id' => $group_id,
                'user_id' => $student_id
            ]);
            return true;
        }

        return false;
    }

    public static function syncStudent($group_id, $students) {
        self::where('group_id', $group_id)->whereNotIn('user_id', $students)->delete();
        foreach ($students as $student_id) {
            self::updateOrCreate([
                'group_id' => $group_id,
                'user_id'  => $student_id
            ]);
        }

        return true;
    }

    public static function deleteStudents($group_id) {
        self::where('group_id', $group_id)->delete();
    }

}
