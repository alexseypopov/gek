<?php

namespace App\Models;

use App\Notifications\FillPassword;
use App\Notifications\ResetPassword;
use App\Traits\FullTextSearch;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use FullTextSearch;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'firstName', 'lastName', 'patronymic'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $searchable = ['firstName', 'lastName', 'patronymic', 'email'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function subdivisions()
    {
        return $this->hasMany(Subdividion::class, 'head_id', 'id');
    }

    public function comissions()
    {
        return $this->hasMany(Comission::class, 'user_id', 'id');
    }

    const AUTH_DISABLED  = 'disabled';  // Аутентификация запрещена
    const AUTH_LINK_SENT = 'link_sent'; // Ссылка на предоставление доступа / сборс пароля отправлена
    const AUTH_LINK_NOT_SENT = 'link_not_sent'; // Ссылка на предоставление доступа / сборс пароля НЕ ОТПРАВЛЕНА
    const AUTH_SUCCESS   = 'success';   // Пользователь может авторизоваться

    const PAGINATE = 10;

    /**
     * Получение полного ФИО
     * @return string
     */
    public function getFullName()
    {
        return $this->lastName.' '.$this->firstName.' '.$this->patronymic;
    }

    /**
     * Получение сокращенного ФИО
     * @return string
     */
    public function getShortName()
    {
        return $this->lastName.' '.mb_substr($this->firstName, 0, 1).'. '.(empty($this->patronymic) ? '' : mb_substr($this->patronymic, 0, 1).'.');
    }

    public static function getShortNameById($user_id)
    {
        $user = self::find($user_id);
        return $user->getShortName();
    }

    /**
     * Имя + Отчество (при наличии)
     * @return string
     */
    public function getIOName()
    {
        return $this->firstName.' '.(empty($this->patronymic) ? '' : ' '.$this->patronymic);
    }

    /**
     * Валидация перед созданием нового пользователя
     * @param $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validateBeforeCreate($data) {
        return Validator::make($data, [
            'lastName' => ['required', 'string', 'max:255'],
            'firstName' => ['required', 'string', 'max:255'],
            'patronymic' => ['nullable', 'string', 'max:255'],
            'email' => ['nullable','required_if:auth,true', 'email', 'max:255', 'unique:users'],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
            'roles' => ['nullable'],
        ]);
    }
    /**
     * Валидация перед созданием нового пользователя
     * @param $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validateBeforeUpdate($data) {
        return Validator::make($data, [
            'lastName' => ['required', 'string', 'max:255'],
            'firstName' => ['required', 'string', 'max:255'],
            'patronymic' => ['nullable', 'string', 'max:255'],
            'email' => ['nullable','required_if:auth,true', 'email', 'max:255', Rule::unique('users')->ignore($data['id']),],
            'roles' => ['nullable'],
        ]);
    }
    /**
     * Создание нового пользователя
     * @param $data
     * @return mixed
     */
    public static function createUser($data)
    {
        return self::create([
            'email' => $data['email'],
            'firstName' => $data['firstName'],
            'lastName' => $data['lastName'],
            'patronymic' => $data['patronymic'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * Уведомление о восстановлении пароля
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token, $this));
    }

    /**
     * Уведомление о получении доступа к системе
     * @return bool
     */
    public function sendPasswordFillNotification() {

        $token = PasswordReset::createToken($this);
        $this->notify(new FillPassword($token, $this));

        return true;
    }

    /**
     * Получение списка ролей
     * @return mixed
     */
    public function getRoleNames()
    {
        return $this->roles->pluck('title');
    }

    /**
     * Получение статуса входа
     * @return string
     */
    public function getAuthStatus()
    {
        if(empty($this->email)) {
            return self::AUTH_DISABLED;
        } elseif (!empty($this->password)) {
            return self::AUTH_SUCCESS;
        } elseif (PasswordReset::haveToken($this)) {
            return self::AUTH_LINK_SENT;
        } else {
            return self::AUTH_LINK_NOT_SENT;
        }
    }

    /**
     * Получение иконки статуса входа
     * @return string
     */
    public function getAuthStatusContent($content = 'icon')
    {
        $status = $this->getAuthStatus();
        $icon = '';
        $text = '';

        $this->getRoleNames();

        switch ($status) {
            case self::AUTH_SUCCESS:
                $icon = '<span title="Пользователю предоставлен доступ к системе" ><b-icon icon="check" variant="success"></b-icon></span>';
                $text = 'Пользователю предоставлен доступ к системе';
                break;
            case self::AUTH_LINK_SENT:
                $icon = '<span title="Пользователь получил ссылку для предоставления доступа/восстановления пароля"><b-icon icon="link" variant="success"></b-icon></span>';
                $text = 'Пользователь получил ссылку для предоставления доступа/восстановления пароля';
                break;
            case self::AUTH_LINK_NOT_SENT:
                $icon = '<span title="Пользователь имеет возможность войти в систему, но не получил ссылку для предоставления доступа/восстановления пароля"><b-icon icon="link" variant="danger"></b-icon></span>';
                $text = 'Пользователь имеет возможность войти в систему, но не получил ссылку для предоставления доступа/восстановления пароля';
                break;
            case self::AUTH_DISABLED:
                $icon = '<span title="Пользователь не имеет возможности войти в систему"><b-icon icon="x" variant="danger"></b-icon></span>';
                $text = 'Пользователь не имеет возможности войти в систему';
                break;
        }

        return $content == 'icon' ? $icon : $text;
    }

    public function changeEmail($email, $send) {

        $this->email = $email;
        $this->save();

        if($send) {
            $this->sendPasswordFillNotification();
        }

        return true;
    }
}
