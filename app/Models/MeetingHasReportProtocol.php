<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PhpOffice\PhpWord\TemplateProcessor;

class MeetingHasReportProtocol extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'meeting_id',
        'user_id',
        'status',
        'number',
        'date',
        'time_from',
        'time_to',
        //'date_document',
        'theme',
        'consultation',
        'adviser',
        //'feedback_adviser',
        //'resume',
        //'min',
        'questions',
        'materials',
        'eval',
        'comment',
        'excelent'
        //'dop_questions',
    ];
    protected $appends = [
        'normal_date',
    ];

    public function meeting()
    {
        return $this->hasOne(Meeting::class,'id', 'meeting_id');
    }
    public function student()
    {
        return $this->hasOne(MeetingHasStudent::class,'user_id', 'user_id')->where('meeting_id', $this->meeting_id);
    }

    //
    const STATUS_CREATED_SLUG = 'created';
    const STATUS_FILL_SLUG = 'fill';
    public static $statuses_named = [
        self::STATUS_CREATED_SLUG => [
            'name' => 'Ожидает подтверждения'
        ],
        self::STATUS_FILL_SLUG => [
            'name' => 'Подтверждена'
        ],
    ];

    public function getNormalDateAttribute()
    {
        return $this->getNormalDate();
    }

    public function getNormalDate($type = 'full')
    {
        $months = array( 1 => 'января' , 'февраля' , 'марта' , 'апреля' , 'мая' , 'июня' , 'июля' , 'августа' , 'сентября' , 'октября' , 'ноября' , 'декабря' );

        if(empty($this->date)) {
            return '';
        } else {
            if($type == 'd') {
                return date('d', strtotime($this->date));
            } elseif ($type == 'm') {
                return date($months[date( 'n' )], strtotime($this->date));
            } elseif ($type == 'y') {
                return date('Y', strtotime($this->date));
            } else {
                return date('d '.$months[date( 'n' )].' Y', strtotime($this->date));
            }
        }
    }

    public function download()
    {
        // Вставка данных с проверкой на наличее
        function setValue($template, $position, $value) {
            if(empty($value) || is_array($value)) {
                $value = '';
            }
            // Вставка значения
            $template->setValue($position, htmlspecialchars($value));
        }
        //
        $desc   = ''; // Приложение 4.2
        $number   = $this->number;
        $day    = $this->getNormalDate('d');
        $month  = $this->getNormalDate('m');
        $year   = $this->getNormalDate('y');
        $timeFrom = explode(':', $this->time_from);
        $timeTo = explode(':', $this->time_to);

        $h_f    = $timeFrom[0];
        $m_f    = $timeFrom[1];
        $h_t    = $timeTo[0];
        $m_t    = $timeTo[1];
        $fio    = 'группы '.$this->meeting->group_name.' '.$this->student->fullName_rod;
        $theme  = $this->theme;
        $chairman  = $this->meeting->getChairmanName();

        $date_document = $this->meeting->normal_date_document;
        $number_document = $this->meeting->number_document;
//
//        $comission_first = '';
//        $comission = $this->gek->comission;
//
        $adviser = $this->adviser;

        $consultations = $this->consultation;

//
        $fio3 = $this->student->fullName;
//
        $type2 = 'Выпускную квалификационную работу';
//
        $evals = [3=>'удовлетворительно',4=>'хорошо',5=>'отлично'];
        $eval = $evals[$this->eval];
//
        $comment = $this->comment;
//
        $chairman2 = $this->meeting->chairman->shortName;
//
        $secretary = $this->meeting->secretary->shortName;

        // Создание файла
        $templatePath = 'templates/vkr.docx';
        $filename     = public_path($templatePath);
        $template = new TemplateProcessor($filename);


        setValue($template, 'desc',        $desc);
        setValue($template, 'number',        $number);

        setValue($template, 'day',        $day);
        setValue($template, 'month',       $month);
        setValue($template, 'year',         $year);
        setValue($template, 'h_f',         $h_f);
        setValue($template, 'm_f',         $m_f);
        setValue($template, 'h_t',         $h_t);
        setValue($template, 'm_t',         $m_t);
//        setValue($template, 'type',         $type);
        setValue($template, 'fio',         $fio);
        setValue($template, 'theme',         $theme);

        setValue($template, 'chairman',         $chairman);

        $template->cloneRow('comission', $this->meeting->persons->count());
        foreach ($this->meeting->persons as $keyTransfer => $transfer) {
            setValue($template, 'comission#'.($keyTransfer+1),   $transfer->fullName);
        }

        $template->cloneRow('material', count(json_decode($this->materials)));
        foreach (json_decode($this->materials) as $keyTransfer => $transfer) {
            setValue($template, 'material#'.($keyTransfer+1),   ($keyTransfer+1).'. '.$transfer);
        }

        $template->cloneRow('question', count(json_decode($this->questions)));
        foreach (json_decode($this->questions) as $keyTransfer => $transfer) {
            setValue($template, 'question#'.($keyTransfer+1),   ($keyTransfer+1).'. '.$transfer);
        }
//
        setValue($template, 'adviser',         $adviser);
        setValue($template, 'consultations',         $consultations);
//        setValue($template, 'faculty',         $faculty);
        setValue($template, 'date_document',         $date_document);
        setValue($template, 'number_document',         $number_document);

        setValue($template, 'fio3',         $fio3);
        setValue($template, 'type2',         $type2);
        setValue($template, 'eval',         $eval);
        setValue($template, 'comment',         $comment);
//
        setValue($template, 'chairman2',         $chairman2);
        setValue($template, 'secretary',         $secretary);

        $chairman_hash = Sign::getHash(self::class, $this->id, $this->meeting->chairman->user_id);
        $chairman_hash_img = Sign::haveImg($chairman_hash);
        if($this->meeting->chairman->distance && $chairman_hash_img) {
            $template->setImageValue('char', array('path' => public_path('img/certificate/'.$chairman_hash.'.png'), 'width' => 300, 'ratio' => false));
        } else {
            setValue($template, 'char', '');
        }
//        setValue($template, 'sec',         $this->getSecretarySign());

        $secretary_hash = Sign::getHash(self::class, $this->id, $this->meeting->secretary->user_id);
        $secretary_hash_img = Sign::haveImg($secretary_hash);
        if($this->meeting->secretary->distance && $secretary_hash_img) {
            $template->setImageValue('sec', array('path' => public_path('img/certificate/'.$secretary_hash.'.png'), 'width' => 300, 'ratio' => false));
        } else {
            setValue($template, 'sec', '');
        }
//        setValue($template, 'char',         $this->getChairmanSign());
        //setValue($template, 'appeal', $this->appeal ? 'Результат аттестационного испытания аннулирован. Протокол №'.$this->appeal_number.' от '.$this->appeal_date.'г.' : '');
        setValue($template, 'appeal',         '');

        //
        header('Content-Disposition: inline; filename="Протокол ГЭК №'.$number.' '.$this->meeting->group_name.'.docx"');
        header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        $template->saveAs('php://output');
        //
        exit;
    }
}
