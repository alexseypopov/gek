<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PhpOffice\PhpWord\TemplateProcessor;

class MeetingHasSolution extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'meeting_id',
        'user_id',
        'status',
        'number',
        'date',
        'specialty',
    ];
    protected $appends = [
        'normal_date',
    ];

    public function meeting()
    {
        return $this->hasOne(Meeting::class,'id', 'meeting_id');
    }
    public function student()
    {
        return $this->hasOne(MeetingHasStudent::class,'user_id', 'user_id')->where('meeting_id', $this->meeting_id);
    }
    public function report()
    {
        return $this->hasOne(MeetingHasReportProtocol::class,'user_id', 'user_id')->where('meeting_id', $this->meeting_id);
    }
    public function exam()
    {
        return $this->hasOne(MeetingHasExamProtocol::class,'user_id', 'user_id')->where('meeting_id', $this->meeting_id);
    }

    //
    const STATUS_CREATED_SLUG = 'created';
    const STATUS_FILL_SLUG = 'fill';
    public static $statuses_named = [
        self::STATUS_CREATED_SLUG => [
            'name' => 'Ожидает подтверждения'
        ],
        self::STATUS_FILL_SLUG => [
            'name' => 'Подтверждена'
        ],
    ];
    public function getNormalDateAttribute()
    {
        return $this->getNormalDate();
    }

    public function getNormalDate($type = 'full')
    {
        $months = array( 1 => 'января' , 'февраля' , 'марта' , 'апреля' , 'мая' , 'июня' , 'июля' , 'августа' , 'сентября' , 'октября' , 'ноября' , 'декабря' );

        if(empty($this->date)) {
            return '';
        } else {
            if($type == 'd') {
                return date('d', strtotime($this->date));
            } elseif ($type == 'm') {
                return date($months[date( 'n' )], strtotime($this->date));
            } elseif ($type == 'y') {
                return date('Y', strtotime($this->date));
            } else {
                return date('d '.$months[date( 'n' )].' Y', strtotime($this->date));
            }
        }
    }

    public function download()
    {
        // Вставка данных с проверкой на наличее
        function setValue($template, $position, $value) {
            if(is_array($value)) {
                $value = '';
            }
            // Вставка значения
            $template->setValue($position, htmlspecialchars($value));
        }

        //
        $desc   = ''; // Приложение 5
        $trend  = $this->meeting->group->trend;
        $day    = $this->getNormalDate('d');
        $month  = $this->getNormalDate('m');
        $year   = $this->getNormalDate('y');
        $number   = $this->number;

        $base = '';
        if($this->meeting->exam) {
            $base .= $this->student->getExamProtocolText();
            if($this->meeting->report) {
                $base .= ' ';
            }
        }
        if($this->meeting->report) {
            $base .= $this->student->getReportProtocolText();
        }


//        $p_number = '';
//        $p_day    = '';
//        $p_month  = '';
//        $p_year   = '';
//        $type     = '';
//        $fio      = '';
//        $eval     = '';
        $fio2     = $this->student->fullName_dat;
        $stepen   = $this->specialty;
        $spec     = $this->meeting->group->trend;
        $fio3     = $this->student->fullName_dat;
        $special  = $this->student->isExcelent() ? 'с отличием' : '';
//
        $for = $this->student->yesCount();
        $against = $this->student->noCount();
//
//        $chairman_fio = $this->gek->getChairmanShortName();
//
//        $comission = $this->gek->comission;
//
//        $secretary = $this->gek->getSecretaryShortName();
//
//        $gekProtocol = GekProtocolReport::where('gek_id', $this->gek_id)->where('student_id', $this->student_id)->first();
//
//        $appeal = (isset($gekProtocol->id) && $gekProtocol->appeal) ? 'Результат аттестационного испытания аннулирован. Протокол №'.$gekProtocol->appeal_number.' от '.$gekProtocol->appeal_date.'г.' : '';


        $appeal = '';

//
        // Создание файла
        $templatePath = 'templates/solution.docx';
        $filename     = public_path($templatePath);
        $template = new TemplateProcessor($filename);


        setValue($template, 'desc',        $desc);
        setValue($template, 'trend',        $trend);
        setValue($template, 'day',        $day);
        setValue($template, 'month',       $month);
        setValue($template, 'year',         $year);
        setValue($template, 'number',        $number);

        setValue($template, 'base',        $base);
//        setValue($template, 'p_number',         $p_number);
//        setValue($template, 'p_day',         $p_day);
//        setValue($template, 'p_month',         $p_month);
//        setValue($template, 'p_year',         $p_year);
//        setValue($template, 'type',         $type);
//        setValue($template, 'fio',         $fio);
//        setValue($template, 'eval',         $eval);
        setValue($template, 'fio2',         $fio2);
        setValue($template, 'stepen',         $stepen);
        setValue($template, 'spec',         $spec);
        setValue($template, 'fio3',         $fio3);
        setValue($template, 'special',         $special);
        setValue($template, 'for',         $for);
        setValue($template, 'against',         $against);


        $chairman_hash = Sign::getHash(self::class, $this->id, $this->meeting->chairman->user_id);
        $chairman_hash_img = Sign::haveImg($chairman_hash);
        if($this->meeting->chairman->distance && $chairman_hash_img) {
            $template->setImageValue('chairman_sign', array('path' => public_path('img/certificate/'.$chairman_hash.'.png'), 'width' => 300, 'ratio' => false));
        } else {
            setValue($template, 'chairman_sign', '');
        }
//        setValue($template, 'sec',         $this->getSecretarySign());

        $secretary_hash = Sign::getHash(self::class, $this->id, $this->meeting->secretary->user_id);
        $secretary_hash_img = Sign::haveImg($secretary_hash);
        if($this->meeting->secretary->distance && $secretary_hash_img) {
            $template->setImageValue('ssign', array('path' => public_path('img/certificate/'.$secretary_hash.'.png'), 'width' => 300, 'ratio' => false));
        } else {
            setValue($template, 'ssign', '');
        }
//        setValue($template, 'chairman_sign',         $this->getChairmanSign());
        setValue($template, 'chairman_fio',         $this->meeting->chairman->shortName);
        setValue($template, 'secretary',         $this->meeting->secretary->shortName);

//        $template->cloneBlock('csign',$this->meeting->persons()->count(), true, true);
//        foreach ($this->meeting->persons as $keyTransfer => $transfer) {
//            $hash = Sign::getHash(self::class, $this->id, $transfer->user_id);
//            $hash_img = Sign::haveImg($hash);
//            if($transfer->distance && $hash_img) {
//                $template->setImageValue('csign#'.($keyTransfer+1), array('path' => public_path('img/certificate/'.$hash.'.png'), 'width' => 300, 'ratio' => false));
//            } else {
//                setValue($template, 'csign#'.($keyTransfer+1), '');
//            }
//            //setValue($template, 'csign#'.($keyTransfer+1),   $transfer->getSignHash($this->student_id));
//            setValue($template, 'cfio#'.($keyTransfer+1),   $transfer->shortName);
//        }
//
//
        $template->cloneRow('csign', $this->meeting->persons()->count());
        foreach ($this->meeting->persons as $keyTransfer => $transfer) {
            $hash = Sign::getHash(self::class, $this->id, $transfer->user_id);
            $hash_img = Sign::haveImg($hash);
            if($transfer->distance && $hash_img) {
                $template->setImageValue('csign#'.($keyTransfer+1), array('path' => public_path('img/certificate/'.$hash.'.png'), 'width' => 300, 'ratio' => false));
            } else {
                setValue($template, 'csign#'.($keyTransfer+1), '');
            }
            //setValue($template, 'csign#'.($keyTransfer+1),   $transfer->getSignHash($this->student_id));
            setValue($template, 'cfio#'.($keyTransfer+1),   $transfer->shortName);
        }
//
//        setValue($template, 'secretary',         $secretary);
//        setValue($template, 'ssign',         $this->getSecretarySign());
//
        setValue($template, 'appeal',         $appeal);



        //
        header('Content-Disposition: inline; filename="Решение ГЭК №'.$number.' '.$this->student->fullName.' '.$this->meeting->group_name.'.docx"');
        header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        $template->saveAs('php://output');
        //
        exit;
    }
}
