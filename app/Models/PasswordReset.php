<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PasswordReset extends Model
{
    protected $fillable = [
        'email', 'token'
    ];

    public $timestamps = false;
    protected $dates = [
        'created_at'
    ];

    /**
     * Проверка на наличие старого токена
     * @param User $user
     */
    public static function haveToken(User $user)
    {
        return self::where('email', $user->email)->exists();
    }

    /**
     * Проверка токена и email
     * @param $token
     * @param $email
     * @return mixed
     */
    public static function haveTokenAndEmail($token, $email)
    {
        return self::where('token', $token)->where('email', $email)->exists();
    }

    /**
     * Проверка на корректности токена
     * @param User $user
     */
    public static function correctToken($token)
    {
        return self::where('token', $token)->exists();
    }

    /**
     * Удаление токенов пользователя
     * @param User $user
     * @return mixed
     */
    public static function clearTokens(User $user)
    {
        return self::where('email', $user->email)->delete();
    }

    /**
     * Создание токена
     * @param User $user
     */
    public static function createToken(User $user)
    {
        // Если есть активные токены - убираем их
        if(self::haveToken($user)) {
            self::clearTokens($user);
        }
        
        $token = hash_hmac('sha256', Str::random(40), $user->email);

        self::create([
            'email' => $user->email,
            'token' => $token
        ]);

        return $token;
    }
}
