<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketTask extends Model
{
    //
    protected $fillable = [
        'id',
        'meeting_id',
        'ticket_id',
        'number',
        'text',
        'img',
    ];
}
