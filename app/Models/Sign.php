<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class Sign extends Model
{
    protected $fillable = [
        'id',
        'model',
        'element_id',
        'user_id',
        'type_sign',
        'hash',
        'created_at',
        'unsign_user',
        'deleted_at'
    ];

    public function user()
    {
        return $this->hasOne(User::class,'id', 'user_id');
    }

    public static function generateHash($model, $element_id, $user_id)
    {
        return md5($model.'/'.$element_id.'/'.$user_id);
    }

    public static function sign($model, $element_id, $user_id)
    {
        if(!self::where('model', $model)->where('element_id', $element_id)->where('user_id', $user_id)->exists()) {
            // Генерация хэша и создание записи
            $hash = self::generateHash($model, $element_id, $user_id);
            self::create([
                'model' => $model,
                'element_id' => $element_id,
                'user_id' => $user_id,
                'hash' => $hash,
            ]);

            self::generateCertificate($user_id, $hash);
        }

        return true;
    }

    // Создание изображения
    public static function generateCertificate($user_id, $hash)
    {
        QrCode::size(80)
            ->format('png')
            ->generate(url('certificate/'.$hash), public_path('img/qr/'.$hash.'.png'));
        $manager = Image::make(public_path('/templates/certificate.png'));
        $manager->insert(public_path('img/qr/'.$hash.'.png'), 'center-left', 10, 10);
        $manager->text('Документ подписан простой', 130, 20, function($font) {
            $font->file(public_path('/templates/Open_Sans.ttf'));
            $font->size(10);
            $font->color('#0000ff');
        });
        $manager->text('электронной подписью', 140, 35, function($font) {
            $font->file(public_path('/templates/Open_Sans.ttf'));
            $font->size(10);
            $font->color('#0000ff');
        });
        $manager->text('Сертификат: ', 110, 53, function($font) {
            $font->file(public_path('/templates/Open_Sans.ttf'));
            $font->size(10);
            $font->color('#0000ff');
        });
        $manager->text($hash, 110, 68, function($font) {
            $font->file(public_path('/templates/Open_Sans.ttf'));
            $font->size(9);
            $font->color('#0000ff');
        });
        $manager->text('Владелец: ', 110, 85, function($font) {
            $font->file(public_path('/templates/Open_Sans.ttf'));
            $font->size(10);
            $font->color('#0000ff');
        });
        $manager->text(User::getShortNameById($user_id), 165, 85, function($font) {
            $font->file(public_path('/templates/Open_Sans.ttf'));
            $font->size(10);
            $font->color('#000000');
        });
        $manager->save(public_path('/img/certificate/'.$hash.'.png'));
    }



    public function getDocumentName()
    {
        $result = '---';

        if($this->model == 'Test') {
            $result = 'Пустой тестовый документ';
        }elseif($this->model == Meeting::class) {
            $result = 'Заседание ГЭК';
        }elseif($this->model == MeetingHasReportProtocol::class) {
            $result = 'Протокол ВКР';
        }elseif($this->model == MeetingHasExamProtocol::class) {
            $result = 'Протокол экзамена';
        }elseif($this->model == Ticket::class) {
            $result = 'Экзаменационный билет';
        }

        return $result;
    }

    public static function getHash($model, $element_id, $user_id) {
        $result = '';
        if(self::where('model', $model)->where('element_id', $element_id)->where('user_id', $user_id)->exists()) {
            $result = self::where('model', $model)->where('element_id', $element_id)->where('user_id', $user_id)->first()->hash;
        }
        return $result;
    }

    public static function haveImg($hash)
    {
        return file_exists(public_path('img/certificate/'.$hash.'.png'));
    }
}
