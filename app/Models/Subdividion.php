<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subdividion extends Model
{
    use SoftDeletes;

    const PAGINATE = 10;

    /**
     * Отношение к руководителю
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function head()
    {
        return $this->hasOne(User::class,'id', 'head_id');
    }

    public function parent()
    {
        return $this->hasOne(self::class,'id', 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(self::class,'parent_id', 'id');
    }

    public function chair_groups()
    {
        return $this->hasMany(Group::class,'chair_id', 'id');
    }

    protected $fillable = [
        'type', 'parent_id', 'name', 'head_degree', 'head_id'
    ];

    const UMU = 'umu';
    const INST = 'inst';
    const FACULTY = 'faculty';
    const CHAIR = 'chair';
    public static $types = [
        [
            'slug' => self::UMU,
            'name' => 'Управление',
            'subname' => 'Учебно-методическое управление',
            'head_degree' => 'Начальник учебно-методического управления'
        ],
        [
            'slug' => self::INST,
            'name' => 'Институт',
            'subname' => 'Институт',
            'head_degree' => 'Директор института'
        ],
        [
            'slug' => self::FACULTY,
            'name' => 'Факультет',
            'subname' => 'Факультет',
            'head_degree' => 'Декан'
        ],
        [
            'slug' => self::CHAIR,
            'name' => 'Кафедра',
            'subname' => 'Кафедра',
            'head_degree' => 'Заведующий кафедрой'
        ]
    ];

    public static $levels = [
        self::UMU => [
            'slug' => self::UMU,
            'name' => 'Управление',
            'subname' => 'Учебно-методическое управление',
            'head_degree' => 'Начальник учебно-методического управления'
        ],
        self::INST => [
            'slug' => self::INST,
            'name' => 'Институт',
            'subname' => 'Институт',
            'head_degree' => 'Директор института'
        ],
        self::FACULTY => [
            'slug' => self::FACULTY,
            'name' => 'Факультет',
            'subname' => 'Факультет',
            'head_degree' => 'Декан'
        ],
        self::CHAIR => [
            'slug' => self::CHAIR,
            'name' => 'Кафедра',
            'subname' => 'Кафедра',
            'head_degree' => 'Заведующий кафедрой'
        ]
    ];

    /**
     * Уровень структурного подразделения
     * @return mixed
     */
    public function getLevel()
    {
        return self::$levels[$this->type];
    }
    /**
     * Имя ркуоводителя
     * @return mixed
     */
    public function getHeadFullName()
    {
        return isset($this->head->id) ? $this->head->getFullName() : 'Пользователь удален';
    }

    /**
     * Валидация
     * @param $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validate($data) {
        return Validator::make($data, [
            'type' => ['required'],
            'name' => ['required', 'string', 'max:255'],
            'head_degree' => ['required', 'string', 'max:255'],
            'head_id' => ['required', 'integer'],
        ]);
    }

    public static function getParents($level) {
        $types = [];
        if($level == self::INST) {
            $types = [ self::UMU ];
        }
        elseif($level == self::FACULTY) {
            $types = [ self::INST, self::UMU ];
        } elseif($level == self::CHAIR) {
            $types = [ self::FACULTY, self::INST, self::UMU ];
        }

        $parents = self::whereIn('type', $types)->get();

        return $parents;
    }


    public function getInfo()
    {
        return [
            'type' => $this->type,
            'id' => $this->id,
            'name' => $this->name,
            'head_id' => $this->head_id,
            'head_fio' => $this->getHeadFullName(),
            'head_degree' => $this->head_degree,
        ];
    }

    public static function getInfoById($sub_id)
    {
        $sub = self::find($sub_id);

        if(isset($sub->id)) {
            return $sub->getInfo();
        } else {
            return [
                'id' => null,
                'name' => '',
                'head_id' => '',
                'head_fio' => '',
                'head_degree' => '',
            ];
        }
    }

    public static function getFacultyByChair($chair_id)
    {
        $chair = self::find($chair_id);

        if(isset($chair->parent->id) && $chair->parent->type == self::FACULTY) {
            return $chair->parent->getInfo();
        } else {
            return [
                'id' => null,
                'name' => '',
                'head_id' => '',
                'head_fio' => '',
                'head_degree' => '',
            ];
        }
    }

    public static function getInstBySub($sub_id)
    {
        $sub = self::find($sub_id);

        if(isset($sub->parent->id) && $sub->parent->type == self::FACULTY) {
            return self::getInstBySub($sub->parent->id);
        } elseif (isset($sub->parent->id) && $sub->parent->type == self::INST) {
            return $sub->parent->getInfo();
        } else {
            return [
                'id' => null,
                'name' => '',
                'head_id' => '',
                'head_fio' => '',
                'head_degree' => '',
            ];
        }
    }

    public static function getUMUBySub($sub_id)
    {
        $sub = self::find($sub_id);

        if(isset($sub->parent->id) && $sub->parent->type != self::UMU) {
            return self::getUMUBySub($sub->parent->id);
        } elseif (isset($sub->parent->id) && $sub->parent->type == self::UMU) {
            return $sub->parent->getInfo();
        } else {
            return [
                'id' => null,
                'name' => '',
                'head_id' => '',
                'head_fio' => '',
                'head_degree' => '',
            ];
        }
    }
}