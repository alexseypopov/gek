<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;

class Meeting extends Model
{
    use SoftDeletes;

    const PAGINATE = 15;

    protected $fillable = [
        'id',
        'group_id',
        'group_name',
        'status',
        'chair_id',
        'faculty_id',
        'inst_id',
        'umu_id',
        'sub',
        'qualification',
        'comment',
        'date_document',
        'number_document',
        'exam',
        'report',
        'tickets_status',
        'tickets_agree_sub',
        'date_agree_tickets',
        'exam_status',
        'exam_hash',
    ];

    // Вычисляемые поля
    protected $appends = [
        'normal_date_document',
        'normal_date_agree_tickets'
    ];

    public function group()
    {
        return $this->hasOne(Group::class,'id', 'group_id');
    }
    public function chairman()
    {
        return $this->hasOne(Comission::class,'meeting_id', 'id')->where('type', Comission::TYPE_CHAIRMAN_SLUG);
    }
    public function secretary()
    {
        return $this->hasOne(Comission::class,'meeting_id', 'id')->where('type', Comission::TYPE_SECRETARY_SLUG);
    }
    public function persons()
    {
        return $this->hasMany(Comission::class,'meeting_id', 'id')->where('type', Comission::TYPE_PERSON_SLUG);
    }
    public function voteComission()
    {
        return $this->hasMany(Comission::class,'meeting_id', 'id')->whereIn('type', [Comission::TYPE_PERSON_SLUG,Comission::TYPE_CHAIRMAN_SLUG]);

    }
    public function tickets()
    {
        return $this->hasMany(Ticket::class,'meeting_id', 'id');
    }

    public function students()
    {
        return $this->hasMany(MeetingHasStudent::class,'meeting_id', 'id');
    }

    public function exam_protocols()
    {
        return $this->hasMany(MeetingHasExamProtocol::class,'meeting_id', 'id');
    }

    const FORM_STANDART_SLUG = 'standart';
    const FORM_STANDART = [
        'slug' => self::FORM_STANDART_SLUG,
        'name' => 'Очная',
        'description' => 'Электронная подпись будет отсутствовать'
    ];
    const FORM_DISTANCE_SLUG = 'distance';
    const FORM_DISTANCE = [
        'slug' => self::FORM_DISTANCE_SLUG,
        'name' => 'Дистанционная',
        'description' => 'Председателю, секретарю и членам комиссии будет открыт доступ голосованию, для всех действующих лиц используется электронная подпись'
    ];
    const FORM_MIX_SLUG = 'mix';
    const FORM_MIX = [
        'slug' => self::FORM_MIX_SLUG,
        'name' => 'Смешанная',
        'description' => 'Некоторым выбранным членам(или председателю) комиссии будет открыт доступ к электронной подписи'
    ];

    public static $forms = [ self::FORM_STANDART, self::FORM_DISTANCE, self::FORM_MIX];
    public static $forms_named = [ self::FORM_STANDART_SLUG => self::FORM_STANDART, self::FORM_DISTANCE_SLUG => self::FORM_DISTANCE, self::FORM_MIX_SLUG => self::FORM_MIX];

    const CONTROL_EXAM_SLUG = 'exam';
    const CONTROL_EXAM = [
        'slug' => self::CONTROL_EXAM_SLUG,
        'name' => 'Экзамен'
    ];
    const CONTROL_REPORT_SLUG = 'report';
    const CONTROL_REPORT = [
        'slug' => self::CONTROL_REPORT_SLUG,
        'name' => 'ВКР/Научный доклад'
    ];
    public static $controls = [ self::CONTROL_EXAM, self::CONTROL_REPORT ];
    public static $controls_named = [ self::CONTROL_REPORT_SLUG => self::CONTROL_EXAM, self::CONTROL_REPORT_SLUG => self::CONTROL_REPORT ];

    const STATUS_CREATED_SLUG = 'created';
    const STATUS_CONFIRMED_SLUG = 'confirmed';
    const STATUS_SUCCESS_SLUG = 'success';
    public static $statuses_named = [
        self::STATUS_CREATED_SLUG => [
            'name' => 'Ожидает подтверждения'
        ],
        self::STATUS_CONFIRMED_SLUG => [
            'name' => 'Подтверждена'
        ],
    ];

    const TICKET_CREATED_SLUG = 'created';
    const TICKET_WAIT_CONFIRM_SLUG = 'wait_confirm_chair';
    const TICKET_CONFIRMED_CHAIR_SLUG = 'confirmed_chair';
    const TICKET_CONFIRMED_SLUG = 'confirmed';
    public static $tickets_named = [
        self::TICKET_CREATED_SLUG => [
            'name' => 'Билеты не согласованы'
        ],
        self::TICKET_WAIT_CONFIRM_SLUG => [
            'name' => 'Билеты отправлены на согласование'
        ],
        self::TICKET_CONFIRMED_CHAIR_SLUG => [
            'name' => 'Билеты согласованы на кафедре'
        ],
        self::TICKET_CONFIRMED_SLUG => [
            'name' => 'Билеты согласованы'
        ],
    ];

    public static function validate($data) {
        return Validator::make($data, [
            'group_id' => ['required'],
            'chairman' => ['required'],
            'secretary' => ['required'],
            'qualification' => ['required'],
            'date_document' => ['required'],
            'number_document' => ['required'],
            'comission' => ['required'],
        ]);
    }

    const EXAM_WAIT = 'wait';
    const EXAM_NOW = 'now';

    public function getNormalDateDocumentAttribute()
    {
        if(empty($this->date_document)) {
            return '';
        } else {
            $months = array( 1 => 'января' , 'февраля' , 'марта' , 'апреля' , 'мая' , 'июня' , 'июля' , 'августа' , 'сентября' , 'октября' , 'ноября' , 'декабря' );
            return date('d '.$months[date( 'n' )].' Y', strtotime($this->date_document));
        }
    }

    public function getNormalDateAgreeTicketsAttribute()
    {
        if(empty($this->date_agree_tickets)) {
            return 'не заполнена';
        } else {
            $months = array( 1 => 'января' , 'февраля' , 'марта' , 'апреля' , 'мая' , 'июня' , 'июля' , 'августа' , 'сентября' , 'октября' , 'ноября' , 'декабря' );
            return date('d '.$months[date( 'n' )].' Y г.', strtotime($this->date_agree_tickets));
        }
    }

    public static function getNormalDate($date, $type = 'full')
    {
        $months = array( 1 => 'января' , 'февраля' , 'марта' , 'апреля' , 'мая' , 'июня' , 'июля' , 'августа' , 'сентября' , 'октября' , 'ноября' , 'декабря' );

        if(empty($date)) {
            return '';
        } else {
            if($type == 'd') {
                return date('d', strtotime($date));
            } elseif ($type == 'm') {
                return date($months[date( 'n' )], strtotime($date));
            } elseif ($type == 'y') {
                return date('Y', strtotime($date));
            } else {
                return date('d '.$months[date( 'n' )].' Y', strtotime($date));
            }
        }
    }

    public function getChairmanName($type = 'full')
    {
        return isset($this->chairman->user_id) ? $this->chairman->{$type.'Name'} : '---';
    }
    public function getChairmanId()
    {
        return isset($this->chairman->user_id) ? $this->chairman->user_id : '';
    }
    public function getSecretaryName($type = 'full')
    {
        return isset($this->secretary->user_id) ? $this->secretary->{$type.'Name'} : '---';
    }
    public function getSecretaryId()
    {
        return isset($this->secretary->user_id) ? $this->secretary->user_id : '';
    }

    public function getStatus() {
        if($this->status) {
            return $this->getTicketStatus();
        }
        else {
            return self::$statuses_named[$this->status]['name'];
        }
    }

    public function isAllowStudent($user_id)
    {
        return $this->students()->where('user_id', $user_id)->exists();
    }

    public static function getMeetingIdByReportId($id)
    {
        return MeetingHasReportProtocol::find($id)->meeting_id;
    }
    public static function getMeetingIdByExamId($id)
    {
        return MeetingHasExamProtocol::find($id)->meeting_id;
    }
    public static function getMeetingIdByTicketId($id)
    {
        return Ticket::find($id)->meeting_id;
    }

    public function getTicketStatus()
    {
        if(empty($this->tickets_status)) {
            return self::$tickets_named[self::TICKET_CREATED_SLUG]['name'];
        } else {
            return self::$tickets_named[$this->tickets_status]['name'];
        }
    }

    public function createTicket($number)
    {
        return Ticket::create([
            'meeting_id' => $this->id,
            'number' => $number,
            'counter' => 0,
        ]);
    }

    public function startExam()
    {
        $hash = md5($this->id.now());
        $this->exam_status = self::EXAM_NOW;
        $this->exam_hash = $hash;
        $this->save();
        return true;
    }
    public function closeExam()
    {
        $this->exam_status = self::EXAM_WAIT;
        $this->exam_hash = null;
        $this->save();
        return true;
    }
}
