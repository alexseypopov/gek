<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PhpOffice\PhpWord\TemplateProcessor;

class MeetingHasExamProtocol extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'meeting_id',
        'user_id',
        'status',
        'number',
        'date',
        'time_from',
        'time_to',
        'dop_questions',
        'review',
        'consultation',
        'eval',
        'comment',
//        'theme',
//        'consultation',
//        'adviser',
//        //'feedback_adviser',
//        //'resume',
//        //'min',
//        'questions',
//        'materials',
//
//        'excelent'
        //'dop_questions',
    ];
    protected $appends = [
        'normal_date',
    ];

    public function meeting()
    {
        return $this->hasOne(Meeting::class,'id', 'meeting_id');
    }

    public function student()
    {
        return $this->hasOne(MeetingHasStudent::class,'user_id', 'user_id')->where('meeting_id', $this->meeting_id);
    }

    public function ticket()
    {
        return $this->hasOne(Ticket::class,'id', 'ticket_id');
    }

    public function getEval()
    {
        $result = '---';
        if($this->eval == 3) {
            $result = 'удовлетворительно';
        } elseif($this->eval == 4) {
            $result = 'хорошо';
        } elseif($this->eval == 5) {
            $result = 'Отлично';
        }

        return $result;
    }

    //
    const STATUS_CREATED_SLUG = 'created';
    const STATUS_FILL_SLUG = 'fill';
    public static $statuses_named = [
        self::STATUS_CREATED_SLUG => [
            'name' => 'Ожидает подтверждения'
        ],
        self::STATUS_FILL_SLUG => [
            'name' => 'Подтверждена'
        ],
    ];
    public function getNormalDateAttribute()
    {
        return $this->getNormalDate();
    }

    public function getNormalDate($type = 'full')
    {
        $months = array( 1 => 'января' , 'февраля' , 'марта' , 'апреля' , 'мая' , 'июня' , 'июля' , 'августа' , 'сентября' , 'октября' , 'ноября' , 'декабря' );

        if(empty($this->date)) {
            return '';
        } else {
            if($type == 'd') {
                return date('d', strtotime($this->date));
            } elseif ($type == 'm') {
                return date($months[date( 'n' )], strtotime($this->date));
            } elseif ($type == 'y') {
                return date('Y', strtotime($this->date));
            } else {
                return date('d '.$months[date( 'n' )].' Y', strtotime($this->date));
            }
        }
    }

    public function getTicket()
    {
        $num = 0;

        while( 1 ) {
            $tickets = Ticket::where('meeting_id', $this->meeting_id)->where('counter', $num)->get();

            if($tickets->count())  {
                break;
            } else {
                $num++;
            }

            if($num > 10) {
                $tickets = Ticket::where('meeting_id', $this->meeting_id)->get();
                break;
            }
        }

        $ticketIndex = rand( 0, ($tickets->count() - 1));
        $tickets[$ticketIndex]->increment('counter');

        $this->ticket_id = $tickets[$ticketIndex]->id;
        $this->save();
        return $tickets[$ticketIndex];
    }

    public function download()
    {
// Вставка данных с проверкой на наличее
        function setValue($template, $position, $value) {
            if(is_array($value)) {
                $value = '';
            }
            // Вставка значения
            $template->setValue($position, $value);
        }
        //
        $number   = $this->number;
        $day    = $this->getNormalDate('d');
        $month  = $this->getNormalDate('m');
        $year   = $this->getNormalDate('y');

        $chairman  = $this->meeting->getChairmanName();
        $comission = $this->meeting->persons;

        $trend = $this->meeting->group->trend;
        $profile = $this->meeting->group->profile;
        $eduForm = $this->meeting->group->form_name;

        $timeFrom = explode(':', $this->time_from);
        $timeTo = explode(':', $this->time_to);

        $h_f    = $timeFrom[0];
        $m_f    = $timeFrom[1];
        $h_t    = $timeTo[0];
        $m_t    = $timeTo[1];
//
        $who = $this->meeting->group->level == Group::POSTGRADUATE ? 'аспирант' : 'студент';
        $group = $who.' группы '.$this->meeting->group_name;
        $fio    = $this->student->fullName;
//

        $ticket_number = empty($this->ticket->number) ? '_______' : $this->ticket->number;

        $dop_questions = $this->dop_questions;

        $review = $this->review;

        $eval = $this->getEval();

        $comment = $this->comment;
//
//        $chairman_sign = $this->getChairmanSign();
//
//        $chairman_fio = $this->gek->getChairmanShortName();
//
//        $sign_block = [];
//
//        //foreach ()
//
//        $secretary_sign = $this->getSecretarySign();
//
//        $secretary_fio = $this->gek->getSecretaryShortName();

        // Создание файла
        $templatePath = 'templates/exam.docx';
        $filename     = public_path($templatePath);
        $template = new TemplateProcessor($filename);


        setValue($template, 'number',        $number);

        setValue($template, 'day',        $day);
        setValue($template, 'month',       $month);
        setValue($template, 'year',         $year);

        setValue($template, 'chairman',         $chairman);

        $template->cloneRow('comission', $comission->count());
        foreach ($comission as $keyTransfer => $transfer) {
            setValue($template, 'comission#'.($keyTransfer+1),   $transfer->fullName);
        }

        setValue($template, 'trend',         $trend);
        setValue($template, 'profile',         $profile);
        setValue($template, 'eduForm',         $eduForm);

        setValue($template, 'h_f',         $h_f);
        setValue($template, 'm_f',         $m_f);
        setValue($template, 'h_t',         $h_t);
        setValue($template, 'm_t',         $m_t);

        setValue($template, 'group',         $group);
        setValue($template, 'fio',         $fio);
//
        setValue($template, 'ticket_number',         $ticket_number);
//
        setValue($template, 'dop_questions',         $dop_questions);
        setValue($template, 'review',         $review);
        setValue($template, 'eval',         $eval);
        setValue($template, 'comment',         $comment);
//

        //$template->cloneBlock('block',count($this->ticket->tasks), true, true);
        if($this->ticket_id) {
            $template->cloneRow('num', $this->ticket->tasks()->count());

            foreach ($this->ticket->tasks as $taskIndex => $task) {
                $taskNum = $taskIndex + 1;

                setValue($template, 'num#'.$taskNum, $task->number);
                setValue($template, 'text#'.$taskNum, $task->text);

                if(!empty($task->img)) {
                    $template->setImageValue('img#'.$taskNum, array('path' => public_path($task->img),  'ratio' => false));
                } else {
                    setValue($template, 'img#'.$taskNum, '');
                }
            }
        } else {
            setValue($template, 'num', '');
            setValue($template, 'text', '');
            setValue($template, 'img', '');
        }

        setValue($template, 'chairman_fio',          $this->meeting->chairman->shortName);
        $chairman_hash = Sign::getHash(self::class, $this->id, $this->meeting->chairman->user_id);
        $chairman_hash_img = Sign::haveImg($chairman_hash);
        if($this->meeting->chairman->distance && $chairman_hash_img) {
            $template->setImageValue('chairman_sign', array('path' => public_path('img/certificate/'.$chairman_hash.'.png'), 'width' => 300, 'ratio' => false));
        } else {
            setValue($template, 'chairman_sign', '');
        }

        setValue($template, 'secretary_fio',         $this->meeting->secretary->shortName);
        $secretary_hash = Sign::getHash(self::class, $this->id, $this->meeting->secretary->user_id);
        $secretary_hash_img = Sign::haveImg($secretary_hash);
        if($this->meeting->secretary->distance && $secretary_hash_img) {
            $template->setImageValue('secretary_sign', array('path' => public_path('img/certificate/'.$secretary_hash.'.png'), 'width' => 300, 'ratio' => false));
        } else {
            setValue($template, 'secretary_sign', '');
        }

        //setValue($template, 'secretary_sign',        $secretary_sign);

        $template->cloneBlock('sign_place',$this->meeting->persons()->count(), true, true);
        foreach ($this->meeting->persons as $keyTransfer => $transfer) {
            $hash = Sign::getHash(self::class, $this->id, $transfer->user_id);
            $hash_img = Sign::haveImg($hash);
            if($transfer->distance && $hash_img) {
                $template->setImageValue('csign#'.($keyTransfer+1), array('path' => public_path('img/certificate/'.$hash.'.png'), 'width' => 300, 'ratio' => false));
            } else {
                setValue($template, 'csign#'.($keyTransfer+1), '');
            }
            //setValue($template, 'csign#'.($keyTransfer+1),   $transfer->getSignHash($this->student_id));
            setValue($template, 'cfio#'.($keyTransfer+1),   $transfer->shortName);
        }

        //
        header('Content-Disposition: inline; filename="Протокол ГЭК №'.$number.' '.$this->student->fullName.' '.$this->meeting->group_name.'.docx"');
        header('Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        $template->saveAs('php://output');
        //
        exit;
    }
}
