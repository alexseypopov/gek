<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'chair_id', 'name', 'level', 'trend', 'profile', 'form', 'graduation_year'
    ];

    /**
     * Отношение к кафедре
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function chair()
    {
        return $this->hasOne(Subdividion::class,'id', 'chair_id');
    }

    public function students()
    {
        return $this->belongsToMany(User::class, GroupHasStudent::class)->orderBy('lastName', 'asc')->orderBy('firstName', 'asc')->orderBy('patronymic', 'asc');
    }

    public function meetings()
    {
        return $this->hasMany(Meeting::class, 'group_id', 'id');
    }

    const PAGINATE = 10;

    // Вычисляемые поля
    protected $appends = [
        'form_name',
    ];

    const SPO = 'spo';
    const BACHELOR = 'bachelor';
    const MAGISTRACY = 'magistracy';
    const SPECIALTY = 'specialty';
    const POSTGRADUATE = 'postgraduate';
    public static $levels = [
//        [
//            'slug' => self::SPO,
//            'name' => 'Cреднее профессиональное образование',
//        ],
        [
            'slug' => self::BACHELOR,
            'name' => 'Бакалавриат',
        ],
        [
            'slug' => self::MAGISTRACY,
            'name' => 'Магистратура',
        ],
        [
            'slug' => self::SPECIALTY,
            'name' => 'Cпециалитет',
        ],
        [
            'slug' => self::POSTGRADUATE,
            'name' => 'Аспирантура',
        ]
    ];
    public static $levels_named = [
            //self::SPO => 'Cреднее профессиональное образование',
            self::BACHELOR => 'Бакалавриат',
            self::MAGISTRACY => 'Магистратура',
            self::SPECIALTY => 'Cпециалитет',
            self::POSTGRADUATE => 'Аспирантура',
    ];

    const FULL = 'full';
    const SHORT = 'short';
    const EXTERNAL = 'external';
    public static $forms = [
        [
            'slug' => self::FULL,
            'name' => 'Очная',
        ],
        [
            'slug' => self::SHORT,
            'name' => 'Очно-заочная',
        ],
        [
            'slug' => self::EXTERNAL,
            'name' => 'Заочная',
        ],
    ];
    public static $forms_named = [
        self::FULL => 'Очная',
        self::SHORT => 'Очно-заочная',
        self::EXTERNAL => 'Заочная',
    ];

    public function getFormNameAttribute()
    {
       return self::$forms_named[$this->form];
    }

    public static function validate($data) {
        return Validator::make($data, [
            'chair_id' => ['required'],
            'name' => ['required', 'string', 'max:255'],
            'level' => ['required', 'string'],
            'trend' => ['required', 'string'],
            'profile' => ['required', 'string'],
            'form' => ['required', 'string'],
            'graduation_year' => ['required', 'integer'],
        ]);
    }

    public function getLevel()
    {
        return isset(self::$levels_named[$this->level]) ? self::$levels_named[$this->level] : 'Уровень '.$this->level.' не найден, обратитесь к администратору';
    }

    public function getForm()
    {
        return isset(self::$forms_named[$this->form]) ? self::$forms_named[$this->form] : 'Форма обучения '.$this->form.' не найдена, обратитесь к администратору';
    }

    public function getCoutStudents()
    {
        return $this->students()->count();
    }

    public function getStudentsJson()
    {
        $result = [];

        foreach ($this->students as $student) {
            $result[] = [
                'value' => $student->getFullName(),
                'id'    => $student->id,
                'data'  => $student
            ];
        }

        return json_encode($result);
    }

    public function getParentText ()
    {
        $chair = $this->chair();
        //$chair_parent =
        return '--';
    }

    public function deleteMe()
    {
        GroupHasStudent::deleteStudents($this->id);
        $this->delete();

        return true;
    }

    public function getChairName()
    {
        return isset($this->chair->id) ? $this->chair->name : 'Ошибка при выводе названия кафедры';
    }

    public function getDataToSelect()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'sub' => [
                'chair' => Subdividion::getInfoById($this->chair_id),
                'faculty' => Subdividion::getFacultyByChair($this->chair_id),
                'inst' => Subdividion::getInstBySub($this->chair_id),
                'umu' => Subdividion::getUMUBySub($this->chair_id),
            ]
        ];
    }

}
