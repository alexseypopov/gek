<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MeetingHasStudent extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'id',
        'meeting_id',
        'user_id',
        'distance',
        'fullName',
        'fullName_rod',
        'fullName_dat',
        'fullName_tv',
    ];


    public function meeting()
    {
        return $this->hasOne(Meeting::class,'id', 'meeting_id');
    }

    public function user()
    {
        return $this->hasOne(User::class,'id', 'user_id');
    }

    public function report_protocol()
    {
        return $this->hasOne(MeetingHasReportProtocol::class,'user_id', 'user_id')->where('meeting_id', $this->meeting_id);
    }

    public function exam_protocol()
    {
        return $this->hasOne(MeetingHasExamProtocol::class,'user_id', 'user_id')->where('meeting_id', $this->meeting_id);
    }

    public function solution()
    {
        return $this->hasOne(MeetingHasSolution::class,'user_id', 'user_id')->where('meeting_id', $this->meeting_id);
    }

    public static function deleteStudent($meeting_id, $user_id)
    {
        self::where('meeting_id', $meeting_id)->where('user_id', $user_id)->first()->delete();
        return [
            'status' => 'success'
        ];
    }

    public static function createStudent($meeting_id, $student)
    {
        self::create([
            'meeting_id'    => $meeting_id,
            'user_id'       => $student['user_id'],
            'fullName'      => $student['fullName'],
            'fullName_rod'  => $student['fullName_rod'],
            'fullName_dat'  => $student['fullName_dat'],
            'fullName_tv'   => $student['fullName_tv'],
        ]);
        return [
            'status' => 'success'
        ];    }

    public static function updateStudent($meeting_id, $student)
    {
        $old = self::where('meeting_id', $meeting_id)->where('user_id', $student['user_id'])->first();

        $old->user_id       = $student['user_id'];
        $old->fullName      = $student['fullName'];
        $old->fullName_rod  = $student['fullName_rod'];
        $old->fullName_dat  = $student['fullName_dat'];
        $old->fullName_tv   = $student['fullName_tv'];
        $old->save();

        return [
            'status' => 'success'
        ];
    }

    public static function changeUserAccess($user_id, $send, $email)
    {
        $validations = [
            'allow' => '',
            'auth' => '',
            'send' => '',
            'email' => '',
            'fullName_rod' => '',
            'fullName_dat' => '',
            'fullName_tv' => '',
        ];

        $user = User::find($user_id);

        $emailValidate = Validator::make([ 'email' => $email ], [
            'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore($user_id),],
        ]);

        if($emailValidate->fails()) {
            $validations['email'] = $emailValidate->errors()->get('email');
            return [
                'status' => 'fail',
                'validations' => $validations
            ];

        }

        $user->changeEmail($email, $send);

        return [
            'status' => 'success',
        ];
    }
    
    public function createReportProtocol()
    {
        if($this->report_protocol()->exists()) {
            return true;
        }

        MeetingHasReportProtocol::create([
            'user_id' => $this->user_id,
            'meeting_id' => $this->meeting_id,
            'status' => MeetingHasReportProtocol::STATUS_CREATED_SLUG,
            'materials' => json_encode([
                'Справка института о выполнении студентом(кой) рабочего учебного плана.',
                'Результаты сдачи государственного междисциплинарного экзамена(при наличии).',
                'Расчетно-пояснительная записка на ___ страницах.',
                'Иллюстративно-графический материал на ___ страницах.',
                'Отзыв руководителя',
                'Рецензия(при наличии)'
            ]),
            'questions' => json_encode([
                'Вопрос 1','Вопрос 2','Вопрос 3'
            ]),
        ]);

        return true;
    }

    public function createExamProtocol()
    {
        if($this->exam_protocol()->exists()) {
            return true;
        }

        $dd = MeetingHasExamProtocol::create([
            'user_id' => $this->user_id,
            'meeting_id' => $this->meeting_id,
            'status' => MeetingHasExamProtocol::STATUS_CREATED_SLUG,
        ]);


        return true;
    }

    public function createSolution()
    {
        if($this->solution()->exists()) {
            return true;
        }

        MeetingHasSolution::create([
            'user_id' => $this->user_id,
            'meeting_id' => $this->meeting_id,
            'status' => MeetingHasSolution::STATUS_CREATED_SLUG,
        ]);

        return true;
    }

    public function statusReport()
    {
        return isset($this->report_protocol->id) ? $this->report_protocol->status : 'fail';
    }

    public function statusExam()
    {
        return isset($this->exam_protocol->id) ? $this->exam_protocol->status : 'fail';
    }

    public function statusSolution()
    {
        return isset($this->solution->id) ? $this->solution->status : 'fail';
    }

    public function getReportProtocolText()
    {
        $result = '';
        if($this->report_protocol()->exists()) {
            $result = 'Протокол №'.$this->report_protocol->number.' от '.$this->report_protocol->normal_date.' г. о защите выпускной квалификационной работы обучающегося/обучающейся с оценкой '.$this->report_protocol->eval.'.';
        }

        return $result;
    }

    public function getExamProtocolText()
    {
        $result = '';
        if($this->exam_protocol()->exists()) {
            $result = 'Протокол №'.$this->exam_protocol->number.' от '.$this->exam_protocol->normal_date.' г. о защите защите государственного экзамена обучающегося/обучающейся с оценкой '.$this->exam_protocol->eval.'.';
        }

        return $result;
    }

    public function isExcelent()
    {
        $result = 'false';

        if($this->report_protocol()->exists()) {
            $result = $this->report_protocol->excelent ? 'true' : 'false';
        }

        return $result;
    }

    public function yesCount()
    {
        return '1628800';
    }

    public function noCount()
    {
        return '-1628800';
    }

}
