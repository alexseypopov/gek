<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comission extends Model
{
    use SoftDeletes;

    const PAGINATE = 15;

    public function user()
    {
        return $this->hasOne(User::class,'id', 'user_id');
    }
    public function meeting()
    {
        return $this->hasOne(Meeting::class,'id', 'meeting_id');
    }

    protected $fillable = [
        'id', 'meeting_id', 'type', 'user_id', 'fullName', 'shortName', 'distance'
    ];

    const TYPE_CHAIRMAN_SLUG = 'chairman';
    const TYPE_CHAIRMAN = [
        'slug' => self::TYPE_CHAIRMAN_SLUG,
        'name' => ' Председатель',
        'role' => 'chairman'
    ];
    const TYPE_SECRETARY_SLUG = 'secretary';
    const TYPE_SECRETARY = [
        'slug' => self::TYPE_SECRETARY_SLUG,
        'name' => 'Секретарь',
        'role' => 'secretary'
    ];
    const TYPE_PERSON_SLUG = 'person';
    const TYPE_PERSON = [
        'slug' => self::TYPE_PERSON_SLUG,
        'name' => 'Член',
        'role' => 'person'
    ];

    public static $types = [ self::TYPE_CHAIRMAN, self::TYPE_SECRETARY, self::TYPE_PERSON];
    public static $types_named = [ self::TYPE_CHAIRMAN_SLUG => self::TYPE_CHAIRMAN, self::TYPE_SECRETARY_SLUG => self::TYPE_SECRETARY, self::TYPE_PERSON_SLUG => self::TYPE_PERSON];

    public function getType()
    {
        return self::$types_named[$this->type]['name'];
    }

    public static function createPerson($meeting_id, $type, $user_id, $distance)
    {
        $user = User::findOrFail($user_id);

        $person = self::create([
            'type' => $type,
            'meeting_id' => $meeting_id,
            'user_id' => $user_id,
            'fullName' => $user->getFullName(),
            'shortName' => $user->getShortName(),
            'distance' => $distance,
        ]);

        $user->assignRole(self::$types_named[$type]['role']);

        return true;
    }

    public function getVoteIcon($protocol, $user_id)
    {
        if($this->distance) {
            return '<span class="text-warning" title="Голос должен распределить секретарь комиссии"><i class="bi bi-asterisk"></i></span>';
        } else {

            return '<span class="text-success"><i class="bi bi-check-lg"></i></span>';
        }
    }
  }
