<?php
namespace App\Models;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;


class Role extends \Spatie\Permission\Models\Role {

    protected $fillable = [
        'name', 'title', 'description', 'guard_name'
    ];

    const PAGINATE = 10;

    /**
     * Описание роли
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Валидация перед созданием нового пользователя
     * @param $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validateBeforeCreate($data) {
        return Validator::make($data, [
            'name' => ['required', 'unique:roles,name'],
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'permissions' => ['nullable'],
        ]);
    }
    /**
     * Валидация перед созданием нового пользователя
     * @param $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validateBeforeUpdate($data) {
        return Validator::make($data, [
            'name' => ['required', Rule::unique('roles')->ignore($data['id'])],
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'permissions' => ['nullable'],
        ]);
    }
}