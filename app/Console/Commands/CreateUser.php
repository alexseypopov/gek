<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\Hash;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:create-user {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Позволяет добавить пользователя.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo "Создание нового пользователя \n";

        $validator = Validator::make([ 'email' => $this->argument('email'), 'password' => $this->argument('password') ], [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        // Проверка фалидации
        if($validator->fails()) {
            echo "\t Произошла ошибка при попытке создания пользователя: \n";
            echo "\e[31m ";
            foreach ($validator->errors()->all() as $error) {
                echo "\t\t ".$error." \n";
            }
            echo "\e[0m";
            return 0;
        }

        // Создание пользователя
        $user = User::create([
            'email' => $this->argument('email'),
            'firstName' => 'Имя',
            'lastName' => 'Фамилия',
            'patronymic' => null,
            'password' => Hash::make($this->argument('password')),
        ]);
        echo "\t\e[32m --Пользователь (id ".$user->id.") успешно создан. \e[0m \n";

        return 1;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('email', InputArgument::REQUIRED),
            array('password', InputArgument::REQUIRED)
        );
    }



}
