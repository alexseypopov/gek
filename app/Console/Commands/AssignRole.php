<?php

namespace App\Console\Commands;

use App\Models\Role;
use App\Models\User;
use Illuminate\Console\Command;

class AssignRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:assign-role {email} {role} {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Позволяет добавить пользователю роль.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo "Присвоение пользователю роли \n";
        
        $user = User::where('email', $this->argument('email'))->first();
        $role = Role::findByName($this->argument('role'));


        // Проверка фалидации
        if(!isset($user->id)) {
            echo "\t Произошла ошибка: \n";
            echo "\t\t \e[31m Пользователь не найден: \e[0m \n";
            return 0;
        }

        if($this->argument('type') == 'disallow') {
            echo "\t\e[32m --У пользователя \"".$user->getFullName()."\" отозвана роль \"".$role->title."\". \e[0m \n";
            $user->removeRole($role);
        } else {
            echo "\t\e[32m --Пользователю \"".$user->getFullName()."\" присвоена роль \"".$role->title."\". \e[0m \n";
            $user->assignRole($role);
        }


        return 1;
    }
}
